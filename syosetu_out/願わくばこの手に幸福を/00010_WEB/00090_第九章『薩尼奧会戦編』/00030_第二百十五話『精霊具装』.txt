如果──你不想依賴我的話，那當然也無所謂。到那時我發誓我不會動用自己的任何力量。作為女王的力量也好，作為個人的力量也好。或者還是需要依賴我呢，能從你嘴裡好好地說出來嗎？

那句話對於艾爾蒂絲來說，是無可否認的真心話語。心裡隱藏著幾分焦慮。
血脈疼痛的感覺提高速度在身體中奔跑。艾爾蒂絲明白了被理性這個外殻覆蓋住的自己的本能，在這個時候這個本能立起爪子責備自己的頭腦。

自己的騎士，不依靠自己的士兵而是依靠了其他的人，只是那個，艾爾蒂絲就難以忍受。

「怎麼樣呢，路易斯？」

為了追趕眼前的騎士，女王縮短了視線，縮短了距離。已經是彼此的手足互相接觸的那樣的距離，像猛禽類追逼獵物似的，以一股緊緊的勢頭，艾爾蒂絲把碧眼轉向路易斯。

路易斯像是選擇語言一樣歪曲著眼睛，讓嘴唇波動。想聽到他的話語。想早點安心。艾爾蒂絲輕輕地咬著嘴唇。
如果，如果，萬一路易斯真的覺得不能依靠我的士兵，甚至還覺得我難以依靠的話。

那就是，我──沒有遵守路易斯的指示，那個下達了要成為出色的女王的命令。
啊，我討厭那個。我就不喜歡那個。我不想被他認為自己沒有用，偏離了他的指示。因為遵守命令是束縛的證明。只要我遵守命令，就如同被他束縛一樣。

路易斯說約束我。我答應了那個。所以這個誓約是絶對的。至少，對於艾爾蒂絲來說，它就如同寶石一般堅固，並且閃耀。
艾爾蒂絲明白了喉嚨發出很大的聲音，自己的眼皮痙攣。等待路易斯的話語。

「⋯⋯當然靠得住。」

路易斯有點困惑地發出聲音，抓住艾爾蒂絲的雙肩說。

聽到這句話的瞬間，艾爾蒂絲在腦海啟動了精靈術的因果。術式編入之後。只要在腦中發出一點點信號，它就會啟動。

就在這時，很多東西，甚至連洪水都不如的海量信息被注入了艾爾蒂絲腦海中。是他的，路易斯的信息。

──呼吸、脈動沒有任何混亂，而且精靈也說他沒有虛言。

艾爾蒂絲的臉頰自然鬆弛下來。眼睛好像也同時恢復了柔軟。他的話是出自內心的真實。

那絶不是不信賴路易斯，但只是猜測話語中的真意的話，艾爾蒂絲的心不是那麼容易就能平靜的。
這是真實的。路易斯身著的深綠色軍服，以及身上穿的幾件裝飾品，告訴了艾爾蒂絲所有的答案。

艾爾蒂絲贈送給路易斯的各類裝備，全部都是編入了艾爾蒂絲的精靈術。連每根絲線和手鐲的碎片，全部都用大精靈賜予艾爾蒂絲的力量精煉而成，是用捨不得睡覺吃飯的下苦工的程度精心製作出來的。

過去為了從塔中環視外面的世界，只使用了創造幻影這一個精靈術式的艾爾蒂絲，更加凝練自己的精靈術，將其固定精煉了的異物，就是精靈具裝。那是受到大精靈寵愛的精靈，一生中只能創造出幾個的超越智慧的存在，做出一定數量後就會死去，就好像說那是宿命一樣。

從某種意義上說，那個配飾是艾爾蒂絲的分身。包裹在身上的他，多麼可愛啊！

戴上精靈具裝以後，他的呼吸和身體的運動，他的身體發出的全部的東西，十分自然地就被艾爾蒂絲的手掌握住了。
想知道他的一切，他在做些什麼，在做著怎樣的思考。艾爾蒂絲認為那是理所當然的事。那個現在成為了現實。太棒了。

當然，如果經常這麼使用的話，就會很容易被信息的濁流吞沒。但是，如果是這種程度的臨時性的利用的話，是很容易的事。
就這樣，現在具裝所提供的全部信息都明白地反饋了，路易斯並沒有說謊，而是依賴著我。
也就是說，自己能夠按照他的希望來執行指示。沒有比這更令人高興的事了。

艾爾蒂絲把險峻的表情完全變成溫柔的笑容，在近處看著路易斯的眼睛，問道。

「那太好了。如果我的騎士能依靠我的話，我也會作為女王盡情的發揮力量。我也依賴著你。給你的那件軍服，就是信賴的證明」

就在這句話面前，路易斯一瞬間不知所措地瞪圓了眼睛。剛才為止還籠罩著周圍的鐵一般的冰冷的氣息，瞬間就失去了，讓人感到驚訝的樣子。
艾爾蒂絲嘴唇被波動著，繼續著語言。

「這是你專用的裝備。怎麼樣，穿著舒服嗎？」

不錯，相當舒適，路易斯很輕地縮著肩膀回答。那也是真心話。嗯，看來用不著了，好像效果很好。艾爾蒂絲很小聲地鳴著牙，切斷跟配件的連接。
長久以來都與裝飾緊密相連與他直接接觸的自己，以及仍舊作為女王的自己，那個才是真實的自己，我怎麼也搞不清楚那個境界。

當然，這也許就是我最喜歡的東西了。路易斯說，要當好女王。那麼也不能不遵守指示。
而且即使不那樣做，精靈具裝除了能探知路易斯的情況以外，在其他方面也很有用。

精靈裝備是艾爾蒂絲的分身，被裝入精靈術的精華的東西。
人類親近精靈術具現化的存在，只要掌握了它，身體就會變得輕鬆，充滿生氣。精靈的保佑，是能保護持有者的生命的。

但是──一直受到精靈呵護的人，不可能永久正常。這就好像在近處持續沐浴著神之光一樣。那個身體會確實地被巨大的力量侵蝕。

總有一天，那個受精靈術影響的人，就只能在精靈術的使用者和大精靈所居住的深綠森林中才能生存下去了。在其他地方待著的話，他連呼吸都會變得不習慣起來吧。那是確實的未來。

艾爾蒂絲的碧眼變細，臉上畫著線似的浮現出笑容。

──我會遵守約定，賭上自己的矜持。絶對不會放跑你的。

想被束縛，想被束縛。靈魂本身像顫抖一樣扭曲的感情。但是，對於活在漫長的時光中，經歷過眾多離別的精靈來說，想要一直束縛著某些人的慾望，從某種意義上說是自然的吧。

艾爾蒂絲在自己的心中，浮現出難以形容的情感，並且靜靜地透出呼吸。但是，絶不認為這種情感會消失。

希望被束縛著，不要離開。想綁住你，不然給你不離開。啊，起床的時間、入睡的時間、吃飯、行動的一切，全都被你命令，如果能這樣被命令的話，那該是多麼幸福的事情啊。

艾爾蒂絲嘴唇蠕動著，視線投向路易斯的脖子，讓長長的耳朵痙攣。
今天暫且就這樣吧，知道了他的真心，也確認了精靈術的效果，取得了很充分的成果。

而且，好像已經沒有時間自由活動了。
艾爾蒂絲的長耳朵聽到了那個聲音。急速，像著急一樣，響起馬蹄的聲音。與普通的傳令兵的情況完全不同，那個。這是抓住某些異變，來傳達某些異變的人的舉動。

「等待的時間好像結束了。走吧，我的騎士。」

只說了這一句話。艾爾蒂絲就把視線投向了帳幕的外面。

在那長長的耳朵裡，開始聽到戰場開始活動的聲音。