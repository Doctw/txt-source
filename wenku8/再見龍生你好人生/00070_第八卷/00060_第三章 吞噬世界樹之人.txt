當代擔任巫女姬的是一個名為琉琉西的少女，即使是在恩特之森的所有精靈之中，她的靈格也是非常出類拔萃的，同時她和羽古多拉席爾的適應性也非常好。
精靈種有名為高等精靈的上位種族，而琉琉西正是高等精靈。而且她還是目前生活在恩特之森的高等精靈之中，最年輕的世代。
森林外的人們曾想像過精靈的外貌，並根據想像描繪出無比美麗的畫像，但琉琉西的美貌，要遠在這些畫像之上。她那奢華的身體描繪著美妙無比的曲線，在這讓人著迷的身體之上還穿著用羽古多拉席爾的枝葉製作的巫女姬服裝。
這身衣物巧奪天工，甚至讓人以為是天女所穿的羽衣，這淡綠色的衣物薄如蟬翼，甚至可以透過衣服看清衣服背後的東西，而她的腳上，則穿著用藤蔓和樹皮製作的涼鞋。

譯：羽衣，傳說神女身上所傳的衣物，特點就是又薄又輕，划重點。

琉琉西手上拿著一根法杖，這法杖的頂端鑲嵌了一個羽古多拉席爾的魔力所凝結而成的黃色水晶球。綠色的長髮垂直腰部，在她的頭頂還戴著一個用羽古多拉席爾樹枝製作的頭冠。
琉琉西的身邊跟著許多護衛她的戰士以及諸多巫女，他們正在等待著羽古多拉席爾神諭的到來和祭典開幕式舉辦，但琉琉西卻一副非常緊張的樣子。
琉琉西她們所處的地方便是在羽古多拉席爾其枝幹內部的神殿，而處於這神殿最深處的便是神諭之間。現在的他們便是在專門設置給巫女姬用來進行準備和休息的休息室內，而這休息室則位於神諭之間旁邊。
不過說是神殿，也不是人外建造出來的，這座神殿是過去的巫女姬們向羽古多拉席爾祈禱，最後羽古多拉席爾對自己的身體內部進行改造和變化而最終形成的。從本質上來說，這神殿依然算是羽古多拉席爾身體的一部分。
雖然羽古多拉席爾的意識也存在於這個神諭之間，但對於現在的琉琉西來說，這個地方與其說是神諭之間不如說是她的處刑場才更為貼切。就在此時，一個剛剛進入房間的巫女姬看著她的那副樣子不由得開口說道。
這個人正是剛剛與多蘭她們分別的迪婭多菈。
「琉琉西，要是你真的想冷靜下來的話，比起像這樣四處走來走去，冥想還要更有作用哦。」
「迪婭多菈，因為你遲遲未到，所以我稍微變得有些焦急了·······雖然很丟臉，但要是有你在的話，我就能安心的去進行接受神諭的儀式和祭典的開幕式演講。」
雖然有兩個巫女姬是突然之間的決定，但當眼前的高等精靈少女看見被選為第二名巫女姬的自己時，卻露出了安心的表情。迪婭多菈鮮明的回憶起了那時的場景。
「嗯，我也不是不懂你的心情。別看我現在表面上看起來很平靜，但其實我心裡也是在緊張的。」
在附近等待著的巫女們靠近迪婭多菈，開始給迪婭多菈換上和琉琉西身上相同的巫女姬服飾。
迪婭多菈將平時變形為禮服外形的一部分肉體取消了變化，變成了全裸，任由巫女們替她更衣。
迪婭多菈沒有一絲害羞的神色，就這樣大大方方的露出了自己的肉體，反而是看著她更衣的琉琉西臉變紅了。儘管對方是不同種族的同性，但迪婭多菈的肢體對她依然充滿了誘惑力。
當然，迪婭多菈能如此大方，也是因為男性的護衛們全都離開了房間內。
更衣完畢後，迪婭多菈和也琉琉西一樣在頭上戴上了一個頭冠。為了習慣這身衣服，她輕輕的做著各種動作，而她的這些宛如舞蹈一般的動作，讓琉琉西和巫女們都看呆了。
「沒想到居然如此合身。這樣的話，只要注意在行走的時候別踩到下擺就好了。不過，果然還是琉琉西你更適合這種衣服呢。只有在履行巫女姬的職責時才能看到你穿這身衣服的樣子，真是可惜啊，琉琉西。」
「不，不，我覺得迪婭多菈你才更漂亮。非常的妖艷，有氣質，現在也感覺要迷上你了。」
聽到琉琉西說出的真心話，迪婭多菈露出了一個微微的笑容。看起來是因為被人誇獎而非常滿足的樣子。
迪婭多菈看著還在休息室內的其中一個護衛，笑容突然變成了捉弄人時的笑容。這個護衛正是在這次擔任護衛的人之中，實力最強，同時也是影響力最大的，她的名字叫歐理維爾。
「是這樣麼？看來我還是有可取之處的呀·······啊，對了，歐理維爾，多蘭他們來了哦。一會和我去見見他們麼？」
因為擔任護衛而在來到這裡的歐理維爾身上穿著用巨大靈鳥的羽毛和萬年蠶的蠶絲製作出來的貫頭衣，身上還拿著法杖。

譯：所謂貫頭衣，就是在一幅布的正中央剪出一條直縫，將頭從這條縫裡套過去，然後再將兩腋下縫合起來的衣服。也有資料說，最早的和服是在布上挖一個洞，從頭上套下再用帶子系住兩腋下的布，再配上類似裙子的下裝。

歐理維爾甚至還戴上了戒指、手鏈、耳環、項鏈等這些她平時不會戴在身上的裝飾品。這些飾品其實是歐理維爾的最強裝備，而製造這些裝備所使用的甚至都稱得上是奢華的材料，都是在歐理維爾當冒險者時攢下來的。
恩特之森的最強精靈使之一——歐理維爾，為了擔任巫女姬的護衛，將自己武裝成了最強姿態，由此可見這一次的祭典對於恩特之森的人們來說到底有多重要。
「是呢，這次就不是作為學院長和學生，而是作為恩特之森的住民和貝倫村的村民來見面吧。畢竟是難得的祭典，要是在這種地方還在意身份的高低，就沒辦法好好享受了。」
「那還真是不錯呢。畢竟你也是難得回到了故鄉，就忘掉身為學院長的責任吧。」
「作為學院長的責任我可是一刻都不會忘記的，迪婭多菈。話說，你和多蘭見面之後心情還真是好啊，看起來完全不緊張了。作為的你朋友，我感到很安心。」
「真不愧是你，眼睛還是一如既往的敏銳。我也是一個普通人啊，緊張是很正常的事情。所以我就去和喜歡的人一起聊天，然後把這多餘的緊張給消除掉。和喜歡的人在一起，會讓人的內心變得強大哦。」
聽到迪婭多菈說出的「喜歡的對象」，琉琉西和其他的巫女們全都瞪大了眼睛，一副非常吃驚的樣子。歐理維爾裝作沒有看到她們的反應，就這和繼續和迪婭多菈對話。
「關於你對多蘭的戀情，就我個人而言我是想支持你的。畢竟你是我去做魔法學院的學院長之前就認識了的老朋友，多蘭雖然有些給人添麻煩，但也是我重要的學生。我覺得，如果是你的話，賽莉娜也能接受的吧。真是不知道你是不是將那兩個人一同愛著，還是你們兩個人分別愛著多蘭。不過這樣下去的話，你和那個女孩有可能會演變成針鋒相對的情況啊。」
「呵呵呵，這時候就要給你看看我的手腕了。」
看著迪婭多菈好像很樂在其中似的笑了起來，歐理維爾腦海中不由得想像出迪婭多菈將賽莉娜推到——是的，並不是多蘭——然後賽莉娜發出了「啊哇哇」難為情的聲音，如此這般的一副畫面。
「真是像黑薔薇之精，不，並不是像黑薔薇之精，而應該是真是像你會說的積極發言。」
歐理維爾扶額嘆息道。
在亞克雷斯特王國，如果滿足了一定的條件，是允許一夫多妻制的。所以一位男性與複數女性保持著關係這種事，在倫理和法律上都是被認可的。
——估計當事人都會覺得幸福，還是別太在意這件事比較好。歐理維爾這樣考慮著，決定將這個問題先放著。
要是知道歐理維爾平時是什麼樣子的人，看到她現在這幅樣子，估計會大吃一驚吧。
原本願意聽自己抱怨多蘭這個頭疼根源的親友，卻因為多蘭又成為了新的頭疼根源，歐理維爾不由得感覺到了一絲舉目無親的孤獨感。
「因為是第一次戀愛，什麼都不明白，所以我無論什麼時候都是全力以赴的哦。所以就算表面上看起來一直都很有餘裕，其實在內心裡我也是很拚命的。」
「你自己的風格的全力以赴······麼。既然你都這樣說了，作為你的朋友，我是不會將你棄之不顧的。」
看著舉起白旗，雖然露出困惑表情，但依然面帶笑容的歐理維爾，迪婭多菈露出了「朋友就該這樣」的溫柔表情。
因為琉琉西而被緊張感包圍的休息室，多虧迪婭多菈和歐理維爾的對話——雖然伴隨著些許震驚——將多餘的緊張氛圍給驅散了。
這樣的話，琉琉西和迪婭多菈都能冷靜的履行自己作為巫女姬的職責了。
除了歐理維爾以外的護衛和巫女們看見琉琉西和迪婭多菈兩人冷靜了下來，都偷偷送了一口氣。就在這時，在琉琉西的背後，突然學出現了不少綠色的光粒子，並開始聚集起來，變化成了一個輪廓看起來是幼小少女的光團。
歐理維爾和迪婭多菈最先反應過來，朝著那有著人形輪廓的綠色光團低垂著頭跪了下去。
對於她們來說，這樣做是理所當然的。
因為，在她們面前顯現的毫無疑問是羽古多拉席爾的意志化身。
不久，這羽古多拉席爾的化身便有了明確的外貌，數不清的巨大花朵纏繞在身上組成衣服，雙肩和鎖骨就這樣大大方方的露出來，小小的雙腳上並沒有穿上任何東西，就這樣赤裸著雙腳。
雖然羽古多拉席爾的服裝看起來像是模仿琉琉西身上所穿的巫女姬服飾，但其實應該是反過來的，是巫女姬們的服裝模仿了以人形姿態顯現時的羽古多拉席爾所穿的服裝。
雖然羽古多拉席爾是一棵高大的仿彿能將天地串聯起來的大樹。但她的這幅化身，看起來卻非常符合嬌小可愛這個詞。
圓潤的下巴和嬌小的嘴唇，翹挺的瓊鼻，大大的眼睛，這幅模樣，別說有二十歲，看起來就是個不滿十五歲的少女。
雖然少女的耳朵看起來像是樹精靈一樣又尖又長，但是在尖端處卻變化成了深綠色的樹葉。
深茶色的長髮就這樣一直垂到雙膝處，從少女的頭頂到身體的各處，燦爛地綻放著櫻色、紅色、青色、黃色、綠色、紫色、白色、黑色、灰色、橙色等五顏六色的花朵，伸出來的手足上也纏繞著細細的藤蔓。
「大家都在呢。其他人也像以前一樣都來了。我很開心，非常開心哦！」
不只是外表，就連聲音和用詞遣句都非常的像小孩，但是琉琉西和迪婭多菈卻完全沒有震驚的神色。
成熟的羽古多拉席爾能夠跨越次元容納數個宇宙，但對於扎根在這顆星球上的羽古多拉席爾來說，要成長到那種程度還需要非常久的時間，現在的恩特·羽古多拉席爾在她的種族中只能算一個小孩。若是將恩特·羽古多拉席爾的年齡以人類這一種族來進行類比，現在的她最多也就是一個只有十幾歲的小孩子。
「好了，大家都站起來，站起來。原本就在我的身體裡面了，無論是跪著還是站著······額，就算是躺著也沒什麼區別。所以就別在意這麼多了。」
說著恩特·羽古多拉席爾露出了笑容。這笑容，正是純潔無瑕的孩子會露出的笑容。
雖然恩特之森的住民們將她稱為「森林之母」，但他們若是知道恩特·羽古多拉席爾的化身和真實性格是怎樣的，肯定都會有一股微妙的感覺。畢竟她這過於幼小的精神年齡，再怎麼說也還沒到能被稱為母親的程度。
但即使如此，對於迪婭多菈她們而言，都是多虧了恩特·羽古多拉席爾她們才能像這樣生活，每天所吃的糧食也都是恩特·羽古多拉席爾的賜予。因此對於這獨一無二的存在，她們抱有著極高的崇敬。
但在這裡卻出現了一個問題，那就是恩特·羽古多拉席爾將她們當做朋友來對待。
雖然被如此溫和的接待不可能讓人覺得難受，但是就立場上而言還是讓人覺得有些困擾。
「那麼，就按您所說。但，羽古多拉席爾大人，有一事能否請您解惑？」
迪婭多菈站起來後說道。
「怎麼了，迪婭多菈？」
「琉琉西一直作為巫女修行至今，被您指定為巫女姬並不奇怪，但為何連我也一同被選為了巫女姬呢？儘管我對自己的三分力量有所自信，但我的適應性應該並沒有那麼高才是？」
迪婭多菈的疑問也是在場所有人的疑問。
迪婭多菈是整個恩特之森也屈指可數的強者，這是所有人都認同的。但強大並不意味著和恩特·羽古多拉席爾的適應性就好。
而且，恩特·羽古多拉席爾點名迪婭多菈還是最近的事情，為了這件事，籌劃祭典的各族族長和巫女們為了調整計劃和相關的準備都跑斷了腿。
「因為，迪婭多菈你呀，靈魂在最近突然就急速成長了哦。明明以前靈魂就很漂亮了，但是在最近靈魂卻變得更漂亮了。我可是被你給嚇了一跳啊，不過也同樣覺得很開心。看了你的靈魂，我馬上就明白了，要是現在的迪婭多菈，可以和琉琉西一起與我進行同調了。」
恩特·羽古多拉席爾慢慢的接近迪婭多菈。
「迪婭多菈，雖然我知道你去了邪惡的魔界，消滅了那裡的一個強大花之精，因此變得更加堅韌與強健。但是想要讓靈魂成長，這樣是不夠的。要是不遇到什麼美好到能將自己迄今為止的價值觀全部破壞的好事情，或是自己從某種絕境之中重新振作了起來，一個人的靈魂是不可能如此急速成長的。」
恩特·羽古多拉席爾看起來打心底裏感到不可思議的樣子，但同時也為迪婭多菈的成長感到開心，並為真心地為迪婭多菈祝福，然後羽古多拉席爾就這樣把自己的小手放到了迪婭多菈的胸前。
年輕，不，年幼的世界樹化身從迪婭多菈那豐滿而又柔軟的胸前，感覺到了心臟的跳動，這股跳動讓人感覺舒適無比，想要就這樣一直聽下去。（譯：確定不是因為奶子摸著太舒服？）
迪婭多菈看著觸碰自己的幼小世界樹，露出了慈母般的笑容，伸出自己的雙手，將其重合在了恩特·羽古多拉席爾的小手上。
「是的，我知道了非常美好而且美妙的事情。」
「哎呀！迪婭多菈，如果這個秘密不涉及你個人隱私的話，能不能告訴我啊？我很感興趣！」
哎，哎，看著不斷說著這句話興奮起來的恩特·羽古多拉席爾，迪婭多菈輕啟紅唇，用著只有她們二人從能聽到的聲音低聲細語地回答道，她的聲音聽起來非常的溫柔——但是卻充滿了熱情和以及一股發狂般的愛情。
「是戀愛，我遇見了我的初戀。」
「哎呀！迪婭多菈，那還真是美好啊！是這樣啊，是這樣啊，就是因為這樣你的靈魂才會突然成長啊。畢竟你所遇到的，就是如此美好的事情。不過就算這麼說，我也不清楚初戀到底有多美好就是了。」
「呵呵，您接受了麼？」
「是啊，完全能接受。哎，迪婭多菈，你的對象現在怎麼樣了？」
「他來了迪普古林。在這之後，他打算參加祭典，來露天舞台觀看我的表演。因此，我現在緊張的心臟好像都快要爆炸了。但是同時，我的內心也激昂無比。」

「是這樣啊，那我也要好好努力了。當然，我平時也是很努力的，這次就特別的更加努力，不把迪婭多菈的好地方展示給那個人看可不行呢！」
看著雙臉染上紅潮的恩特·羽古多拉席爾，迪婭多菈帶著高興和害羞的心情說著「真是的」並露出了微笑。
琉琉西和歐理維爾她們，儘管沒有聽到迪婭多菈和恩特·羽古多拉席爾之間到底說了什麼，但是看見她們崇敬的世界樹看起來心情變得非常好以及年幼的化身那副幹勁滿滿的興奮樣子，她們的內心也變得平靜了起來。
看起來，迪婭多菈與恩特·羽古多拉席爾不僅僅只是適應性和靈魂的強度上的適合，就連性格方面也沒有問題，非常合得來。畢竟對於性格就如同外表一樣還是小孩的恩特·羽古多拉席爾來說，迪婭多菈這種願意照顧她人的性格的確相性很好。
儘管按照以往各屆祭典的召開時間計算，這次祭典的召開意外早了很多。但因為巫女姬有兩人，同時恩特·羽古多拉席爾的心情非常好，所以眾人內心中的不安都被消除了。
連續不間斷舉行數日的這場祭典，可以確定規模會比比以往歷屆的都要大，質量都要更高，琉琉西這樣想著，放寬了內心。
但是，就在這時異變突起——
在恩特·羽古多拉席爾魔力最為濃厚的此處，在這個展開了數重針對從外部轉移魔法結界的神諭之間，卻從天花板附近毫無徵兆的出現了四道呈四方位置的閃電。
誰也不能責備她們的疏忽。
除了利用她們這份疏忽而讓自己奸計成功的人以外，沒有人能責罵她們。
像是「大意了呢，這群笨蛋」這樣的話。
在黑色閃電的源頭處涌現出了無數黑色的粒子，這些黑色粒子聚集在一起形成了一道不斷回旋著的大門。
對於這突發的異常事態，最先有反應的是迪婭多菈和歐理維爾以及恩特·羽古多拉席爾。
迪婭多菈迅速的上前將恩特·羽古多拉席爾護在自己身後，歐理維爾也同時向前，將琉琉西拉了過來，讓身後的護衛們保護她。
就在迪婭多菈她們準備應戰之時，閃耀著黑色光芒的大門門軸發出了聲音，緩緩地打了開來，隨後從中流出了數不盡的瘴氣，連世界樹都無法在短時間內全部淨化。
然後迪婭多菈她們聽見了一個青年的聲音，聲音中充滿了愉悅。這是一道毫不掩飾自己的醜惡慾望，讓人從心底裏感到發寒的聲音。
「果然啊。雖然暫且年幼，但這世界樹依然如此地惹人怜愛。特地和人類定下契約還是有價值的。」
被猶如實質一般的瘴氣纏繞著，三個人影出現在了神諭之間。
這三個人便是與吞噬世界樹的惡竜末裔聯手，同時與人類歷史上最強大也是最邪惡的魔法使巴斯托雷爾定下契約，終於如願以償來到了世界樹內部的惡魔王子卡班以及他的護衛。
卡班沒有在意迪婭多菈和歐理維爾，完全將她們當做了路邊的石子，眼神的焦點全部聚集在恩特·羽古多拉席爾身上，而他的臉則因為喜悅而變得扭曲了起來。
帶著冷酷至極的笑容，頂著一張和人類一抹一眼臉龐的卡班，在其開口說話的瞬間，迪婭多菈便釋放出了大量帶有劇毒的花粉，將卡班和他的護衛們包裹了起來。
「嚯，這股味道，還真是熟悉啊。這不是葬送了拉芙拉西婭的傢伙麼。雖然你在人界算得上珍貴，但卻並不是我喜歡的類型啊。」
就好像出來度假一般，卡班的樣子無比悠閑。迪婭多菈再一次痛心的感覺到，對方並不自己能戰勝的對手。
「羽古多拉席爾大人，歐理維爾！」
被毒花粉所包圍的卡班三人，釋放出了殺氣，感受到這殺氣，迪婭多菈發出了悲鳴一般的喊聲，但也同時拼盡全力的發動魔法，保護恩特·羽古多拉席爾和歐理維爾。
卡班他們甚至都沒有發動攻擊，只是就這樣靜靜地站著釋放殺氣，就能讓人界的生物死去，甚至還能將其靈魂破壞，讓其永遠無法進入輪廻，進行轉生。這無形無色的殺氣與淡綠色的魔力充滿了整個神諭之間，不斷的對抗著。
「真是的，甚至都開始覺得拉芙拉西婭那樣的敵人變得可愛親切起來了。要是沒有上次那個蓋歐魯多程度的實力，都無法與他進行對抗。」
迪婭多菈所說的話，表明了現在的情況有多危機，她已經快沒有閑工夫去思考多餘的事情了。曾經把她逼到死地，千辛萬苦才打到的拉芙拉西婭和面前的這三個惡魔比起來，弱小的就像是可愛的花之妖精一般。
這些高次元的怪物，就算在人界現身時力量被壓制，但他們中的每一個人，都仍然有著能隨意決定一顆行星命運的力量和惡意。
「那麼，雖然珍貴但對我毫無價值的黑薔薇，以及那些螻蟻般的存在啊，趕快滾吧，我可不關心你們的死活。我的目標只有那邊的羽古多拉席爾。」
聽到卡班的話，在迪婭多菈發怒前，歐理維爾就大聲喊道。
「不行，迪婭多菈，羽古多拉席爾大人！」
「還真是乾脆的惡魔呢。對於你這種那麼性急的男人，只需要連同靈魂一起全部湮滅掉就好了！」
與無形無色的殺氣不同，一股紅色的魔力突然出現，沿著地板不斷的蔓延，很快就纏繞上了恩特·羽古多拉席爾那幼小的身軀。
「不要，不要，這個，好噁心。」
「哈哈哈，魔界是我的故鄉，而這些正是魔界的瘴氣。對於被純淨大氣包圍，在人界成長的你來說，應該相當污穢吧。不過這樣就好，你很快就會習慣的。看著人界之物墮落至魔界，受到無盡的折磨與苦痛，也是一大樂趣。」
恩特·羽古多拉席爾被纏繞在身上的魔力拖拽向純黑色的大門。
在黑色大門的另一頭，就是邪神和惡魔四處橫行的大魔界。換句話說，大魔界就是卡班的大本營。要是被帶到了那種地方，恩特·羽古多拉席爾就再也不可能回到人界了。
「你、這、家、伙，休想如此輕易的就把羽古多拉席爾大人給······」
就在恩特·羽古多拉席爾快要被拉入黑色大門的瞬間，迪婭多菈不顧自身情況，伸出藤蔓把恩特·羽古多拉席爾給卷了起來，就這樣一同被拉進了漆黑的大門之中。
「羽古多拉席爾大人，迪婭多菈！」
崇敬、仰慕的恩特·羽古多拉席爾與自己的好友就這樣消失在自己的眼前，這一現實對歐理維爾產生了巨大的打擊，讓她發出了悲痛的叫聲。但卡班絲毫不在意，和護衛們一同轉身向著黑色的大門走去，姿態無比的隨意。
「雖然把多餘的東西也一起帶了過來，不過無所謂，就在羽古多拉席爾的面前拷問那傢伙吧，感覺在兩個人身上都能看到有趣的反應。你們的話······是呢，就來當這些傢伙的對手吧。再過一會那條蠢竜也要來，你們的死活就要看你們自己了，好自為之吧。」
卡班三人就這樣消失在了漆黑的大門之中，同時一股顏色漆黑，甚至還在不斷冒泡的淤泥出現在了神諭之間。隨後，在神諭之間不斷蔓延的黑泥之中，無數的惡魔開始慢慢浮現。
在失去了恩特·羽古多拉席爾與最強戰力迪婭多菈之後，歐理維爾和琉琉西她們還要與無窮無盡的惡魔進行戰鬥，處境無比危急。


（視角轉換至多蘭處）
和迪婭多菈分別後，我和賽莉娜跟著菲歐和瑪露的指引，不斷的享受著熱鬧的祭典，每個人的臉上都帶著笑容。
生活在恩特之森中的各種不同的種族以及從森林之外來的人們不斷的從我身邊經過。自轉生成人類以來，我還是第一次體驗到像迪普古林這樣規模這麼大，這麼熱鬧的祭典。
雖然龍宮國也相當的熱鬧，但我只去過城堡裡，沒有去市區逛過，因此沒辦法將兩者進行正確的比較。
但這兩個地方都是因為有著恩特·羽古多拉席爾或水龍皇龍吉的庇護，才能如此的和諧與繁榮。
雖然這棵羽古多拉席爾還非常年幼，但卻能人森林裡的住民們和諧相處，真是讓人感到欽佩。
不過，我有些許不安，羽古多拉席爾所釋放出來的魔力，無論是從數量還是靈格的方面上來講，都是人界之中最高級別的。不只是對生活在人界的各種生物來說非常有裨益，甚至對精靈和妖精這種其他世界的存在來說，也是非常好的糧食與資源。
「哼姆，羽古多拉席爾的魔力麼······對於人界的人來說，正可謂是恩賜啊，就怕被別有用心的人給盯上。不過為了應對這些惡意，四處都設置了遮斷空間的結界，應該······」
就在我將不安從嘴裡說出來的時候，異變突起。
羽古多拉席爾中心處的次元壁突然開始震動和搖晃起來，這股騷動沿著靈脈四處傳播，感覺到異變的精靈們開始吵鬧起來。羽古多拉席爾宛如被暴風席捲一般，枝葉在劇烈的搖晃著，看起來就好像在不斷地慘叫。
「難道，就因為我剛剛立的flag······」
我嚴肅的看著羽古多拉席爾，而在我的身邊，菲歐和瑪露還有賽莉娜也一樣不安的看著羽古多拉席爾，觀察著世界樹的異變。
不只是菲歐她們，整個迪普古林的人們，全都離陷入恐慌僅有一步之遙，能感覺到氣氛非常的凝重。
「難、難道是因為多蘭說了奇怪的話才？」
是想起了在科爾涅普被海魔襲擊時的事情麼，賽莉娜轉過頭來看著我，發出了疑問。
「不，又不是我叫人來搞事的，雖然這前面的確發生了什麼事情······」
就在我們說著話的時候，羽古多拉席爾被一股深紫色的瘴氣包圍，仿彿要與這些瘴氣對抗一般，羽古多拉席爾渾身散發出黃金色的光芒，與紫色的瘴氣進行著拉鋸戰。
碧藍的天空中，同樣生成了深紫色的雲塊，不斷的向著四周擴散。不一會，陽光就被完全遮擋，周圍都陷入了黑暗。
「多蘭，到底發生了什麼事情？！」
「從異界而來的入侵。目標是羽古多拉席爾與其產生的魔力。雖然我在來之前稍微調查了一下有沒有異變的前兆，但看來這群傢伙在這個時刻到來之前，一直都潛伏的很深啊。」
我一邊回答著賽莉娜的問題，一邊看向在羽古多拉席爾上空生成的一道巨大裂縫。
我從那之中感覺到了加害羽古多拉席爾之人的氣息。
「在來之前想起了和勇者們相遇時的事情我還以為是怎麼了呢······原來如此，我記得和他們相遇的契機，就是你啊，尼祖厄庫。」
我將眼睛變換成了閃耀著光芒的虹瞳，然後從裂縫中，我看見了一只噴吐著毒霧，背後有著雙翼的邪惡之竜，尼祖厄庫。

譯：尼祖厄庫是種族名，下文大部分的尼祖厄庫都指的是這只龍的種族，怕各位搞混在這裡特別提一句。

前世時，為了打到一只尼祖厄庫，阻止他啃食某一棵羽古多拉席爾，有一隊被稱為「七勇者」的人尋找到了我，並祈求我的幫助。這就是我和勇者們的初次相遇。
那個時候的尼祖厄庫被七勇者們打敗，肉體被他們分裂成了五塊，然後被我完全消滅，沒有留下一絲痕跡，所以這應該是另外一只個體。
尼祖厄庫，原本應該是生活在冥府某一塊領域中的貪食惡竜，是匹敵眾神的存在。低等級惡魔王和下位邪神這種程度的傢伙，要是敢向尼祖厄庫發起挑戰的話，反而會被其吃掉。
雖然這些傢伙和眾神一樣，想要出現在人界的話，就會被限制權能和力量。但若是尼祖厄庫以完全姿態顯現，別說是這在這棵羽古多拉席爾，整個行星都會被他吞噬殆盡，迎來滅世之災。
雖然不知道有怎樣的因果，但現在這裡有我在，那就是最大的幸運。就如同過往一般，由我來當你的冥界引渡人吧。
「貪食的偽竜啊，你就為盯上這個世界的羽古多拉席爾這件事而懺悔吧。」
沐浴著尼祖厄庫釋放出來的氣息，包含菲歐在內的所有迪普古林的人們，身心都陷入了異常之中。而我則讓身體內古神竜的力量高漲起來，並在內心向著這個打擾節日雅興的蠢貨發誓，一定要馬上將他給幹掉。
在恩特·羽古多拉席爾上空產生的次元裂縫進一步擴張，迪普古林的人們從中看到了兩個可怕的巨大火球。
這兩個可怕的巨大火球，正是尼祖厄庫這吞噬世界樹的貪食魔物的兩只眼睛。而此刻這兩只眼睛中，充滿了食欲。
尼祖厄庫的豎瞳閃耀著光芒，帶著凌辱一般的視線，看著在他身下的羽古多拉席爾。
聚集在迪普古林的人們從次元的裂縫中看到了這位於神魔領域的存在，所有人的精神都在一瞬間受到了衝擊，靈魂就這樣變得衰弱起來。
迪普古林的人沒有在看見尼祖厄庫的一瞬間被同時湮滅掉精神和靈魂，都是多虧了羽古多拉席爾盡全力保護他們。
羽古多拉席爾在尼祖厄庫出現的瞬間便將體內的魔力和靈力全部釋放而出，抵消著尼祖厄庫身上所放出的邪惡氣息。
「多蘭，你剛剛說了尼祖厄庫對吧？！那個吞噬羽古多拉席爾大人他們的惡竜？！」
對於我口中所說的尼祖厄庫之名，菲歐是反應最快也是最大的，她的臉上現在擠滿了不安和恐懼，淚流不止。
對於敬愛羽古多拉席爾，並與其一同生活的這位樹精靈少女來說，尼祖厄庫是與世界樹為敵的存在之中，最令人恐怖的存在。
不，不只是菲歐。
周圍的樹精靈們在聽到尼祖厄庫這個名字之後，甚至都不問真偽，每一個人都散發出恐怖、不安、憤怒、憎惡等情緒。
對於他們來說，羽古多拉席爾是不可或缺的存在，而威脅著羽古多拉席爾的存在就在眼前，他們會對這個掠奪他們敬愛存在的敵人感到憤怒也是當然的。
「是的，雖然這傢伙還相當年輕，但能在人界顯現，說明這傢伙擁有著很強的實力。而且，恩特·羽古多拉席爾還太年幼了，要她單獨與尼祖厄庫進行對抗——雖然說出來可能會讓你們不安，但情況相當不妙。」
「怎麼會這樣······那我們快點去巫女姬大人所在的地方吧！只要巫女姬大人盡快舉行儀式，就能和羽古多拉席爾大人進行同調，產生出大量的魔力。只要使用這些魔力，應該就能戰勝尼祖厄庫了對吧，多蘭！？」（譯：直接喊男主上去揍啊）
「哼姆，將羽古多拉席爾介入天地靈脈，向世界釋放的這些魔力拿來對付尼祖厄庫的話，的確可以打到他。但是，羽古多拉席爾的魔力對於尼祖厄庫來說可是求之不得的美食啊。要是產生出來的魔力被尼祖厄庫吞食的話，反而會加強尼祖厄庫的力量。」
「但是，這樣下去的話，羽古多拉席爾大人就······！」
「而且，好像出現的並不只有尼祖厄庫。」
雖然在此之上還要再告訴他們一個雪上加霜的噩耗讓人感到痛心，但就算是在現在，尼祖厄庫周圍的空間也在不斷的扭曲著，產生出各種各樣凶惡的存在。
在扭曲的空間中出現的存在，雖然實力不如尼祖厄庫，但也都有著令人懼怕的力量，而這些存在此刻全都帶著惡意現身於這個世界。
「多蘭，那、那個，難道是魔兵？！怎麼會······我們之前不是才擊退過蓋歐魯多他們麼？！」
「哼姆，不只是魔兵，魔兵只是被製作出來用於戰鬥的人偶，是沒有靈魂的被造物——也就是惡魔們經常會用的棄子。但那裡出現的還有下級或者中級惡魔。除此以外也出現了不少下級偽竜。我曾經聽說過尼祖厄庫一族生活在冥界的某一地方，而且聽說他們有人後來移居去了魔界。所以看起來要和那些臣服于尼祖厄庫的惡魔也打上一架了。」
因為尼祖厄庫的力量而屈服於他的惡魔與偽竜，實在是太多了。
有的長著一個山羊頭，背後有蝙蝠的翅膀，穿著用無數的頭蓋骨製作而成的破爛衣服，有的皮膚是藍白色，頭上長著牛的角，還有的腦袋是一個巨大的蛙頭，但是卻有著一個美女一般的上半身。這群怪物的種類之多，外貌之奇怪，感覺都可以做一個圖鑑了。
數個月之前，在萬物復甦的春季還未到來之時，恩特之森的西部曾經被蓋歐魯多這一魔界四騎帶兵侵略過，與蓋歐魯多進行戰鬥的記憶，至今依然非常鮮明。
看菲歐的樣子，應該是覺得這一次入侵比起上次還要更加危險，所以覺得自己已經無可奈何了吧，她會這樣想也是沒辦法的。
上一次的時候還有著其他種族的援軍這一名為希望的存在，但這次敵人是直接襲擊了位於恩特之森心臟部位的迪普古林。而且還出現了尼祖厄庫，這一將他們恩特之森的命脈——羽古多拉席爾當做食物捕食的傢伙。
雖然上次襲來的敵人，無論是數量還是戰力都非比尋常，但與這一次相比，卻還要遜色一籌。
不只是菲歐，在迪普古林的每一個人心中都蔓延著無可奈何的絕望。
但是，我在這裡。
「魔兵四千，下級惡魔大概有三千，中級惡魔有五百左右吧，沒有上級惡魔。數量並沒有想像中的那麼多，也就是說，他們構築的通道還沒有大到那種地步麼。」
看著鋪天蓋地的惡魔們，菲歐和瑪露甚至連賽莉娜都露出了恐怖的神色。可不能就這樣看著我的友人與我可愛的蛇女精神就這樣被壓垮。
對這些不懂風情擅自前來的客人，這裡就讓我也自作主張地請你們快點退場吧。
儘管我慣用的長劍不在腰間讓人有點寂寞，但是，這不會對我排除這些傢伙產生任何影響。
「菲歐，你們這裡肯定有為了預防這種緊急情況而準備的避難所對吧？你快點和瑪露一起先回到阿杰拉姆先生他們那，然後帶上全部人一起去避難所。」
我們貝倫村的村民們可是從懂事起就一直在被教育當發生突發性的襲擊和自然災害時該怎麼做的。
雖然這裡也有可能不會這樣做，但還是得先確認一下，畢竟菲歐不是在這個深綠之都出生成長的，無法知道她清不清楚發生緊急情況時到底該怎麼做。
「那、那個，迪普古林有建造地下通道，當遇到外敵入侵時，可以使用那條通道逃向外面。但是，我們完全沒有想到羽古多拉席爾大人會是第一個被盯上的。不過，只要我們的戰士還在就肯定不會讓他們得逞的。」
最優先的居然是保護羽古多拉席爾麼，但這次這些護衛卻完全被無視，敵人一上來就直接找上了羽古多拉席爾，這是樹精靈們完全沒有預料到的情況。
但是，現在並不是為了這些意外而哀嘆的時候。我們應該把這些失敗踩在腳下，將其作為我們前進的基石。
「哼姆，也就說，迪普古林的外圍反而比較安全麼。看起來，這群沒有禮貌的傢伙並不打算追到城外啊。」
就在我和菲歐說著話的時候，惡魔的數量也在不斷增加。很快的，這個都市純淨的大氣就完全被污染了。
出現的大部分惡魔，都是曾經在眾神之戰中戰敗的惡神們墮落到魔界之後新製造出來的。觀察他們存在的氣息，全都感覺不到存在時間非常長久才有的厚重感。
「要討伐闖進妖精與精靈之都的無禮者，果然還是使用精靈的力量更合適一點。」
竜種魔力從我的全身滿溢出來，周圍的空間之壁因此不斷地扭曲著，開始連接起其他的世界，注意到這一變化的菲歐她們露出一副吃驚的表情看著我。
只有賽莉娜的表情是一副「又來了」這般早就認命了的表情。
看起來就算是她也對我所做出的驚人之舉感到習慣了。
「於漂蕩的世界中回應吾之呼喚，將愚者們盡數葬送吧，偉大的精靈們！吹拂於虛無的奇跡之風，米拉涅托！將天地淨化之神水，托阿庫婭！無盡鼓動之大地，維阿斯！」

譯：這裡的幾個詞是是可以意譯的，一開始我還以為是咒語，原來是人名，早知就不查了，不過查都查了，還是貼一下原意吧，但最後一個原意我還是有些不確定，米拉涅托——ミラネート，ミラ是mira，指一顆名為蒭藁增二的紅巨星，ネート指風暴；多阿庫婭——トァアクア，トァ就我查到的釋義簡單來說就是因為地殻運動而上升的冰面，アクア就是大家都知道的阿庫婭，指水或水神；維阿斯——ヴァイアース，アース指地球或大地，ヴァイ查到了幾個詞，發現對應字母是vi，然後還查到了超級機器人大戰經常用這詞，對照了一下日文與中文譯名，發現可能指程度深的附加前綴，後面讓朋友查了查，居然查到了北歐神話，說vi有可能是指大地母神之類的神明所加的前綴，總之很迷。

在我念完咒語之後，在我們頭頂之上的空間中，突然出現綠、藍、茶三種顏色的波紋，並在在空間中不斷擴散著。隨後，三只由精靈界而來的大精靈就這樣出現在了人界。
由精靈神所產生的精靈王是所有精靈的王者，而大精靈則是精靈王之下，位格最高的精靈。
與眾神相同，為了不影響原本就生活在人界的居民，精靈界的存在顯現在人界時，權能和力量也會受到巨大的限制。但即使如此，他們所擁有的的力量也超越了人界所有的種族。
召喚大精靈的魔法，可謂是精靈魔法的高等奧義。而同時召喚出三只大精靈這種近乎不可能的事情，則只有我才能做得到。
身上穿著翡翠色的羽衣，有著一頭綠色長髮的少女，正是風之大精靈米拉涅托。

譯：羽衣，傳說神女身上所傳的衣物，特點就是又薄又輕，再次划重點。

將纏繞在身上的水流當做衣飾，就這樣渾身不著片縷出現的端整美女，正是水之大精靈托阿庫婭。
有著一身濃密的茶色毛皮，在毛皮之上覆蓋著苔蘚，甚至還生長著結有果實的果樹，最後出現的這個巨熊，便是地之大精靈維阿斯。
我所召喚出來的大精靈，就算是在眾多的大精靈之中也相當的出類拔萃，甚至有著匹敵精靈王的力量。尼祖厄庫與他的那群惡魔帶來的震驚還沒有完全消失，眾多的精靈們又被我所召喚出來的大精靈給再一次震撼住了。
其中最甚者當然是親眼看見我召喚大精靈出來的菲歐和瑪露。
被我召喚出來的大精靈們釋放出了壓倒性的靈壓，甚至就連感覺到這靈壓的惡魔們都出現了些許動搖。但是，在我身邊的賽莉娜卻和周圍的反應完全不同，她雙手抱了起來，帶著看開了的表情點了點頭。
「哎，畢竟是多蘭啊。一般的精靈使就算是賭上一生別說是召喚了，就連讓其傾聽一下聲音都做不到的大精靈，多蘭你居然可以同時召喚出三只，還真是厲害呀，嗯。」
「賽莉娜，你，知道自己在說什麼嗎？大精靈這種存在，就算只能召喚出一只也已經難於上青天了，但是現在卻······」
「菲歐醬，我啊，和多蘭在一起的時候學到了一件事，那就是啊，在這種時候，像這樣把腦袋放空才是正確的哦？」
「？」
看著抱有疑問的菲歐，賽莉娜宛如開悟的賢者一般，表情莊嚴而神聖，渾身散發出自信的氛圍，開口說道。
「‘因為是多蘭啊’這句話，可以解決一切問題哦。因為是多蘭所以沒辦法，因為是多蘭所以做得到。所以，就算多蘭同時召喚出三只大精靈也沒什麼不可思議的哦。要是不這樣想的，你們會一直為多蘭的行為感到震驚的，震驚到你們心累為止。」
呵呵，發出如此笑聲的賽莉娜，總感覺身上有著一絲看破紅塵一般的超然。

譯：接地氣的翻譯就是賽莉娜看起來無比佛系。

雖然我有自覺自己所做的大部分事情都無法用常識去考慮，但是我會這樣做，很多時候都是為了這世界的人們，沒錯，為了這個世界的人們，我的內心的確是按照打算去做的。所以要是有必要的話，我依然會像這次這樣義不容辭的使用我身為古神竜的力量。
「因為是多蘭······」
雖然對沉默無語的菲歐感到不好意思，但現在沒有說明的時間了。
在大精靈的威嚴與靈壓面前顫抖的惡魔們，再一次因為再度降臨至人界而興奮了起來，開始向著迪普古林降落下來。
「菲歐，雖然在你還沒緩過神的時候對不起，但這種程度的事情對我來說沒什麼大不了的。考慮到我所召喚出來的大精靈他們的屬性，應該不用說明什麼，接下來的事情，你看著就明白了。」
菲歐的臉上仿彿寫著「這麼可能看著就明白啊」，但還是別把著這個說出來了吧。
在我和菲歐對話的時候，在我頭上的大精靈向我搭話了。
「還真是粗暴的就把人給召喚出來了啊，但是沒想到來到這邊一看，居然······」
「實乃懷念至極，居然能被您主動召喚，再次拜見您此等偉大之龍的尊顏。」
「姆，沒想到居然轉生為人了啊。之前就覺得您是一位十分奇怪的大人了，沒想到居然會變成這種情況，還真是出乎意料啊。」
用著小孩子一般語調說話的是米拉涅托，用詞非常古風的是托阿庫婭，語氣非常嚴厲的是維阿斯。
他們三人都是我在前世認識的大精靈。說起來，今天這還是第一次為了戰鬥而把他們召喚出來啊。
對於當時我的來說，雖然大精靈或精靈王以及眾多的神靈都是我朋友，但每次有麻煩的時候，我都不借用他們的力量靠自己就解決了，畢竟這樣才是最快最便捷的辦法。


「見到了老朋友我也很開心哦。如果不是這種場面的話，我會更開心就是了。」
「我也是啊。自從你死了之後，都很久沒人來找我玩了。」
雖然米拉涅托和我說話的樣子沒有什麼奇怪的，但他並不是和人類一樣讓聲音在大氣中震動、傳播從而傳達自己的聲音的。
他們是直接在精神層面與我進行對話。
這種方式很接近念話，不過大精靈的精神過於強大，所以對於人類或者亞人來說是非常難以承受的。
只有像是羽古多拉席爾的巫女姬那般擁有優秀天賦的人，將精神與靈魂經過長年累月的磨練，才能夠承受住。
所以，就算是常年鑽研這一技藝的精靈使們，也很少有人能夠召喚大精靈。
「雖然難得的把你們叫了出來，但現在這情況沒辦法和你們悠閑的聊天啊。如你們所見，魔界那群慾望深重的傢伙想要加害這棵羽古多拉席爾以及這裡的居民。就讓我們來好好的教導一下他們，讓他們用靈魂來理解自己到底有幾斤幾兩吧。」
「雖是無妨，但無須吾等的微薄之力，您應該無敵於此世之間才對啊？」
「多阿庫婭，雖然你說的很對，但我要去收拾那邊的蠢貨。」
我伸手指向了站在裂縫處的尼祖厄庫，多阿庫婭他們三人隨著我的手指將視線移了過去。
理解了我所說的「笨蛋」是誰之後，大精靈們紛紛嘆了一口氣，他們嘆氣時的樣子看起來實在是有夠像人類的。
哼姆，說起來賽莉娜好像也做過類似的舉動。
「那應該是尼祖厄庫一族的吧，姆。我記得您和人類的勇者一起討伐惡竜就是這一種族的？」
「那傢伙好像打算把羽古多拉席爾醬給啃掉的樣子。所以，我們把那群惡魔給收拾掉就好了對吧？」
「你能肯幫忙真是太好了。在這個都市裡的人死掉任何一個，都代表著我的失敗，還希望你們能銘記於心。」
受點小傷的話還好說，但絕對不會讓任何人死亡的，我在內心如此發誓到。
「既然被你所召喚，那麼吾等定將竭盡全力。若非如此，屆時便無顏反對吾王的問責。」
說完，托阿庫婭在製造出無數水球，仿彿與這些水球互相呼應一般，組成托阿庫婭本體的水也開始產生波動。
「啊哈哈哈哈，是——啊。有那些傢伙在的地方，風都會變髒，讓人很不舒服啊。看我馬上將他們全部給吹飛吧！」
以米拉涅托為中心的小範圍內，開始刮起了風。儘管範圍小但這風的風速超越了音速，尼祖厄庫與惡魔們散發出來的瘴氣被這刮起來的颶風暴力地淨化著。
「如果是羽古多拉席爾遇到了危機，那就不是我們能夠置身事外的了。妖精們看起來也陷入了危機的樣子，您能把我們召喚出來真是太好了。」
維阿斯就這樣緩緩飄向高空，然後，在一無所有的空中，就這樣開始製造一根根的岩槍。
「那麼，這裡就拜託你們了，托阿庫婭，米拉涅托，維阿斯。」
聽見我的聲音，大精靈們一齊回答道。
「就請安心交給吾等吧！」
大精靈的回答還在我的腦內回蕩時，他們用高等靈力所製造出來的水、風、土就這樣朝著數量已然超過一萬的魔界軍隊襲去。
我能夠提供給這些大精靈的魔力，與一般精靈使提供給被召喚精靈的魔力相比，稱得上是龐大無比。再加上，是我作為基石讓他們顯現至人界的，這也成為了增強他們的力量要素。
惡魔與魔兵們認為當下最大的敵人便是大精靈，於是為了排除大精靈，一個個地開始出擊，使用他們那強韌的肉體以及被武器化的手臂或尾巴與大精靈們進行廝殺。
就算是壽命無比悠長的高等精靈，在有生之年也可能見不到一次的大精靈們，在菲歐和瑪露以及迪普古林的所有居民的緊張注視下不斷戰鬥著。
不一會，覆蓋著天空的漆黑瘴氣，就被風、水、地三種力量給全部吹散，露出了原本碧藍的天空。
在不抓住什麼東西穩定身體就會被吹飛的狂風中抬頭望去，米拉涅托上下翻飛，不斷操縱著颶風將惡魔們盡數剁碎，托阿庫婭製造出來的水流漩渦之中，有著無數被卷入其中的惡魔，這些惡魔統統都被撕裂成了碎片，維阿斯操縱著岩石，不斷的將惡魔們一個個擊碎，壓扁。
「那就是大精靈的力量麼······真是壓倒性的強啊。將這樣強大的大精靈隨便就召喚出來的多蘭還真是一如既往的多蘭呢。」
賽莉娜帶著頑強的精神說道。
「賽莉娜，你這是在表揚我麼？這裡的惡魔交給他們就足夠了，掀不起什麼波瀾的。賽莉娜，我們就先去羽古多拉席爾那邊吧。畢竟我們沒辦法斷定他們沒有入侵到羽古多拉席爾的內部，也不知道有沒有尼祖厄庫以外的人瞄準這個機會打算趁火打劫。」
因為我和維阿斯他們是直接通過精神對話的，所以賽莉娜他們並不知道我們之間說了什麼。
尼祖厄庫的話讓我的分身去做他的對手就行了，身為本體的我就去保護羽古多拉席爾好了。
畢竟若是有其他的尼祖厄庫察覺到羽古多拉席爾的氣息而趕來的話就麻煩了，而且還有可能引來其他的魔物和邪神，造成多餘的麻煩。
「多蘭，等一下啊。我也要去羽古多拉席爾大人那邊，好歹我也是被恩特之森養育的樹精靈！」
聽到我要去羽古多拉席爾那裡後，菲歐露出了拚命的表情說道。
「雖然我知道你在想什麼，但你並不是戰士。更何況······雖然這樣說可能會打擊你，但你並不是魔界惡魔們的對手。」
「但、但是！」
「你還快點和阿杰拉姆先生他們會合，讓他們安心下來吧。而且還有瑪露呢，你別告訴我你想把瑪露也帶上戰場吧？」
聽到我說的話，菲歐才察覺到自己的肩膀上還站著一個正在顫抖的小小友人，馬上轉過頭看了過去。
瑪露是我們之中最不能上戰場的典型，就算我再怎麼努力不讓犧牲者出現，也不可能說危險就已經完全被消除了。
「要是菲歐說要去的話，瑪露我也會跟著一起去的。雖然很害怕，但如果是跟多蘭和菲歐一起的話，我沒事的！」
雖然瑪露說話的時候滿臉都是笑容，看起來沒有任何異樣，但顫抖的嬌小肩膀與不斷震動的翅膀暴露了她的內心，瑪露明顯非常的恐懼。
看見這樣的瑪露，菲歐便不再堅持下去了。她放鬆了肩膀，露出笑容，溫柔的抱起了這嬌小的友人。
「對不起啊，瑪露。我盡是考慮自己，完全忘了你的感受，作為一個朋友真是不合格呢。多蘭，那我就先先帶著瑪露去叔叔那裡了。賽莉娜，請你別太勉強自己了。雖然我覺得多蘭和賽莉娜就算以惡魔為對手也沒有任何問題，但還是不希望你們受傷，所以請你們務必小心。」
「菲歐······」
瑪露眼睛濕潤的看著自己的朋友。
「沒關係的，瑪露。想去羽古多拉席爾大人那裡的想法，只是我的任性而已。」
賽莉娜看著重要的兩個朋友，露出溫柔的笑容，把手放在菲歐的肩膀上安慰道。
「明明難得的祭典卻出現這樣的事情，但是沒關係的，我們現在就去把那群沒有禮貌的傢伙全部都痛扁一頓，把他們全部揍回老家，然後祭典就能重新開始啦。你們就想著祭典重開後的樣子，好好的等我們回來哦。」
「謝謝你，賽莉娜，我會這樣做的。還有多蘭，雖然你們有歐理維爾大人的許可證，但現在情況緊急，不知道著許可證到底能通用到什麼地步，如果你們做出一些可疑的舉動有可能會被當做危險人物給拘禁起來，所以還請多多注意自己的言行。」
「明白了。畢竟就算是為了貝倫村，我也希望能與恩特之森的住民們友好的長久來往，所以我會注意這方面問題的。」
于菲歐分離後我的和賽莉娜，向著閃耀黃金色光輝不斷中和瘴氣的羽古多拉席爾出發了。
就這此時，我們頭頂上的尼祖厄庫也在次元裂縫中慢慢的踏出了腳步，一步一步的向著這邊的世界走來。
他那充滿了貪慾的黃金色瞳孔中，只映照著對他而言最至高無上的美食——羽古多拉席爾。
宛如蛇一般修長龐大的身軀被深紫色的鱗片覆蓋，而在鱗片上則分散著無數橙色、黑色的斑點，在強壯四肢的四肢上有著巨趾。在前肢的根部附近還有著紅色的皮膜，這皮膜便是他的翅膀，看起來宛如巨大化的蝙蝠。

我一邊朝著羽古多拉席爾前進著，一邊從靈魂中生成魔力，構造和尼祖厄庫體型一般大的白龍分身，而且我是在那傢伙的背後——也就是魔界之中構造分身的。
尼祖厄庫現在是被食欲驅使著而行動的，但突然之間，在他的背後出現了一股和他非常相似但又似是而非的強大力量。就算尼祖厄庫再怎麼想吃掉世界樹，他也無法無視這股力量，於是他打算轉過身去。
「你發覺得也太慢了吧。」
但我的分身在尼祖厄庫轉過身之前就這樣抓住了尼祖厄庫的脖子，為了讓他遠離次元裂縫，我把他向著身後的魔界扔去。
「吱呀哇？！」
尼祖厄庫無法反抗我的臂力，就這樣被扔了出去。
就這樣飛行在魔界天空尼祖厄庫，在粉碎了好幾座漂浮在空中的浮島之後才終於停了下來。
我為了阻止魔界和尼祖厄庫的瘴氣繼續流入迪普古林，用新製造出來的空間將迪普古林上空的空間裂縫給填補上之後，才開始觀察周圍的環境。
「哼、姆。大概是在大魔界的外部區域吧。要是我的記憶沒出錯的話，這裡應該是住著很多惡魔的領域。」
在我的周圍有好幾個魔族們拿來居住的浮島，這些浮島都是惡魔們製造出來的。而在這些浮島中最巨大的那個浮島上面，尼祖厄庫那股臭味特別的濃厚。
那就是尼祖厄庫的巢穴麼，這附近應該就是他的勢力範圍了吧，也就是說這些惡魔是他的手下。
「嘎吱，嘎吱，嘎吱。」
尼祖厄庫將埋入土裡的大半個身體拉了出來，離開了浮島，用蘊含著敵意和殺意的視線看著我，發出了宛如切割金屬一般的聲音。
我悠然的看著尼祖厄庫卷起了他那長長的身體，將翅膀張開到最大限度，擺出一副最高等級的警戒與恐嚇姿態。
要是在大魔界逗留太久的話，可能會讓卡拉薇糸發覺到我的存在，所以還是速戰速決吧。
「你是誰？居然在我即將把世界樹抽皮剝筋，吃肉喝血的至福時刻打擾我······還真是不知死活。吾名為尼祖厄魯，乃是吞食世界樹的蛇竜——尼祖厄庫一族之人，你就為你膽敢招惹我而感到後悔吧!」
尼祖厄庫——不，尼祖厄魯那牙齒緊咬著的嘴中，不斷有著深紫色的毒氣隨著呼吸被吐出，看來他現在的確很憤怒。
「要說是誰不懂禮貌先動手的，那可是你們才對。居然在恩特·羽古多拉席爾為了慶祝豐收而舉辦祭典之時，頂著張那麼丑的臉闖進來，還說要吃掉恩特·羽古多拉席爾······就算你現在道歉，我也不會原諒你了哦。」
像這樣與尼祖厄庫一族之人對峙，讓我想起了以前在太古時代與七勇者一起同尼祖厄庫戰鬥時的記憶。
那時的那個尼祖厄庫可是屬於尼祖厄庫的直系後代，是一個位格相當之高的強大個體，若是在人界顯現，甚至可以影響到眾多不同的次元。
「原諒？原諒我？真是愚蠢至極，我有什麼必要祈求他人原諒我。世界樹全都是我們尼祖厄庫一族的食物。而依靠世界樹而活的傢伙們，也理所當然的是我們的食物。向食物祈求原諒這種事情在哪個世界都不可能存在。雖然你看起來好像是一只挺有力量的竜，但我會吃掉你的心臟，讓你轉化為我的一部分。」
哼姆，還以為尼祖厄魯是在知道我是前段時間與海魔神對峙的古神竜多拉貢之后還膽敢向我發起挑戰的，沒想到他只是單純的把我看成了高位白龍啊。
為了防止尼祖厄魯逃跑我才沒在一開始就將我作為多拉貢特徵的虹瞳與六翼顯現出來，反而使用了這一副普通的白龍姿態，但沒想到會被這樣侮辱啊。
雖然這種體驗也說得上是新鮮，但看到對方那副嘚瑟的不行的表情，實在是讓人開心不起來啊。
「這就是你的回答麼，我確實聽到了。你這傢伙是叫尼祖厄魯對吧，你居然想啃食我所在世界的羽古多拉席爾，你現在可以為你打算做這件事情而感到後悔了。」
我將對方剛才對我說的話，用差不多的感覺還了回去，在聽到我的回答後，尼祖厄魯的怒氣好像終於到達了極限。
尼祖厄魯張開了滴著黃色毒液的雙顎，向我咬來。
「咕哇wryyyyyyyyy！！」
將彌漫在大魔界空間各處的以太和魔素當做推動劑，尼祖厄魯瞬間加速來到了我的面前。
「狗叫的還真是響亮啊！」
向著伸出雙鄂打算咬住我脖子的尼祖厄魯，我握緊了右手，全力的向著他的側臉打去。
被我的打中之後，尼祖厄魯嘴中不斷噴吐著夾雜牙齒的黑色血液，又一次向著遠方飛去。
「和以前戰鬥過的尼祖厄庫相比，還相當的年輕啊。要是那次出現的是這種程度的敵人，七勇者應該就不會去找我幫忙了吧。」
「不過這種程度的話，連活動筋骨都算不上」這樣想著，我張開了翅膀，向著被吹飛的尼祖厄魯飛去，作為追擊想再打上一發。
但尼祖厄魯在中途就重新穩定住了身體，他重新看向我的眼中，殺意正在不斷的高漲著。突然他的肺部和咽喉一瞬間膨脹起來，然後向我噴出了一股深紫色的劇毒吐息。
「被劇毒侵蝕吧！」
噴射而來的劇毒吐息宛如洪水一般撲面而來，阻擋住了我全部的視野。
但是，我毫不在意的繼續向前衝去。
尼祖厄魯完全沒有想到我居然會從正面突襲，瞪大了眼睛看著出現在他正前方的我。
「就這點程度還想讓我中毒？你還是太年輕了，小伙子。」
「嗚噢噢！？」
尼祖厄魯馬上停止噴吐劇毒吐息，一瞬間進行了回避，但突破了劇毒吐息的我比厄祖厄魯還要更快的就採取了行動。
尼祖厄魯瞬間把雙手舉到了臉前進行防御，但卻用雙手抓住了他的身體，然後就這樣抓著他的身體在空中不斷的進行旋轉旋轉，最後將他向著位於我們下方的一個浮島上丟去。
尼祖厄魯那巨大的身體與浮島發生強烈的碰撞，浮島承受不住這股巨大的力量被砸的粉碎，而尼祖厄魯就這樣無可奈何的繼續向下飛行，不斷與沿途的浮島發生碰撞，勢頭絲毫沒有減弱的跡象。
這場肉搏戰的持續時間極短，但尼祖厄魯被我擊碎鱗片卻能以萬為單位計算，他那滿嘴的利牙也有近一半被全部打斷，不斷有鮮血混雜著毒液留下。
「可惡······看來你不是一只普通的白龍啊！」
到了現在，冷靜下來的尼祖厄魯才終於明白我不是外表看上去的那麼簡單。
「雖然如此，但尼祖厄庫一族之人啊，就算你再怎麼冷靜，我和你之間的實力差距也不可能就這樣被你超越。」
尼祖厄魯重整態勢，快速的吸收著充滿了大魔界的魔素和以太，將其與體內的魔力進行融合，不斷地提升著自己的力量。
尼祖厄庫一族不擅長將魔力釋放出體外，但相對的，他們卻很擅長用魔力對自己的肉體進行強化以及將魔力直接注入對方身體內部破壞其血肉和靈魂。
看他的這幅樣子，是要再來一場肉搏戰了。
那麼我就用壓倒性的力量從正面將他擊敗，讓他好好的弄清楚，我的實力是完全碾壓他的。
因此，我也一樣讓分身不斷地吸收周圍的魔素與以太，但是，我分身所增長的力量卻是尼祖厄魯完全無法比擬的。
「讓我表揚一下你的氣勢吧，畢竟你也只有氣勢還算得上可以了，尼祖厄魯！」
尼祖厄魯看到我的力量在急劇增長，判斷再這樣繼續下去會非常危險之後，他停止了吸收以太和魔素，在自己的力量到達巔峰之前就向我飛了過來。
「咕啊嚕啊啊啊啊啊啊啊啊啊啊！！」
尼祖厄魯一邊發出奇怪的聲音，一邊在大魔界的虛空之中描繪著宛如流星一般的軌跡向我飛來，儘管他的樣子看起來挺淒慘的，但他臉上的表情卻充滿了一定要將我殺死的決意。
我向我的雙腕中注入了不會讓大魔界的空間崩潰的力量，也張開了翅膀向他飛去。
尼祖厄魯張開了他的雙鄂，向剩下的牙齒注入了所有的力量，瞄準了我的脖子。
雖然這一招比最開始那一招要更快更強，但對於曾經和身為最高邪神的原初尼祖厄庫們戰鬥過的我來說，這樣的攻擊，簡直弱的想讓我打哈欠，完全感覺不到恐怖。
「哈啊啊！！」
隨著我的咆哮，我的右拳打向了尼祖厄魯的鼻子，然後就這樣大大的回旋身子，用我的尾巴打出了宛如斬擊般的第二招。
「吱、吱咕啊啊啊啊！！」
尼祖厄魯甚至連後悔的時間都沒有就直接去世了，靈魂的核心被我完全消滅，永遠不可能被復活。
雖然對到達了神域的存在來說，這樣的結局稱得上讓人目瞪口呆，但與我為敵的傢伙們，末路基本都是如此。
「這樣一來，惡食之龍的威脅就消除了······不過，另外一邊的情況卻不太好啊，希望落空的預料成真了。」
我的本體正在向著羽古多拉席爾的內部進行突擊，但羽古多拉席爾的內部有無數直接被轉移進去的下級惡魔，甚至還有不少率領下級惡魔的上級惡魔存在。
我所處的大魔界這一帶區域有著不少惡魔，在我看來這些惡魔應該不是屈服於尼祖厄魯力量的部下······也就是說這些惡魔恐怕是尼祖厄魯的共犯。
「我的本體在那邊的話，就不可能有什麼問題了。但為了以防萬一，我還是應該回歸本體。不過在此之前······」
情況還真是變得好麻煩了啊，我這樣想著轉過了頭去。
同時，一道熟悉的聲音也響了起來。
「呀吼，多蘭醬！怎麼了，怎麼了？特地來到魔界，難道是想來見我的？真是個喜歡撒嬌還容易感到寂寞的孩子呢。」
這個我已經聽到無比厭煩，甚至想從記憶中消除的甜膩聲音，其主人正是司掌破壞與遺忘的大邪神卡拉薇糸，而且這次她又改變了自己的姿態。
這一次的她，有著白色的皮膚，這不是比喻，而是真正的白色，而她紅色的頭髮就這樣隨意垂在身後輕撫著肌膚，身上穿著的是妖艷誘惑的黑色皮衣，完全將她那凹凸有致的豐滿身材凸顯了出來。
卡拉薇糸那宛如雕刻出來的美麗藍色眼瞳中，充滿了喜悅，這是因為她覺得我來到這裡一定是來見她的。看見她那麼開心，我實在是沒法開口和她說這是錯的啊。
你看她現在因為過於開心感覺都要跳起舞來了，不，實際上是真的開始跳舞了。看著這樣的她，我強行壓下內心的躊躇，將真相說了出來。
「卡拉薇糸，雖然你這麼開心，但是很對不起，我來到魔界並不是為了見你。」
「欸欸欸~什麼嘛什麼嘛，居然不是來見我的~？切，那多蘭醬你是來幹什麼的呀。機會那麼難的，要不要來對我進行愛的告別啊？」
「因為尼祖厄庫的末裔來到了人界啊，我為了收拾他才特地製造了這個分身來到魔界。明明我來到魔界的時間那麼短，卻依然能找到我，只能說真不愧是你啊，卡拉薇糸。」
最後的那句話所含有的感情之中，比起讚嘆，更多的是呆滯。但卡拉薇糸卻將我說的話只往對自己有好處的地方進行解釋，露出了滿臉的笑容。
這傢伙還真的以為我是在真心表揚她了。
「啊哈哈哈，因為我的身體和靈魂，只要一接近多蘭醬就會像觸電一樣有感覺啊！多蘭醬你一來到魔界我就感覺到了，然後就像這樣馬不停蹄的趕了過來哦！然後呢，多蘭醬已經把事情解決完了？總感覺尼祖厄庫的末裔君有點可怜啊，就這樣一絲痕跡都不剩的被消滅了。所以，這裡的多蘭醬也差不多要消失了？」
卡拉薇糸將脖子歪成了直角狀向我問道，我否定了她的問題。
雖然製造這個分身時的目的是完成了，但是根據本體那得到的情報，這邊的我又有了一件要做的事情。
「不，雖然尼祖厄魯的事情已經解決了，但剛剛又有了一件必須要去做的事情。我接下來打算去做這件事。」
「耶！那，反正機會難得，就讓我這個卡拉薇糸碳來給最喜歡的多蘭醬幫個忙吧！」
卡拉薇糸臉上一副「沒有比這更好的主意了」的表情，唱著歌向我身邊靠近。
麻煩了，這種情況的話很難拒絕啊。因為卡拉薇糸沒有惡意。我和她來往了這麼久，所以能夠明白她剛剛的發言是完全基於善意的。
要是她說的話有一絲一毫的邪念，我就可以說「大邪神根本沒辦法信任」這樣的話來拒絕他。
「哼—姆······但我一個人也足夠了啊，雖然人手變多也不會有什麼問題······」
「啊哈哈，再怎麼說，我肯定也要比多蘭醬更熟悉魔界啦，而且生活在大魔界的人要更熟悉我哦。我卡拉薇糸的惡名在魔界可是不遜色於多蘭醬的哦~。」
但問題是，幫忙的是卡拉薇糸啊。要是這裡就這樣接受了她的善意，可能會讓事情變得更加麻煩起來。
我在內心中進行了無數次的權衡與思考，最終下定決心，由我來幫卡拉薇糸擦屁股，解決所有她惹出來的麻煩。
「是呢，我知道了，卡拉薇糸。就讓你助我一臂之力吧。但是，要注意分寸啊。」
「就交給我吧！能和親愛的多蘭醬一同工作，讓我的內心燃燒起來啦！」
你的這份幹勁，就是我最擔心的東西啊，身為我惡友的邪神啊。
「然後呢，多蘭醬要做什麼？接下來要去哪裡？還要做些什麼呢？」
「現在就開始和你說明。這邊的我要去做的事情是······」


（視角轉換至迪普古林的多蘭處）
在我的分身於大魔界和尼祖厄庫進行戰鬥的時候，身為本體的我和賽莉娜一起在迪普古林的街道上全力奔跑著。
在我們我頭頂上，我所召喚出來的大精靈們正在盡情的揮灑力量，驅逐者惡魔們。
惡魔以及偽竜並沒有去襲擊迪普古林的人們，而是拚命的攻擊大精靈們。
惡魔們在人界顯現時，力量一樣都會被壓制，而被我召喚出來的大精靈們則因為由我為他們供應魔力，所以這二者之間的戰鬥力有著無法跨越的差距。
哼姆，這樣的話，就算出現了蓋歐爾古這樣的墮神或上級惡魔，也能夠安心的交給他們了。不，要是蓋歐爾古這種等級的敵人，可能還是有點過於勉強了。
「多蘭，你知道現在羽古多拉席爾的內部變成什麼樣了麼？」
在我的旁邊以最快的速度全力滑行著的賽莉娜向我問道。
看起來賽莉娜是以我已經把羽古多拉席爾的內部情況給全部把握住了為前提才提問的。不過因為事實也是如此，所以也沒辦法反駁什麼。
就把這當做賽莉娜對我的信賴吧。
「在羽古多拉席爾的內部，各個種族的戰士們現在正以巫女姬為中心，不斷的與出現在內部的惡魔先鋒兵進行戰鬥。雖然現在還沒有出現重傷者與死者，但相當於羽古多拉席爾意識核心的地方，情況卻有些危急。恐怕能從那裡找到幕後黑手。只要將那些傢伙們的首級給砍下來，應該就能解決這一次的事件了。」
「原來不只是外面，裡面也發生了很嚴重的事情啊。」
賽莉娜那美麗的臉龐更加嚴肅了一分，她想到自己接下來要去的地方可能會遇到的威脅與危險，將自己的警戒意識提高到了最高。
只要有我給他的守護符，再加上她作為使魔我所能提供給她的輔助，現在的賽莉娜應該可以和上級惡魔進行戰鬥。
「雖然聽起來情況好像很嚴峻，但多蘭你對那邊的情況有什麼看法？」
「這些戰士不虧是被選來保護羽古多拉席爾的，實力很強，但最關鍵的羽古多拉席爾正被敵方頭目壓制著，沒辦法給他們提供輔助，情況還是很嚴峻的。敵人不僅是從魔界而來的，相性太差，而且數量上的差距也很明顯。要是這樣的情況一直持續下去，堅持不了多久。所以，我們要幫他們打破現狀。」
「要是多蘭不在的話就是非常絕望的情況了呢。還以為這一次又要借用迪婭多菈的力量了呢。」
和蓋歐爾古他們戰鬥的時候，好像也發生了類似的情況，就算賽莉娜覺得在意也是沒辦法的。
「羽古多拉席爾的危機換句話說也就是世界規模的危機。沒有必要將森林內外分開來考慮。而且，幫助羽古多拉席爾渡過這次危機，也能讓恩特之森的諸多種族對我們貝倫村抱有良好的印象。不過比起這些，我更生氣的是難得的祭典居然被這群沒有禮貌的家國給打擾了。總之，對我們來說，戰鬥的理由已經夠多了。」
「該說你是意志力強大，還是心思縝密呢······要是迪婭多菈也能聽到你剛剛說的話，估計會很安心的吧。」
「正所謂吃一塹就要長一智······但是，情況發生變化了。」
我一邊和賽莉娜說著話一邊觀察著羽古多拉席爾內部的情況，但目前看來，事態變得有些緊張了。
原本正在全力奔跑的我突然降低速度，最終停了下來，賽莉娜對我投來了不安的視線。
「多蘭？」
「沒時間給我們慢悠悠的跑過去了，就這樣直接跳躍進羽古多拉席爾內部去。」
是因為完全信任我麼，賽莉娜雖然看起來非常驚訝，但是卻沒有任何異議。


（此處視角轉換至羽古多拉席爾內部）
琉琉西與專屬於巫女姬的護衛們，正背靠著正對神諭之間入口的牆壁，不斷的與襲來的惡魔們進行著戰鬥。
巫女姬的專屬護衛們，都是從恩特之森各種族中精銳中選拔出來的精銳中的精銳。而有五個人，就算是在這群精銳中也格外的出色。
使用四肢上的利爪與尖牙，將魔兵和惡魔們四分五裂，有著灰色毛髮的狼人名為烏魯卡，有著茶色毛髮，如同閃電一般迅疾的虎人名為迪凱路，如同隱居森林之人一般，身上穿著注重輕便裝備的樹精靈少年名為拉坦塔，雖然拉坦塔身上所裝備的防具沒有任何一件是金屬制品，但在森林中的眾多植物裡，也有很多植物的強度並不輸於鋼鐵。
除了拉坦塔以外，還有兩位服飾和琉琉西非常相似的巫女。
其中一人看起來和琉琉西年紀相同的樹精靈少女，她是同時負責照料與護衛琉琉西的精靈使克艾露。在她那有些削瘦的臉頰與脖子還有手腳外露的皮膚上，能看到許多複雜的紋路，而在她周圍則漂浮著幾個她所召喚出來的風和水的精靈。
然後，在這五人之中，實力最為強大，屠戮了最多敵人的最後一位是誰呢？沒錯，她便是伽羅瓦魔法學院的學院長，恩特之森的重要人物，歐理維爾。
有傳言說亞克雷斯特王國的建國與歐理維爾有著極其深厚的關係。而此時此刻，這位女性高等精靈面對著將她們包圍的侵略者，冷酷的眼中毫無動搖與焦躁之色，實在是稱得上是女中豪傑。
雖然其他也還有十名左右的戰士，但能在戰鬥中壓制惡魔以及上位魔兵的只有以歐理維爾為首的這五名精銳。
雖然琉琉西也擁有著大量的魔力和極高的精靈使天賦，要是加入戰線的話，能成為極大的助力。但她此刻正在與羽古多拉席爾進行同調，阻止惡魔們繼續轉移到羽古多拉席爾的內部，因此無法讓她成為直接性的戰鬥力。
戰況無比激烈，但隨著時間的流逝，戰士們的臉上的疲倦之色越來越濃，看著這情況，歐理維爾張開嘴，開始咏唱屠戮敵人的言語。
歐理維爾所使用的並不是將風之精靈其給存在召喚出來的那種的精靈魔法，而是將精靈的權能顯現在這個世界上的高位精靈魔法。
「風乃刀刃，風乃鐵錘，風乃箭矢，風乃裁決裁，風乃死亡，風乃汝等之敵，風殺刑！」
因為歐理維爾的技術，導致咒語的大部被簡化，因而以極快的速度被使用的出來【風殺刑】，其效果則正如咏唱的內容一般，化作無數武器的狂風向著四周的敵人襲去。
混入了精靈與歐理維爾魔力的狂風，將魔力抗性遠遠高於人界生物的魔兵以及惡魔們全部撕裂，擊潰，貫穿，僅僅一瞬間就擊殺了十幾個敵人。
以琉琉西為中心，在陣型最內側的是類似克艾露那樣的精靈使以及弓箭手們，而在最前線的則是拉坦塔或烏魯卡這樣的戰士，他們組成一道圓形的牆壁，將襲擊而來的惡魔們不斷擊退。
負傷的人會馬上脫離前線來到圓陣的中心處，讓精靈使或魔法使治癒傷口，然後再馬上返回前線。
「可惡！無論消滅多少這些傢伙，還是會一個接一個衝上來，根本沒完沒了啊。」
虎人迪凱路看著不管完全沒有減少跡象的敵人，口中說出了有些焦急的話語。
但就算如此，他也沒有鬆懈，躲過了一個山羊頭惡魔揮舞的長柄斧，用自己那自豪的利爪將這惡魔從腹部到下顎間劈開，然後在一拳將腦袋給狠狠的敲碎，看起來他作為戰士的力量的確是超一流的。
使用短劍與短弓還有飛刀與小型魔兵戰鬥的拉坦塔對迪凱路說的話表示同意。
作為琉琉西專屬的戰士，這名樹精靈的少年從小邊陪伴在琉琉西身邊和她一同成長。雖然這位名為拉坦塔的年輕戰士經歷了無比嚴格的訓練，但與獸人相比，樹精靈在體力上天生就有非常大的劣勢，所以此刻的他，早就因為不斷積累的疲勞導致動作變得遲緩起來。
無論如何鍛鍊，不同的種族在從根本上就有著致命性的差距，這是無法彌補的。
「正如迪凱路所言。這樣下去的話我們遲早會因為力竭而被擊潰。在此之前必須找到辦法突破這困境離開這裡。」
作為青梅竹馬，作為守護巫女姬的戰士，對於拉坦塔來說，琉琉西的生命安全是永遠排在第一位的。就算是犧牲自己，也要讓琉琉西安全離開這裡，這是拉坦塔內心的真實想法。
要是再這樣繼續與惡魔們戰鬥下去，自己這邊就會因為疲勞而倒下，所以不如趁現在還有餘力，拼上一把。
但是，就算成功離開了這個地方，也不能說情況就一定會有所好轉，所以這也只是一時之策，狼人烏魯卡如此說道。
「而且，要是那樣做的話，會影響巫女姬殿下與世界樹大人的同調。更何況，說不定現在惡魔已經出現在了整個迪普古林，就等著我們衝出去被他們一網打盡呢。」
「但是······就算一直堅守在這裡，也是有極限的。不管外面有沒有出現惡魔，我們都應該離開這裡，然後帶領居民們事先撤退。而且，最差的情況下我們還會需要來自森林之外的援助，到時候就需要歐理維爾大人來當交涉人，這場災難並不只是我們恩特之森子民的危機。」
「拉坦塔說的也有道理。至少我們得先離開這裡，到外面去搜集資料才行，不然便無從判斷······」
說著烏魯卡向著歐理維爾投去了尋求意見的視線，但歐理維爾卻一言不發，眼睛只看著面前的惡魔，並不斷的將其一個個貫穿。
原本就沉默寡言的女性到了這種危急時刻也依然態度不改，大部人都在期待她是否有什麼秘策，但也有一些人覺得她是不是已經被逼到山窮水盡的地步了。
雖然並不是代替歐理維爾進行回答，但克艾露一邊發動精靈魔法消滅敵人一邊對烏魯卡張開了雙唇。
樹精靈這一種族的成長速度原本就極其緩慢，但克艾露的成長速度在樹精靈之中也屬於非常遲緩的，所以儘管她的年齡與拉坦塔和琉琉西相差不大，但外表與她們比起來卻相當的年幼。
因為克艾露這副幼小的外表，導致她經常會對某些事情反應過度，在日常生活中也非常的爭強好勝，不希望別人把她當做小孩子。
「烏魯卡，迪凱路，拉坦塔，有一件事我要說一下，剛剛精靈們有一瞬間動搖了······不，與其說動搖，不如說是震驚才對。」
克艾露注意著不讓自己的語氣過於女性化這樣說道。
「精靈在動搖？不是在恐懼和膽怯？」
雖然拉坦塔的天賦並不足以成為精靈使，但他作為樹精靈，天生便可以與精靈盡興一定程度的交流。他反問克艾露並不是因為他對克艾露的話有疑問，而是想要克艾露進一步的解釋。
「嗯，震驚是最符合的形容詞了。精靈們絕不是在恐懼，所以，或許是發生了在精靈們預料之外，但卻不是壞事的事情。我並不清楚詳細的情況，但歐理維爾大人應該不會沒有察覺到才對······」
克艾露也向歐理維爾投去了視線，但歐理維爾依然不為所動。
不，正確來說，她是沒有對克艾露他們做出反應的餘裕了。
歐理維爾那伶俐的雙眼，緊盯著人惡魔不斷出現的那個出口。
突然，在那附近的空間有波紋產生，下一個瞬間一個巨大的異形黑影出現在了那裡。
那道巨大身影的外貌明顯與迄今為止的惡魔風格不同，散發出來魔力也那些惡魔大相徑庭，這是一個有著獅頭的獅子人。
看起來宛如鋼鐵一般堅硬的肌肉被赤銅色皮毛覆蓋，全身燃燒著熾熱的火焰，在太陽穴附近還有有一對犄角彎曲著向前方生長。
有六根手指的兩只雙手上各握著一把如同鋸子般的大劍。
和握著劍的主人一樣，劍上也燃燒著火焰，這個惡魔十有八九是擅長操控火焰的高位惡魔。
歐理維爾那迄今為止都毫無波動的雙眼，此刻卻浮現出了極度緊張的神色。
「大惡魔麼······雖然不是墮神，但也不是一個可以輕視的對手啊。」
在歐理維爾迄今為止的漫長人生中，一次都沒有遇到過如此強大、恐怖的敵人。
大惡魔這一等級是僅次於爵位級惡魔的，與歐理維爾之前戰鬥的下級惡魔與中級惡魔有根本上的不同。
沒想到都已經深陷如此絕境了，居然還雪上加霜的出現了如此強大的敵人，除了歐理維爾以外，包括那四名精銳，所有的擔任護衛的戰士們都感到了絕望。
但是，大惡魔並不是只出現了一只。
在纏繞著火焰的大惡魔身邊，又產生了波紋，連續出現了數只可以認為同樣是大惡魔的個體。
在青白色的皮膚上附著著反射光芒的粘液，外表看起來恐怖無比的巨大青蛙，身上穿著破爛不已的披風，臉上有五個閃耀著紅色光芒眼球的巨大骸骨。
實力遠在他們之上的惡魔們出現在了對面，護衛們只是面對面地站著與其對視，都感覺靈魂會被削弱——這並不是簡單的用一句「強大的惡魔參與了戰鬥」就能概括的事態。
迄今為止琉琉西一直都在與巫女們一起進行祈禱與同調來增強羽古多拉席爾的力量，讓其即使被帶去了魔界，也能竭盡全力的干涉次元壁，讓上級惡魔或尼祖厄庫不出現在迪普古林。
但是，之前一直都被壓制的上級惡魔卻出現了，這就代表羽古多拉席爾的力量衰弱了。
察覺到這件事的拉坦塔帶著無法隱藏的焦急神色轉過身去看自己的青梅竹馬。
「琉琉西！！」
正跪坐著專心向羽古多拉席爾祈禱的琉琉西，此刻滿臉都是冰冷的汗水。
看起來，琉琉西的精神正在承受著無比沉重的負擔。
就在拉坦塔下意識的跑向琉琉西的瞬間，有著獅子頭的大惡魔向前踏出了一步，隨著他的動作，地面宛如地震一般在不斷震動。
「什？！」
是覺得沒有與人界的下等種族說話的必要麼，大惡魔那凶惡的獅子頭就這樣靜靜的露出了一個了笑容。
他的那副表情仿彿在說「你們覺得能無視吾等的存在麼？」。
對此感到氣憤的拉坦塔和迪凱路以及烏魯卡都咬緊了牙關，用力瞪視著大惡魔們，歐理維爾也平淡的架好了法杖。
但歐理維爾的臉上，不知為何卻浮現出了宛如小孩惡作劇成功時的笑容。
只有短短的一瞬間，歐理維爾的表情從冷酷的戰士變成了擁有慈愛之心的溫柔女性。
「真是非常抱歉，結果還是要借用你的力量。居然要學生來幫忙，作為一個教育者真是不合格。儘管丟人，但還是請你助我們一臂之力吧。」
隨著歐理維爾的話音落下，在她與大惡魔之間，一道新的波紋產生了。
拉坦塔與克艾露他們以為又有新的敵人要登場，又提高了警戒，但出乎他們意料的，大惡魔們也開始緊張了起來。
看樣子接下來出現的人物，不只是拉坦塔他們，就連大惡魔也不認識。
但是只有歐理維爾知道引起這喜劇一般場景的人是誰。
歐理維爾當然知道精靈們的異變，作為有著極其罕見天賦的精靈使，她清楚的知道引起異變的原因是什麼。
那是因為精靈們感覺到了極其少見的大精靈出現的氣息，所以才會動搖。
那麼召喚大精靈之人究竟是誰？
歐理維爾低語著他的名字。
「多蘭。」
宛如陽炎一般的波紋消失的同時，多蘭和賽莉娜出現在了神諭之間的地板上。

譯：陽炎便是空氣因為高溫而導致光線扭曲的顯現，你們可以自己去搜搜。