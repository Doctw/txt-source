 佐奴塔克王國復興

「再這麼下去，魔族就要衰退了！所以朕要復活魔族的王國！」

一位魔王大人（年齡十歲的少女）突然跑來和我們見面，然後宣言要復活魔族的王國。

老實說，給人種【隨便你吧】的感覺。

如果我們和這邊的內政扯上關係會刺激佐奴塔克共和國政府，所以我當然不會幫忙了，而且也沒有幫忙的理由。

「如果能建國，將來就能和赫爾姆特王國與阿卡特神聖帝國結成對等的聯盟。所以，剛才朕說的可不是無心之言哦」

「不過如果冒失的接受或尋求援助，應該會讓佐奴塔克共和國那邊產生警戒吧……」

身為宰相的萊拉小姐似乎有著正常判斷力的樣子

「不過，為什麼是現在？」

路易斯對魔王在當下這個時期做出王國建國宣言並前來拜訪我們的理由發出了疑問。

「魔族到現在為止，實行的都是即便過去一萬年以上也仍舊保持一個國家的安定統治。但因為過於安定魔族的本能都衰退了。然而，現在終於確認了有兩個人類國家存在。所以今後魔族將迎來巨大的變化。這件事帶來的的好處壞處都很多，沒人能保證佐奴塔克共和國會做出正常的判斷。因此，朕等人才要站出來！」

「也有【把國家分成兩部分的話，最後總能有一邊存留下來】這樣的現實理由」

這位魔王大人才十歲思考就這麼正經了。

也許是因為她是女孩子的緣故吧。

宰相萊拉小姐也相當冷靜，現如今的魔族中女性比較優秀嗎？

「魔族會被人類所滅？那種事可能發生嗎？」

魔族每個人都是魔法使，魔導技術也比人類那邊壓倒性的優秀。

不管怎麼想，都讓人覺得被毀滅的會是人類這邊。

所以說情況如果反過來才讓人擔心吧。

「魔族就像你們看到的那樣因為少子化人口正在減少。另一方面，人類的數量卻在增加。等到琳蓋亞大陸開髮結束的時候，人類的勢力就會像其他島嶼或大陸延伸了吧。確實魔族的魔導技術非常優秀，但有充足時間的話也有可能被人類趕超過去。就因為有這樣的長期視角，女王陛下現在才要站出來」

「已經站出來了哦」

魔王大人挺起胸堂堂正正的這麼宣言道。

然而，很遺憾的是她的身高和上圍都很沒份量。

雖然我覺得她這志向是很了不起……。

「可是，為什麼突然要獨立？如果遭到共和國警備隊鎮壓的話就全完了不是嗎？」

嘛，普通來說都會是像伊娜說的那種結果。

佐奴塔克的國內問題雖多，但也沒出現因為分裂導致的混亂這種情況。

「獨立還是很久之後的事。多半在朕還活著的時候是不可能辦到的吧。但是，將今後的根據地先準備好卻是可能辦到的！總之，你們就好好看著朕等怎麼活躍吧」

「陛下，差不多到回家的時間了」

「唔，學校的老師也說過在天黑前必須回家吶」

「「「「「「「「「「哎呀呀！」」」」」」」」」」

雖然扯了一大堆國家獨立什麼的，但卻好好遵守學校老師教導的魔王嗎。

這是什麼超現實的存在啊……。

「那麼後會有期了！」

「告辭」

等把想說的話全說了的兩人離開房間後，我們陷入了揣摩她們發言中真意的思考。

「親愛的，這話題不會很危險麼？」

我能理解埃莉絲在擔心什麼。

認為這個國家已經沒有力量的魔王，在和我們見過面後立刻產生了從共和國中獨立的想法。

肯定會讓人得出是我，和我身後的赫爾姆特王國在分裂魔族的結論吧。

因為除此外沒別的解釋了。

不過，那種擔心也不過是讓人一笑置之的東西。

因為有新聞記者廬米在。

「我覺得不需要擔心哦」

「喂，新聞記者。這種時候，煽動讀者產生那種陰謀論想法才是正常的做法吧？」

「我家報社雖然因為對民權黨的政治家言聽計從的上司很多而被視為問題兒，但姑且也佔著發行量排行的首位啊。給人花邊八卦感覺的東西是不會豋的」

「你是說你們走的是讀者高精路線嗎？」

「就是那個啊！我和前輩們不同，可是認認真真的進行求職的」

「「「我們也很認真的啊！」」」

聽到廬米的說法，莫爾他們火冒三丈的反駁。

認真求職結果還全都落選了嗎……。

要是我的話肯定會受到心理傷害

前世時工作雖然很辛苦，但總算能就職真是太好了呢。

「首先，警備隊完全沒有警戒她們嘛！雖然偶爾被批判做些邪惡的勾當，又作為反軍事思想的首當其衝對象階層而遭到厭惡，但基本上警備隊的優秀人才很多的」

即便能就職，內部的競爭率似乎也很高。

所以收入和待遇都很安定的公務員工作，更容易聚集優秀的人才吧。

「我能保證不用擔心啦」

「那就好……」

到了第二天早上，魔王和宰相的獨立宣言的詳細做法被闡明了。

在廬米帶來的每日專刊早報上。

「出來了哦。看這條」

「我看看」

這條新聞在生活版裡。

明明是王政國家要獨立的話題，卻被刊登在生活版裡。

這種不對勁的感覺是怎樣？

「我看看……『歷史悠久的佐奴塔克王國的女王陛下，就任新生佐奴塔克王國的國王』。然後最關鍵的新聞具體內容是……」

新聞所描述的詳情大致是這樣的。

魔族所居住的這個島，目前有人居住的區域還不到全島面積的四分之一。

雖然過去有人居住區域達到全島面積四分之三的程度，但因為人口徐徐減少放棄了很多地方。

變成無人區後的土地逐漸荒蕪化重返自然狀態，有些場所因為作為放棄地太古老甚至變回了魔物領域。

這種放棄地，現在由無職的、或是逃脫待遇太糟黑心企業的年輕人們開始了再生活動。

他們在放棄地中過著基本自給自足的生活，連生活所必須的基礎設施設備，也是靠修理好原本拋棄在放棄地裡的東西後來運用維持的。

主要經濟收入遠是將自己種植的農作物販賣後所得的收益，雖然這種收入低於全國總平均收入值，但因為過的是自給自足生活所以倒也不至於陷入貧困。

不如說，這種生活讓他們的精神層面變得很富足。

這個挑戰是否能順利進行下去呢？

還請各位讀者敬請期待。

「……」

讀完這段文章後，我產生了很嚴重的既視感。

這種活動，地球上的各國也都進行過。

年輕人跑到廢棄的村莊，利用那裡廢棄的耕作地開始新生活。

我記得，是叫農村再生運動來著，好像也叫綠色再生？

「廬米，這些你早就知道了嗎？」

「我畢竟是記者嘛，所以是以前就知道有這種活動了哦。不過，生活版的取材在我的負責範圍外啦」

「你這傢伙說出了官僚一樣的話啊」

「鮑麥斯特伯爵，你對魔族的社會理解的太詳細了吧！」

不是，我並不是對魔族的事理解的有多透徹，僅僅是因為這裡和日本太像的緣故而已

「不過這個能算是獨立嗎？在我看來就僅僅是農村復興運動而已……」

「誰知到？我根本搞不明白是怎麼回事」

對我的問題艾爾歪著腦袋表示不理解。

就在我們看著報紙煩惱的時候，魔王大人和宰相再次出現了。

「不好意思今天穿便裝來的」

「陛下今天要去上學」

今天的魔王大人穿著很普通的連衣裙。

然後，背著雙肩書包……。

魔族的孩子們上學事似乎都會背上雙肩書包去學校。

（譯註，11區似乎有不成文規定只有小學生上學會用雙肩包，升入中學後會換成手提式書包）

「這就是獨立運動了嗎？」

伊娜把報紙拿給二人看。

「正是！在共和國的傢伙們放棄的土地裡，將無職的、對眼下生活不滿的人們集合起來形成集團。這就是千里長途的第一步！只要像這樣不斷增加臣民，最終就能實現王國獨立的目標了！」

比起臣民，組員或者團員的叫法說不定更適合那群人。

雖然掛著王國的名號，但這怎麼看更適合叫某某團體或者某某公司比好。

「陛下，已經就任了執行這次活動的非營利性團體的會長一職。我則專心負責副會長和會計的工作。因為陛下還是假日之外都要去上學的身份，所以預定平時由我來統帥這個團體」

由放棄地中的各個農村組合而成的非營利性團體啊……。

在共和國政府認為無價值的土地上，魔王大人和宰相集合了無職的年輕人在哪裡摸索自立之道。

變得不需要生活保障的人增多，雖然只有一點但稅金總算也有在上繳，這樣還有故意去妨礙的話那只會產生損失而已吧。

首先，怎麼都不覺得魔王大人她們會擁有武裝力量。

所以警備隊才對她們置之不理吧。

說到底，魔王大人她們就沒有同共和國政府對立嘛。

「但是，還不能太天真。雖然在朕活著的時候會很難，但到了朕子孫的世代我們的組織規模也會擴大很多了吧。到那時即便不依靠武力鬥爭要分離獨立出去也是可能的」

「不愧是陛下，非常精明的作戰呢」

「沒什麼，這不都是靠了萊拉的獻策嘛」

那與其說是獨立，就只不過是擴大組織而已吧……。

非營利團體那邊，也是出於這個原因才選中了魔王大人把她抬出來當象徵物吧……。

還是LOLI的魔王大人的話，就算隨便抬出來也不會讓人產生警戒感。

庶民中喜歡王啊公主啊的人其實很多的。

實施獨裁統治的話會很讓人困擾，但這樣幼小的魔王大人應該只會讓人微微一笑吧。

「陛下，差不多該去學校了」

「已經到這個時間了嗎。朕為了成為被所有人喜愛的魔王，必須得在學習上好好努力才行吶」

魔王大人在宰相的陪送下向著學校去了。

似乎只要學校不放假她就會好好去上學的樣子。

「雖然即便好好學習，如果無法就職的話就會變成像我們一樣的人就是了呢」

「現實，真的很殘酷」

「「「薇爾瑪醬！好過分啊！」」」

「你們幾個，其實有著什麼問題吧？」

「「「卡特莉娜小姐也好過分！」」」

莫爾，這種事就是那麼沒有夢想沒有希望哦。

「親愛的，沒有產生任何問題真是太好了呢」

「是啊」

到了當天下課時間，魔王大人又跑來了。

一邊喝著埃莉絲泡的茶，吃著我們今天從其他高級糕點店買來的蛋糕，一邊寫作業。

話說，這位魔王大人為什麼要特意來我們的房間？

弗裡德里希他們也在這裡，我覺得這實在不是個適合學習的地方。

「鮑麥斯特伯爵的小嬰兒們真可愛啊。孩子可是國家的寶貝哦。呃，下一道是分數的除法嗎……」

魔王大人一邊做著作業中的算術題，一邊時不時的眺望小嬰兒們。

「朕變成大人的時候，也必須得生下後繼者才行吶。問題是不知到時能不能找到相親對象……」

「戀愛結婚也很好的不是嗎。政治聯姻什麼的如今可不流行了喲」

「你說什麼呢……我等身份高貴的人都是為了家族國家才結婚的哦。雖然戀愛結婚是不錯，可包括教授在內這裡不就有四個沒法結婚的人嗎」

「陛下，您這話太傷人心啦————」

莫爾他們因為被魔王大人指摘一直單身，全都重傷跪倒了。

「吾輩有研究這個戀人兼妻子」

當然，恩斯特那邊就完全不在意了。

「確實，我認為貴族和王族結婚是該最先追求家族國家的繁榮。但是，即便如此和出色的丈夫相遇的可能性也是有的哦」

「埃莉絲，你遇到好老公了嗎？」

「是的」

被當面這麼說有點羞人，但知道埃莉絲是這麼想的又讓我很開心。

「就是說，朕遇到白馬王子的可能性也是有的。可以稍微有點期待了嗎。話說鮑麥斯特伯爵。這個計算朕不懂」

魔王大人把學校作業中的算術題指給我看。

看起來，因為她是魔王，所以好像沒有把作業翹掉不寫的選項存在。

「分數的除法嗎……」

魔王大人在上課時做過的題目，全都得了×的結果。

因為她明明做的是除法，計算時卻沒有把除數的分子分母顛倒。

這麼一來，除法就變得和乘法沒區別了。

「二分之一÷三分之一，結果是四分之三哦」

雖然我已經放棄學業很久了，但也不可能連小學的分數除法怎麼算都忘了。

「鮑麥斯特伯爵，為什麼做分數除法時非得把除數的分子分母顛倒過來不可的？」

「關於這個，那邊有很多知識分子您還是去問他們吧」

恩奈斯以前是有名大學的教授，學歷更是出自這個國家的最高學府。

連莫爾他們，其實也是從這個國家內排行前五的大學畢業的，宰相萊拉也出身自很優秀的大學。

所以比起我還問他們更好。

「威爾，好厲害！」

我明明只是解開了一道小學生的算數題，可不知為什麼卻被路易斯尊敬了。

大概是因為在琳蓋亞大陸生活，只需要會讀文字會做簡單的四則運算就很足夠的緣故。

分數除法什麼的，我覺得只要不是去學院之類的地方就沒什麼必要。

據說那種地方裡，聚集了一群連埃莉絲也會退避三舍的學術笨蛋。


順便說下，我從沒去過什麼學院。

因為我和那種人談不到一起。

「只是簡單問題的話還好吧……」

如果現在太得意，就有被問到更難問題是答不上來的自爆可能。

給魔族留下我最多只有【大家基本都能做到】程度學力的印象是最好的。

「吾輩，是專攻考古學的人」

「我也是文科」

「我也是！」

「算數意外的很難哦，數學什麼的……」

恩斯特和莫爾他們居然沒能回應我的期待。

雖然好像能完成分數除法，但關於計算時為什麼要將除數的分子分母顛倒的問題就無法進行說明的樣子。

「我說你們幾個，居然在這種時候派不上用場是怎麼回事啊！」

「鮑麥斯特伯爵，你這話太過分了。吾輩在發掘遺蹟時是很有用的吶」

這麼說起來，恩斯特是做出過很大貢獻來著。

「那，莫爾你們呢？」

「我們幾個的工作，是輔佐滯留在這個國家的鮑麥斯特伯爵啊。為分數除法的計算理論進行說明不在工作範圍之內的……」

「沒錯沒錯」

「我是，文科生」

莫爾他們是不行了，現在只好拜託繼承了宰相家血統的才女萊拉小姐進行說明了吧。

「分數的除法嗎……」

「是的。為什麼計算時除數的分子分母要顛倒呢」

「這個嘛，請去向學校的老師請教吧。那些人，就是為了這種時候才存在的。陛下，理解追隨自己的家臣的特性並讓他們適才所用是很重要的哦。話說回來陛下，關於明天的視察……」

被矇混過去了————！

萊拉小姐因為自己也不知道，所以切換話題反倒對魔王大人提出了問題。

而且，她的手法還很熟練。

「視察嗎。讓人期待吶」

「是的，不管那個村子的住民，都打心裡期待陛下的來訪」

「嗯。畢竟是朕的臣民候補嘛」

只是被推成農村再生運動招牌的話也不需要多在意嗎，畢竟怎麼說也比胡亂發動革命好。

如果真的亂來，首先就過不了防衛隊這一關。

萊拉小姐是所謂的現實主義者呢。

「鮑麥斯特伯爵，要一起來嗎？」

「這個嘛……」

到目前為止，最關鍵的兩國交涉方面完全沒有進展，而這個佐奴塔克共和共中關於我們的事的話題報導回數已經大幅減少了。

【某個有名的歌手結婚了】，熱門話題已經逐漸轉向了那一類方向上。

是覺得我們連歌手都不如吧，提拉哈雷斯群島上的交涉雖然仍在繼續，但關於兩國交涉團的事已經基本不會再被報導了。

雖然還有廬米這個渠道，但關鍵的交涉本身已經停滯到了難看的地步，再加上和民權黨的支持率下滑有聯繫，所以報社據說被政府施加了【儘量別再報導了】的壓力。

「喂，民主主義的！」

「被鮑麥斯特伯爵這麼說耳朵好疼！可民權黨裡有很多從記者升上去的人物啊。前上司的親筆書啊口頭命令啊什麼的可是很難違背的啊。在政治版面塞進小小的一條報導，是只有新人才做得到的反抗啦！」

雖然是很過分的事，但我們也因為這個原因現在很閒。

去農村玩玩也沒關係吧。

「問題是，防衛隊會認同這種事嗎」

雖然這是唯一需要擔心的地方，可防衛隊很乾脆的就給了許可。

「啊啊，是說那個農村再生運動吧。可以啊。因為沒有奇怪的運動家，所以護衛起來也輕鬆」

「誒？沒有嗎？」

「你看，這個運動不是把魔王大人抬出來當裝飾用的首領了嘛？所以不會有奇怪的左翼運動家湊過來的」

那種運動家裡，有不少人很討厭國王這種存在本身。所以從魔王大人成為首領那一刻開始就不會跑了摻和了。

許可也得到了，從第二天開始我們就在佐奴塔克共和國內的各種來回巡遊了起來。

翌日是五之日。

所謂【之日】，就是類似日本的【星期幾】的東西。

一之日到七之日期間，五之日到七之日學校都會放假。

「假日相當多呢」

不止是伊娜，我們一行的其他人也這麼覺得。

因為羅德里希那樣的傢伙如果沒什麼事的話，一週就只會給出一天假期而已。

「因為就算急匆匆的接受完教育，往後的出路也都被堵塞著啊」

按照莫爾的說明，這是儘量拖長較預期的苦肉之策。

就算早早受完教育，社會能提供的職位太少的話也只會讓無職的人增多吧。

無職的人一多對政府的批判聲就會變強，於是只好增加假期來拖長受教育期了。

「畢竟都明說是拖延了吶」

「嗯斯特，你那說法太露骨了」

我覺得恩斯特是因為自己有工作，所以才用那麼過分的說法的

「鮑麥斯特伯爵殿下，抵達」

因為聽說這個村子有個老舊的機場，所以我們是乘坐自家的小型魔導飛行船過來的。

另外，還有一艘警備隊的小型飛行船和我們同行。

雖然已經不引人注目了，但我們姑且也是國賓所以得有人護衛。

「陛下，勞駕您遠道而來」

和魔王大人一起抵達村子後，大約百名年輕人出來迎接我們。

所有人看上去都是二十歲前後的年紀，但魔族非常不顯老所以我也搞不清他們的真實年齡。

不過，所有人都還不滿百歲。

「各位，謝謝你們今天特意來迎接。看到村子如此充滿活力朕感到非常開心哦」

「因為大家都很有幹勁嘛」

「陛下，從事農業很快樂呢」

「雖然也有幾乎沒什麼收成讓人心情沉重的時候，但當手裡感受到收穫物沉甸甸的份量時那些東西就全被吹飛啦」

「陛下，在下開始養雞了哦。似乎最近終於開始下蛋了呢。這比以前我把羽絨被高價賣給老年人們的工作充實多了」

我覺得年輕人幾乎都沒工作對於社會來說是很深刻的問題。

然而，因為在農村生活後他們開始有了自己活著的實感，所以即便只是充門面也罷魔王大人也還是幫了很多人的忙吧。

「今天來的客人很多吶」

「嗯，他們是來自從遙遠的東方琳蓋亞大陸的赫爾姆特王國的鮑麥斯特伯爵和其家人。是朕的客人」

「那不搞個歡迎會可不行吶。就利用今天要收穫紅薯的機會搞個挖紅薯大會吧」

「挖紅薯嗎！讓人期待吶！」

就算是魔王大人，但果然還是個小孩子。

一聽村代表說可以挖紅薯就興奮了。

「挖紅薯嗎？似乎很有趣呢」

當然，我也很感興趣。

或者說，那種什麼也不幹只是待在賓館裡待機的生活我已經過夠了。

因為政府的應對實在太隨便，都產生【我們為什麼會人在這裡呢】的心情了。

「紅薯的話說不定能做成弗裡德里希他們的斷奶食品吧？」

「那個提議很好呢」

「我們也想吃紅薯料理啊」

連伊娜和路易斯都幹勁十足，於是我們把弗裡德里希他們交給飛行船內的女僕照看，然後一起參加了挖紅薯大會。

連我也絕對不使用魔法，只靠自身的力量挖紅薯。

「啊咧？好小？」

「老公挖到的紅薯都是些小個頭啊」

雖然我幹勁十足的抓住紅薯葉子拔個不停，可拔出來的紅薯個體全都很小。

看到這幅情景的卡琪婭笑話起我來。

「卡琪婭你還敢說，你挖到的紅薯不也都不太大嗎」

「啊咧？關於紅薯本來是我們家的得意本領來著啊……」

雖然挖到的成果和普通蕃薯很像，但和圓薯完全不是一回事吧？

「因為沒使用合成肥料，所以不會長得太大」

合成肥料？

類似化肥的東西嗎？

魔王大人的話真是簡單易懂。

簡單來說就是這個村子裡進行的農業，是類似在日本被稱作有機無農藥栽培的農業類型吧。

「因為不需要配合什麼農作物規格，收穫的東西也不會拿到通常流通渠道去賣的嘛。不過，支持這項運動的志願者們買走後給出的評價都很棒哦」

越來越感覺這運動曾經在日本見過了。

農耕用回古法，收穫成果就賣給支持者來獲得經費。

有機無農藥蔬菜農場，好像是叫這個名字吧？

「老公，人工肥料是什麼？」

「大概是應用了魔導技術的肥料吧？」

「朕在學校有學過，是只把作物成長所必須的成分抽取出來，在工廠裡生產的肥料哦」

魔王大人回答了我的問題。

因為沉迷於挖紅薯她鼻尖沾上了泥土，不過這個樣子挺可愛的。

雖然堅強又努力，但她畢竟還是個孩子嘛。

「陛下，鼻尖沾上泥土了哦」

「嗯，宰相你受累了」

察覺的主君臉上有異的萊拉小姐用手帕幫魔王擦掉泥土。

看到這種場面後，感覺她們兩個與其說是君臣關係不如說更像一對母女吧。

「嘿誒—，有那種東西的話那我家大哥就能輕鬆點了吧？」

「我也不知道。說到底，如果和交易的交涉不能好好完成的話這種東西就很難進口吧」

「那群傢伙，真虧他們能呆在那種無人島上每天只顧廢話連篇的磨蹭。老公你也是這麼認為的吧？」

雖然不知道交涉是不是真的沒用，但進展幾乎完全沒有也的確是事實沒錯。

所以我不是不能理解卡琪婭的意思。

「就連老哥，製作肥料時也總是很辛苦的啊」

這麼說來，拜特先生他是僅靠自然肥料就培育出了圓薯。

如果能進口合成肥料的話，也許就能輕鬆的大量種出圓薯了？

「合成肥料也是有長有短哦。只有培育單一大量作物時才有利」

重要的事不能濫用，要靈活的和自然肥料搭配使用，負責村代表工作的青年這麼告訴我們。

這位青年是某個大農場家的孩子，在自家的大農場裡任職，因為贊成這項運動所以跑來為參加者們做技術指導。

魔王大人已經擅自把他說成是未來的農業大臣候補了。

「我們村子的場合，是因為人工肥料的購買費太離譜不得已才使用自然肥料的」

即便因為想過省錢的生活參加了運動，如果肥料費太貴辛苦就沒有意義了嗎。

農藥方面也是同樣情況，就算肥料和農藥的公司可以賺大錢，如果農家這個基礎陷入貧困就沒有意義了吧。

「自然肥料也能好好種出好吃的作物呢。驅蟲劑也是我們自製的哦。收穫到的作物裡個頭大外形好的送給支援者，剩下的自己吃掉。這樣就很足夠啦」

「威爾，這個紅薯很大喲」

「哼哼哼，我的這個才更大」

「偶爾像這樣重拾童心也不錯呢！」

「要是帶女兒來她們會很高興吧？」

「時不時做些沾到泥土的工作也不壞吶」

從年輕人到三位大約算是大叔的人，大家都挖紅薯挖的很愉快。

因為通常收穫作業也一併進行，村子紅薯地裡的紅薯最後全都被村民們好好挖了出來。

「這些要做成孩子們的斷奶食品嗎」

「薇爾瑪，弗裡德里希他們還沒長牙，所以要搗成儘可能稀一點的糊狀物哦」

「威爾大人，瞭解的好詳細」

因為我前世時也做過關於斷奶食品的生意。

只是不知道為何在少子化的時代卻要模仿大商社的手法進行販賣，結果不出所料的失敗了。

雖然我只是學到了『斷奶食品應該這麼做』的知識就算了事，但這個項目的責任人科長，就很可憐的被趕去了中國分公司。

因為他太太對他說了『中國的大氣污染很嚴重，為了孩子的健康著想請你一個人去赴任吧』話，所以送別會上他還淚流滿面了來著。

「給小嬰兒吃味道太濃的東西不好，要按照凸出素材本身甘味的方向製作。對了，不要用蜂蜜」

在某美食漫畫裡給一個嬰兒吃過後可是遭到了批判呢。

因為有被肉桿菌感染的可能，所以一歲以下的幼兒要避免食用蜂蜜。

「威爾，還是老樣子對微妙的事情知道得很詳細……」

「因為有學習啊」

雖然還要再加上前世這個條件就是了。

在前世沒什麼了不起，但在這個世界卻被視為很厲害，這類知識意外的很多。

「鮑麥斯特伯爵，簡直就像是這個國家的人一樣吶」

因為日本和魔族之國的相似部分太多。

「埃莉絲，我覺得過濾的話這樣就可以吧」

「沒錯，調味方面我覺得也是現在這樣就好」

「因為已經有了充分的甜味呢」

埃莉絲、泰蕾莎、麗莎幾個將斷奶食品裝好，然後為了弗裡德里希一小口。

「噠———」

「是嗎，好吃嗎」

艾爾和遙也開始夫妻一起喂里昂吃斷奶食品。

「孩子這麼多真讓人羨慕吶。我們這裡都沒有小孩子」

雖然魔王大人自己就是個孩子，但除了她以外這個村子裡確實沒有一個兒童。

「雖然覺得孩子是國家之寶，但贊成我等運動並參加進來的人大半是前無職業者啊。因為所有人全都未婚，孩子當然也就沒有了。建國計畫真是困難重重啊」

「不過，也有在這裡遇到意中人然後結婚了的人。現在肚子裡已經有了孩子的女性也出現了數名，所以不需要太悲觀啦」

魔王大人先不說，萊拉小姐似乎對眼下的狀況並不是很悲觀的樣子。

「可是，上學怎麼辦？」

這裡距離普通人居住的地方十分遙遠。

因為是將原本已經廢棄了的村子拿來再利用，所以這附近沒有可供孩子們上學的學校。

連魔王大人自己，也處於沒有休假就無法過來視察的狀況。

「教育是必須的吧」

「有退休教師，我們這裡也有好幾位持有教師資格的人。剩下的，就是想辦法把能進行義務教育的學校蓋出來了。該說是很萬幸吧，這裡有原本就是學校的建築物」

「能得到許可嗎？」

我最擔心的就是這點。

如果這方面也和日本類似的話，想創建一所新學校肯定需要天知道有多少的大量必要文件。

就算有以萊拉小姐為主的一群人去市公務所進行交涉，這條道路也會漫長而凶險吧。

「確實，必要文件和條件多到離譜導致困難重重啊」

即便好不容易生出孩子，如果為了讓那些孩子上學而必須得讓他們離開村子就本末倒置了。

如果現在不能為孩子母親腹中的孩子們建設好學校，村子規模的擴大就會變得很困難吧。

「還有一點，醫療該怎麼辦？」

「不是有治癒魔法嗎？」

「關於這個啊，在這個國家裡如果沒有資格證明的話是禁止使用治癒魔法的」

「真的嗎？」

「真是讓人震驚的事實呢」

不止是埃莉絲連我都被嚇到了，據說如今的魔族之國如果沒有資格認證的人使用治癒魔法的話，最糟糕的情況甚至會有被關進監獄的危險。

「會有這麼愚蠢的事嗎？」

在泰蕾莎看來，魔族之國會對便利的治癒魔法進行限制這簡直令她難以置信。

「雖然光聽理由是覺得很蠢……」

魔族之國為了追求國家富足，一直不斷的提高魔導技術。

結果就是，如今這個國家的繁榮是靠普及非常節能的魔道具來支撐的。

生來魔力量就很多的魔族，很少有勉強鍛鍊自己來提升魔力量的必要。

不如說如果人人都會便利的魔法，那些量產的魔道具就會沒法賣不出去了。

而魔道具如果賣不了，國家就會運轉不靈。

於是，非常嚴格的關於魔道具和魔法的禁止使用規則就出台了。

舉例來說，可以長時間將食材以最佳狀態保存的魔法袋就屬於禁止使用的魔道具。

另一方面，沒有醫師資格的人也被禁止使用治癒魔法，理由是『連醫師都不是外行人，如果施展治癒魔法時出了什麼岔子誰來負起責任？』。

這麼一說的話，確實亂用魔法會有人困擾。

連攻擊類魔法那邊，也很少有人去練習的樣子。

軍警那邊，採取的是對肉體進行訓練然後統一配給武器的做法……因為武器只需要注入魔力就能用所以本人就沒必要使用魔法了……另外還制定了地球上的先進國家那樣警察軍隊專用訓練方法。

至於恐怖分子什麼的……似乎也就只是為了尋求工作而遊行示威的程度……如果軍警對這樣的人發動攻擊魔法鎮壓導致死傷者出現，反而會遭到媒體和人權團體的責難。

不會致人死地程度的電擊系魔法很難控制力道，加上平時不會做魔法訓練所以魔族中幾乎沒人能用。

不過，他們似乎準備了注入魔力就會發出適量電擊的類似麻醉槍的東西。

因為有這類用起來很輕鬆的東西，魔族才沒多少勉強自己去使用魔法的人吧。

「難得天資獨厚真是太浪費了」

「如果在市裡使用魔法什麼的，還會遭到職務質疑呢」

莫爾這麼向泰蕾莎說明道。

使用魔道具不會有任何問題，可使用魔法就會遭來責難。

『突然放攻擊魔法出來，要是有人因此受傷可怎麼辦』因為有著這種警戒心理，所以魔族是絕對不會在人多的場所使用魔法的。

而魔道具那邊，卻是只要注入魔力立刻就能知道會產生什麼後果。

因此，大家才覺得只要擁有夠發動魔道具程度的魔力就好。

「這個村子裡倒有一部分人會使用魔法，因為有很多基礎設施已經放置了不知多少年無法修復，所以只能得靠魔法來彌補了」

和魔法有關的資料被大量的留了下來，參考那些東西後這裡的人們變得可以用魔法搬運重物，耕作農田，整備道路了。

「不過也算不上回覆以前的生活」

「因為沒有錢，所以如果想做什麼都得靠自己，這是整個村子的規矩哦。萬幸的是村裡有持有藥劑師資格認證的人，這附近也有藥草類的植物生長」

既然能生產魔法藥，那麼也算對受傷和疾病有所準備了吧。

「持有醫師資格可以使用治癒魔法的人當中也有人對這個村子產生了興趣，所以我覺得關於治癒魔法的事很快就能解決啦」

就是說這個魔王大人當村長的村子，確實正在一步一步穩定的發展著嗎。

「難懂的話就說到這裡吧，朕肚子餓了」

「我也感覺有點餓了」

用收穫的紅薯製成的嬰兒食品也完成了，弗裡德里希他們正好像很美味的吃著。

接下來，只需要漸漸增加進食斷奶食品的次數就可以了吧。

「其他的料理也完成了」

在埃莉絲她們的幫忙下，紅薯天婦羅、拔絲紅薯、蒸紅薯泥等料理也完成了，於是就和村民們一起開吃。

自己收穫到的作物拿去烹飪後最是美味。

雖然這種說法有可能是錯覺，但味覺也不是僅靠舌頭的感受就能判斷的東西。

「看啊，鮑麥斯特伯爵。朕的臣民候補們都非常開心不是嗎」。

年輕的村民們一起講自己收穫的作物拿去烹飪，然後很美味的吃著。

也有眾人各自帶來的其他料理、酒、點心等東西，簡直就像是一場豐收祭。

「沒有工作，無法結婚，這樣的人只要對這個村子感興趣，不管來多少人朕也會接納。然後只要有充足的時間，佐奴塔克王國就有十足的可能了」

「陛下，關於那方面的準備正在進行中哦。已經決定開始販賣這個村子作物的專賣店了。而且採用了只要把生產者的名字和面孔全都告訴給客人，那麼即便價錢稍微高些客人也會買得安心的制度」

「噢噢！真是出色的方法不是嗎！」

怎麼說呢。

那種做法，在日本可是非常理所當然的啊……。

「還有其他好幾個廢村開始了再生，在其中選一個距首都最近的吧產品都集中到那裡，然後開設定期集市吧。使用村子作物為材料的特產商品也在開發了。販賣這些的城市就以『道之驛』來命名吧」

「真是個好主意啊！萊拉」

「……」　

那個……萊拉小姐。

那些做法，對於日本的農村都是再理所當然不過的事哦。

所以實在很頭疼該說什麼好……。

「這麼下去村子的規模就能很順利的擴大了，佐奴塔克王國的復興一定能實現的吧」

「噢噢！正可謂王國復興的千年大計吶！」

「我說，威爾」

「反正說不定真能這麼復興王國，當事者本人們又那麼開心。所以就別太在意了吧」

伊娜看上去有話想說的樣子，結果被我搶先截停了。

「鮑麥斯特伯爵，我拿報紙來了」

結束參觀農村後的來週一之日。

新聞記者廬米拿著當天的每日專刊來了。

記載了魔王大人和宰相主持的王國復興運動……雖然只是她們自己這麼認為的，或者說是由廬米採訪的農村復興運動報導的報紙，被拿到了我們眼前。

報導中，也記載有被佐奴塔克共和國政府扔著不管的我們訪問了那個農村這件事。

「這個，會被當成有外交企圖嗎？」

「只有白痴才會有那種想法的哦。再說，那個農村的人們其實並沒有脫離共和國的統治嘛」

廬米笑著回答了泰蕾莎的問題。

號稱目標是獨立的人就只有魔王和宰相兩人而已，村民們……現在因為那裡還被視為廢村所以他們其實也不是正式村民因為多少有些收入，所以雖然很少但也有在繳稅。

沒人會將好好納稅的人視為想要脫離國家的人，所以在外部的人看來這就僅僅是一場農村再生運動罷了。

「說的也是。首先，他們是不可能贏過防衛隊的吧」

沒有武裝力量當然打不過了。　　

陪著我們一起去的防衛隊的人，都感覺不到他們有任何威脅。

因為村民們只被他們視為一群從事農業者所以這是理所當然的。

反倒是村民們送護衛隊食物時，他們會戰戰兢兢的用『抱歉，這樣的東西我們是絕對不能收的』的話拒絕。

可能是因為有廬米在，所以擔心自己會被視為接受了賄賂進而陷阱麻煩中吧。

『我是覺得這種程度沒什麼關係啦，但那些記者前輩中確實存在會以僅憑這些就開心的對護衛隊展開批判的人，所以也能理解他們為什麼這麼警戒』

雖然我的看法也不夠全面，但我覺得防衛隊裡的人還是為人處事認真的人比較多。

「話說回來，聽說您今天就要返回鮑麥斯特伯爵領去了呢」

「因為就算在這裡繼續呆下去，狀況也不會有任何改變」

雖然帶著妻子和孩子們試著進行了親善外交，但外交交涉那邊完全沒有任何變化。我們幾個雖然最開始受到了關注，但民眾很快就沉迷於其他話題對象了。

佐奴塔克共和國的國民，基本上都對外國的事沒什麼興趣。

恩斯特這樣的魔族似乎真的是很特殊的存在。

「陛下說不定還會給我什麼命令，下次會用『瞬間移動』立刻趕來的」

「真讓人羨慕。移動幾乎不花時間什麼的」

魔族中沒有人能使用『瞬間移動』。

不過作為代價運用了魔導技術的載具就進化的很厲害，我覺得還是魔族的做法更先進些。

「鮑麥斯特伯爵，我們幾個被免職了嗎？」

「因為我不能帶上你們一起走」

莫爾他們是魔族出身的外國人。

恩斯特這樣已經被陛下默認的人還好，要是我把莫爾他們也帶回去會產生各種問題的吧。

「訂下約定的那天，我一時間還以為從此會過上金色的人生了」

「明明第一次領到工資了，可還是短期的非正式員工！」

「嗚哦———！逃避了新人勞苦期的我們還有成為正式職員的道路嗎？」

「這個世道，為什麼這麼殘酷！」

聽著三個人的話，我的心越來越寒。

沒想到，會聽到魔族之國的就職殘酷故事……。

「那個，你們去魔王大人的農村不就好了嗎？那裡的話，說不定也會有發掘類的工作吧」

看不下去的麗莎提出了莫爾們去上週去過的農村工作的意見。

「還有這個方法嗎！」

「確實說過會接受所有希望加入者的話吶」

「去那裡的話，說不低就能結婚了！」

因為未來又有了希望，莫爾他們三個的情緒一下子高漲起來。

「「「萊拉小姐————！」」」

萊拉小姐嗎……。

的確是個美女，但莫爾他們究竟有沒有可能能追到她呢？

「對了！只要好好輔佐她的話！」

「行得通！」

「才不會輸給你們！萊拉小姐————！」

三個人接下我給的報酬後，立刻飛奔前往萊拉小姐身邊去了。

「吾輩的學生們，終於可以就職了吶」

在旅館整理好行李後我們登上了自己的魔導飛行船離開，暫時滯留過一段時間的佐奴塔克共和國很快被拋在了身後

然而，關於兩國的交涉現在還沒有抓到任何解鎖關鍵。
