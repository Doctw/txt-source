即使是冬天莉雅仍在天亮前就醒了，她看了一眼還在熟睡的薩基後，便起身開始換衣服。

從側邊的走廊來到庭院裡，接著拔出虎徹。
這是從未改變的早晨練習習慣，刀子在空中揮舞著，發出撕裂空氣的聲音。
她在自己腦中描繪出完美的姿態，然後緩緩的不斷用刀描繪出那完美的型。

莉雅感受到氣息，因此將刀朝向氣息的來源，那是完全沒發出腳步聲就出現在側邊走廊並靜靜看著莉雅的菊池。

「看來你還真的是田村阿，要不要互相比試一下？」
「你這番話雖然讓我感到很高興⋯」

莉雅撿起一顆小石頭，接著用手指粉碎了那顆石頭。

「因為我一半以上已經不是常人了，還是別比試的好」
「剛剛那個是魔法？」
「不，這只是單純的握力而已，就算是鑽石也能捏碎吧」
「這還真的不是人類阿⋯」

她跟來到庭院的菊池開始談起了武術相關的事情，而菊池非常好奇在那個世界裡，武術真的有辦法派的上用場嗎。

「這個嗎，倒不如說在日常中發揮的作用比較大呢，因為戰爭的時候幾乎都是騎在馬上的」
「戰爭阿⋯殺了很多人嗎？」
「直接用刀斬殺的⋯差不多三百人左右吧，阿不對，其實大多數都是用長槍殺死的就是了」
「那弓箭呢？在戰爭中應該是弓箭殺的人最多吧？另外那個世界沒有火槍嗎？」
「以人類為對手的時候都不太使用弓箭呢，因為用魔法還更加確實。至於火槍的話⋯說起來還真沒有想過呢，不過相比之下同樣也是魔法比較實在阿」
「你也能使用魔法對吧？」
「是的，就像這樣」

莉雅當場飄浮了起來，接著緩緩的降落在菊池身旁。
菊池驚訝的合不攏嘴。

───

「田村先生，早餐已經準備好了，也請您的同伴一起來享用吧」

聽到了傭人的傳話後，莉雅一腳將薩基給踢醒並讓他整理服裝儀容，接著來到客廳後兩個人在御前的對面坐了下來。

「歐歐─久違的日式早餐！」

薩基眼睛為之一亮，說了句我開動了之後便開始吃起早餐。

「那麼的話、田村⋯不對，現在已經不適合用這個名字來稱呼了呢」
「叫我莉雅就好，我已經習慣這個名字了」
「那麼莉雅阿，昨天晚上已經派人去召集人手了」

不過更正確來講是在魔王發表宣言後，他們就固定每天會召開會議。

「都是些實際上掌控這個國家的人，也包括我在內」
「我知道了，那麼會議是幾點開始？」
「會在午餐結束後開始，這麼說來你是有其他預定的行程嗎？」
「⋯也是呢，雖然想跟家族的人見上一面⋯但用這個身姿過去，他們大概會難以理解吧」

想讓他們理解的話，必然會花上很長一段時間，怎麼想都不可能在中午前結束。

───

在預定的時間到達前，莉雅跟御前談了非常多政治性的話題。

像是對於魔王的宣言各國所做出的反應等，而美國一點也不例外的基於自由與正義之名，主張要捍衛地球，至於俄國與中國也都表示贊同，但老實說他們大概是怕那一片未開拓的處女地被人搶先佔領走吧。

日本則是打算追隨美國的腳步，但由於魔王阿魯斯指定的會面場所是日本的國會大廳，因此許多國家也質疑著日本是不是跟異世界有所關聯。

「一開始就傳遞這些資訊給我們的話，這樣我們不就能做出對應了嗎」
「這說的也是呢，不過對他來說，這些完全不構成威脅」
「那個魔王真的有這麼強嗎？」
「他本身說起來非常強，但跟我相比的話會是我比較強一點。只是他還有魔族之國在，那個國家的文明是完全不同等級的，所以根本贏不了」
「⋯如果讓你去打倒魔王也沒有用嗎？」

現在想想的話，的確會想到這個手段。
但如今在地球與異世界相連的情況下，已經沒有其他選擇了，更何況魔王擁有神竜的支持這個決定性的手牌。

「當提到要狙殺魔王的時候，就時機來說已經太遲了」

是的，魔王的人魔共生理想，現在想想也仍覺得很不錯。

當說起要在大崩壊時破壊另一個世界的時候，莉雅也是站在贊成方的，但等到知道另一個世界其實是地球的時候，已經錯失了與魔族開戰的機會了。因為自己的國民中也有所謂的魔族，自己對他們也有著留戀。

像是讓雷姆多利亞實質戰敗並導向雙方和談也好，這一切都早在莉雅誕生前就緩緩且慎重的在進行著了。

「也就是說，等那傢伙到達這裡後，彼此交涉才是主軸嗎」
「先不論是否有辦法交涉⋯他是個非常強大的對手這點是無庸置疑的」

莉雅一邊喝著味噌湯，一邊想著在另一世界的那個男人。

───

在國會大廳某個受到嚴密警戒的會議室裡，有著數十名男性在內。

裡面包括了現任的大臣們、高級官僚們以及在商界呼風喚雨的大人物，換句話說的確是一群實際掌控國家的人。
而這些大人物在御前進入會議室時，全都有志一同的向他低頭致意。雖然他本人說自己已經退休了，但他的影響力卻一點也沒減少。

「好了，抬起頭來吧」

眾人隨著這句話抬起頭來時，紛紛對於跟在御前身後進來的兩人感到吃驚。
一個是有著黑髮金黃色眼瞳的超級美女，另一個則是年紀看起來差不多是少年的男性。

「總之先向各位介紹一下，他們是從那邊世界來的莉雅小姐以及薩基先生」
「我是奧古斯公國大公琉庫蕾雅娜・庫里蘇托魯・卡薩莉雅・奧古斯」

莉雅是穿著西裝長褲來的，雖然在威嚇的意義上穿著那邊的正式服裝會比較好，但她想避免過度引人注目。

「我是奧古斯大公國男爵薩基塔留斯・庫里蘇托魯・克羅利」

御前則是開始一一介紹著會議室中的男人們，不過對莉雅來說要記住這麼多人名實在很麻煩阿。
薩基則是不知道從哪裡拿出筆記本與筆，將這些資訊全都記載下來。

「那麼、讓我們進入正題吧」

御前與莉雅首先就座，接著眾人也跟著一一就座，
漫長的談話就這樣開始了。

「所以說⋯首先我想了解一下那邊的世界，您⋯稱呼您為閣下可以嗎？用日本話能溝通嗎？」
「不管是閣下還是直接叫我琉庫蕾雅娜都可以，不過其他人一般都是用陛下就是了，另外日本話也沒問題」
「那麼陛下，想先問您那邊的世界是什麼狀況，但⋯」
「薩基，地圖」

薩基用魔法在半空中顯示出竜骨大陸的地圖，在眾人感到驚訝的同時，莉雅則是開始解說起來。

「這塊藍色面積最大的土地是魔族領，是由魔王阿魯斯治理著，而在南邊這第二大的雷姆多利亞王國，西邊第三大的則是奧古斯大公國。雖然還有其他國家存在，但基本上是以這三個國家為主做出決定的」
「那個、陛下，您所提到的這個魔王，是字面意義上的魔族之王嗎？還是說有著不同的意義呢？」
「不從這裡談起不行嗎」

莉雅抓了抓頭，不過只有人類存在的地球本來就很難理解這個部分吧。

「在那個世界裡擁有知性的生命體主要分成三大類，人類、亞人以及魔族，其中人類是最晚出現的種族，不過人數應該是最多的。至於亞人與魔族則各自又能分類成非常多不同的類型，而魔王則是統治魔族的王」
「那個，可以更詳細說明一下那個魔族嗎？他們跟人類是敵對的嗎？」
「過去曾經彼此敵對過，而他們至今也沒有完全文明化，因此還是有一部分的魔族是敵對的。對於同屬魔族的其他人來說就像是野蠻人般的存在，順便一提，就算是人類間也是有野蠻人存在的」
「那麼所為的魔族⋯具體來說跟人類有什麼不同呢？」
「要分類魔族還蠻難的⋯與人類不同的亞人，以及跟人類敵對的都是魔族，不過那是以往的定義了，如今在當代的魔王侵略之下，魔族與人類最後達成和平了，如果真的要舉個例子的話⋯吸血鬼是魔族之一」

這段話引起一陣騷動。

「吸血鬼⋯是嗎？」
「如果以地球上比較有名的來舉例的話，哥布林、黑暗精靈或狼人都是」

───

之後因為這些人的好奇心而持續問著相關的問題，不過這一切完全不是今天原本預定要討論的主題就是了。

「也就是說是這個魔王非常積極的想要破壊地球是嗎？」
「雖然構築這一切的是魔王沒錯，但實際破壊地球的是神竜，更正確的來說應該是⋯魔王基於神竜必然會毀滅地球的情況下而做出這一切吧」
「那個神竜是什麼？」
「殺死神的竜，在那邊世界生存超過數十億年，相當於神的存在。他們為了守護那個世界，對於摧毀地球毫不猶豫，魔王則是暫時制止他們，並打算讓少部分地球人移民，而產生現在這個狀況」
「依陛下您這麼說的話，其實是可以不用這麼麻煩的移民，可以直接就將地球摧毀囉？」
「就是這樣沒錯，所以這個該怎麼說⋯算是魔王個人的傷感吧，因為他原本是日本人的關係」
「日本人！？」
「他在１５歳的時候被召喚到了那個世界，接著把前代魔王打倒，然後導引支離破碎的魔族邁向文明化，如今已超過一千年了」
「人類在那個世界可以活超過一千年嘛？」
「使用魔法或是拜託神竜的話就辦的到，我自己的壽命也預估至少數億年以上」

數億。

人類的歷史最多也不過是以萬為單位而已，看著眾人一臉驚訝的表情安靜下來後，莉雅耐心的等著他們回神。

───

終於開始談論正題了。

說起來莉雅來此的目的主要是傳達移民以日本人優先這件事而已。

「話說回來，到底該怎麼將數千萬人經由空中送到那邊去呢⋯」
「關於這點，我們這邊有辦法做到。只要使用浮游大陸的話，一次可以運送數百萬人，只要來回個十趟就可以達成了」
「浮游大陸⋯」
「那是總長１２０公里的飛行島嶼，可以進出宇宙。說起來這座島原本就是為了探索宇宙而製造的阿」
「可是設備跟設施的話⋯」
「那也是由我們這邊負責，只需要約十年，應該至少可以達到１９８０年代的生活水準才對」
「太蠢了」、「國民是不可能接受這種事情的」、「這也太過自說自話了吧」

那些充滿敵意的視線，對莉雅完全不痛不癢。

「各位似乎搞錯一件事情了呢⋯」

莉雅站了起來對眾人施放了竜眼，僅僅如此那龐大的壓力感就將眾人緊緊壓在椅子上了。

「這可不是交涉、不是拜託也不是命令，只是個提案而已」

莉雅露出微笑繼續說著，

「這只是個這樣的提案而已，是所有人類跟地球一起毀滅呢，還是至少讓一小部分人類得以存活下來，在這兩個選項中選擇一個而已」

會議室再度陷入沉默之中。

「請稍為給我們一些思考的時間」

至今一直保持沉默的御前開口了。

「莉雅你們可以稍微在外面等一下嗎」

───

莉雅與薩基兩人一邊喝著自動販賣機的咖啡，一邊背靠著牆壁站著。

「⋯結果會是如何呢？」
「今天大概不會有結論吧，因為還要跟美國溝通過吧？」
「美國嗎⋯中國與俄國該怎麼處理呢？」
「這兩個直接無視吧，尤其是中國本土的移民是不打算接受的」

這是她跟阿魯斯兩人討論出來的結果，那就是不接受中國的移民，如果是台灣的話另當別論。

「問題在於伊斯蘭教徒呢─」
「是因為聖地麥加將不復存在的關係吧，不過說到這個基督教徒也有同樣的問題阿」

如果是虔誠的基督教徒的話，就直接將這個事件當成最後的審判然後就這樣赴死是最好的，不過美國大概會盡全力進行抵抗吧。

「擋的住洲際飛彈的攻擊吧？」
「大概沒問題吧，那邊的防守已經都交給神竜負責了」

不過說實話，神竜守護的是「世界」，而不是人類或魔族。

只是萬一人口遽減也會對他們造成困擾就是了，而這一點已經確認無誤了。

───

將近一個小時後，莉雅與薩基再度被請回會議室內。

而他們所得出的結論幾乎跟預期的一樣。

那就是「尚未決定」

他們必須跟同盟的美國商討後才能做出決定，對此莉雅輕輕的點了點頭。

「反正也不急著在這一時半刻所以沒關係，不過話先說在前頭，如果是想仰賴美國的軍事力量的話，最好還是放棄比較好」

莉雅一邊露出善意的笑容，一邊說出了跟魔王一樣的宣言。

「一但有國家針對那個世界發起攻擊的話，必然會將那個國家從地圖上給抹除掉」

───

「那麼就在這告辭了」

離開國會大廳後，莉雅向御前告別。

「你打算去哪呢？」
「打算去找原本的家族，最糟糕的情況下至少要守住自己的親人」

薩基也暫時在這離開並單獨行動，至於那些大人物們到底會將自己的話聽進去多少呢。

「御前您也挑選一下要帶到那個世界的人員會比較好」
「⋯跟全世界為敵的你們，真的有辦法贏嗎？」
「說實話完全沒有所謂的勝負吧，如果是以佔領為目的的話或許還能分勝負呢」

尤其是一但演變成游擊戰的話，戰鬥時程必然會拉的很長吧。但是魔王跟神竜的目的本來就不在於領土或資源，而是破壊地球。

「那麼我先走一步囉」

薩基施展了轉移，來到了前世生活的故鄉。
莉雅也微微行了個禮後飛到天空中。

送行的人們全都看傻了，御前接著對著他們開口說著。

「好了，讓我們開始工作吧。雖然很不合理，但至少還有希望存在著」

御前在心中默念著『這算是最後一次為國家出力了吧』