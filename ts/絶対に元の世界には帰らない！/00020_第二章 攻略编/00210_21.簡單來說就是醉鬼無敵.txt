之前提到的巧克力似乎用了相當好的酒，味道相當不錯。明明還是吃飯前卻吃了一堆，不過影耶的樣子吃掉的東西也是變回魔力，所以只要願意吃多少都不是問題。

作為點心稍微吃了點巧克力後，我們來到一家好像很高級的店。

「然後──」
「嘿，是這樣──」

夏緹亞她們好像在聊著什麼，但腦袋一時跟不太上。
最近有點忙，可能是累了也說不定。

光彌似乎是發現我的樣子有點奇怪，過來對我搭話。

「⋯⋯影耶，你的臉很紅沒事嗎？」
「唔嗯？是這樣⋯⋯嗎⋯⋯？」

我用手摸了下脖子，感覺有些微熱⋯⋯是感冒了嗎？不對不度，最強美少女影耶怎麼可能會感冒呢。

不過姑且還是補充下營養好了。⋯⋯欸，不過這個身體吃什麼⋯⋯那個，會怎樣來著。算了怎樣都好。

總之就隨便吃點料理吧。⋯⋯恩？總感覺都沒什麼味道呢。
想說要漱個口的我拿過現在剛上的飲品開始喝。

「啊，影耶，那個是酒──」

安古好像說了什麼，不過那也是在我喝了之後的事情。

※

因為影耶的拜託，我也一起來幫忙殺掉勇者。

說實話這我只感覺很麻煩而已，但我更不想影耶回到原本的世界，最重要的是這還是我可愛女兒的願望。
為此，就算是王女完全讓人提不起興趣的對話也得打起精神認真聽。

「然後，光彌就一擊把白色惡魔給砍成了兩半！」
「嘿，是這樣嗎，光彌真厲害呢，真不愧是勇者。」

然而，那個可愛的女兒本人，現在看起來卻有點呆滯的樣子。⋯⋯本來應該是要在旁邊輔助我的對話不要出錯才對，是怎麼了嗎？

「小影耶沒問題？」
「恩⋯⋯？夏緹亞⋯⋯？」
「叫媽媽。啊啊，這個是酒，不要喝比較好喔。這個是水，喝這個就好」

雖然不太為人知，但高級的酒是一種魔法藥。就算小影耶能把食物粉解成魔力，攝取帶有魔力的酒精還是有可能會醉酒的。

⋯⋯不過，明明每次都說但小影耶就是不肯叫我媽媽。每次都叫我母親敷衍了事⋯⋯⋯

「媽媽⋯⋯謝謝⋯⋯」
「欸！？」

突然如願以償被叫媽媽的我立刻回頭看向小影耶。
雖然感覺是不小心叫出來，但感覺沒有在害羞的樣子，也沒有要訂正的意思。

難道說，小影耶──

「⋯⋯喝醉了嗎？」
「欸欸⋯⋯？啊哈哈，最強美少女影耶怎麼可能喝醉呢～⋯⋯？」
「（已經醉到神智不清了！）」

這、這該怎麼辦才好。就我個人來說還想看小影耶更醉的樣子，但這個情況下這樣做的話之後肯定會被小影耶討厭的。角色崩壊肯定會生氣到不行。而我不想這樣。

「最、最強美少女⋯⋯？」

不行，角色已經崩壊在即。得想點藉口才行。

「小影耶真是的，還是一樣不善常開玩笑呢，大家可要當真嘍？」
「嗯呣⋯⋯？是嗎⋯⋯？」
「⋯⋯！」

好、好可愛！世界第一可愛！平常冷酷美女的演技完全不見，現在這軟呼呼的毫無防備感！而且因為精神是男性（陰矢）的關係，所以對現在的狀態沒有任何一點危機感！給人一種更加無防備的感覺！太可愛了！

「那個，小影耶好像有點累了所以就──」
「影耶才不累⋯⋯」

不妙，第一人稱變成自己的名字了。不對，對陰矢來說影耶才是第三人稱。真夠複雜。

「⋯⋯那個，難道說影耶喝醉了──」
「不是的！這是那個，就是⋯⋯」

想不到什麼適合的藉口。拼了命也得想出來，只要能隨便找個理由離開這裡，就能用轉移道具回去──

「影耶果然感冒了──」
「就說沒有感冒了⋯⋯你看⋯⋯對吧？」
「──？！」

小影耶抓過光彌的手，放上自己的脖子，並對他甜甜的笑了。

⋯⋯不、不妙！一擊墜落！光彌要被一擊墜落了！這絶對是心動了沒錯！

啊啊，王女用恐怖的表情瞪著這邊。那可不是公主大人該擺出的表情啊。這修羅場究竟對誰有好處，就現在這混沌一般的狀況。

「比起這個⋯⋯想喝那個，幫我拿⋯⋯」
「影、影耶，我覺得不要比較──」
「⋯⋯為什麼，要拒絶我⋯⋯？」
「因為──」
「啊啊討厭，不要說了⋯⋯」

影耶寧靜地放出多到誇張的黑色魔力，神光國的黑夜變得如同在幻視深淵一般漆黑。這明顯不是人類──或者說是一個生物可以擁有的力量。怎麼考慮都是力量的控制出錯了，平常無意識的透過精神箝制，但現在名為自重的枷鎖已經徹底鬆脫。這個狀態的小影耶就算讓她和魔王戰鬥感覺無關無敵的障壁什麼也能普通獲勝吧。
總之不早點離開肯定大事不妙。

我拉起還手握酒瓶的小影耶的手，從位置上站起來。

「那個因為這樣，所以明天見吧！啊，王女大人真的很對不起！不過我想她沒有惡意就原諒她吧！」
「啊⋯⋯媽媽⋯⋯搖搖晃晃的⋯⋯」

⋯⋯咕，好可愛！好像現在直接抱緊她！

一邊忍住自己的慾望，我一邊把影耶帶到店外，然後躲到陰影處，從她的懷裡找出轉移道具，轉移到平時的地下室。

回到地下室的我鬆一口氣。展開多層陰矢特製的防御結界和隔音結界的這裡，不管怎樣亂來應該都沒問題。房間裡面會怎樣就不知道了。

「呼⋯⋯好了，回到家了⋯⋯」
「恩⋯⋯？歡迎回家⋯⋯？」
「好好好，我回來了～。真是的，有夠可愛的呢」
「欸嘿⋯⋯是吧⋯⋯？」

恩，太棒了！好了好了，繼續喂酒吧。
小影耶的話不管喝多少也不會中毒倒下。身體能力以及連帶的回復能力都拔群的高，就算讓她這樣直接睡去，明天也不會宿醉可以清爽的醒來吧。

「好了，酒在這裡。媽媽在這所以安心喝也沒關係唷」
「恩⋯⋯咕⋯⋯」

小影耶拿起杯子慢慢地開始喝起酒。

「呼哈⋯⋯」
「好可愛～！」
「嗯⋯⋯很癢不要這樣⋯⋯」
「對了！洗澡，好久沒有一起洗澡了！」

雖然聽說醉酒的人去洗澡會因為血液循環變好而更醉，不過應該沒事吧。就算真的危險用改變魔法把酒精提取出來應該就沒問題了。

我脫下一年前為了影耶的冒険者出道，和陰矢兩人一起設計製作的裝備。

⋯⋯唔嗯，真是完美的身材。不得不稱讚自己的手藝啊。

我把小影耶抱進浴室。平時要是擅自這樣做大概會被用奧里哈剛鋼纜給綁個一天半日吧。不過現在不僅毫無防備還毫無抵抗。
決定了，今天我要好好玩弄玩弄一番！

※

玩太過火被殺掉了。不是比喻，就是字面意思。

「要、要是沒有之前給的復活藥就真的死了⋯⋯！」

討厭我惡作劇的影耶，給了我完全沒有手下留情籠罩驚人魔力的一擊。嘛就算死了也只是被強制送還魔界而已。

⋯⋯恩，我想那時候對那邊那樣果然還是有點過了。接下來還是稍微自重點好了。

小影耶則是穿著有著可愛貓耳的輕飄飄睡衣睡著了。就像什麼事情都沒發生，在床上縮著身體安靜地睡去。

「⋯⋯只是一起睡應該可以吧？」

我在小影耶旁邊躺下，蓋上棉被。

恩，因為我是母親，所以一起睡也是OKOK～
嘛，魔人沒有什麼親子，所以母親該做什麼我也不太清楚⋯⋯但不管這個，這孩子和陰矢也是少數我重要的人。才不會讓給勇者什麼的。

「明天得早起，準備各種事情才行⋯⋯呼啊」
「呣喵⋯⋯」

打哈欠的我抱緊小影耶，落入夢鄉。