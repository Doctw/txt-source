士康遞運輸的機師們，由幾人輪流交替，無休止地一直不停往帝都前進。

不過，在無休止地移動了超過了40小時的時候。
為了去帝都避難而一同移動著的愛克羅賽的居民們，疲勞達到頂峰了。
雖然為了如廁和吃飯而停下過不少次、在車廂裡睡覺也非不可能的事，但畢竟長時間逗留在搖晃的密閉空間裡的話，疲勞便會一點一點地累積起來的。
克里普托，雖然本來似乎是打算就這樣不在任何地方中途落腳便直接前往帝都的，但或許是判斷果然這樣做是不可行的吧，他決定暫且去途中的城鎮落腳。

建在河邊的城鎮，福隆。

充滿綠意的這個城鎮，直率地說就是鄉村小鎮。
不過由於這裡離帝都不遠，所以似乎備有一定程度的住宿設施。
況且，如果身為四將的克里普托來交涉的話，城鎮的設施，也可以讓給愛克羅賽的居民暫住的吧。

「雖然現在是每一刻都彌足珍貴的情況，但也不能胡來勉強脆弱的普通市民嗎。士康遞運輸的人似乎也累壞了呢」

從車廂下來，看著樣子放鬆了的愛克羅賽的居民們，克里普托如此說道。

「和像是肌肉塊似的傢伙在一起的普通市民也很辛苦呢—」

或許是相當合不來吧，聽到克里普托的自言自語後芙蘭西思不滿地說道。
百合露出了苦笑。
確實，在車廂中長時間移動是非常夠受的。
腰又痛，身體又沉。

「拉比先生意外地好像還好呢」
「因為我習慣長時間的移動了，所以沒有體力的問題哦」

見習商人不是說說而已的，就是這麼一回事嗎。
看著與拉比進行這樣的對話的，抱在自己的手臂裡的艾爾萊亞，百合想對她提出同樣的問題。
“意外地好像還好呢”。
她的樣子和平常沒甚麼不同，但明明，岬不在這裡。
於是，也許是察覺到注視著自己的百合的視線了吧，艾爾萊亞抬頭上看詢問道。

「怎麼了，百合」
「嗯？不……明明岬那樣了，但你好像還好甚麼的」
「我不是還好。但是，因為我相信著。除了相信之外我沒有其他的選項」

恐怕，如果岬死了的話，艾爾萊亞便會毫不猶豫地結束自己的性命吧。
與其說是愛，不如說是視為同一。
自己的存在價值，是只是為了岬而已。
聽到毫無動搖的艾爾萊亞的話語後，百合再次深切地感受到。
自己，“還差得遠呢”。
也許是察覺她這樣的心情了吧，艾爾萊亞溫柔地微笑著教誨道。

「思緒的形式是因人而異的，我並不認為現在的自己是端正的。如果要說哪邊是正常的，那就是百合你那邊」
「可是……」
「正因為愛著才會擔心，才會不安。這不是很正常嗎。我只是以我的方式來愛著而已，所以請不要拿來比較。百合應該對自己的心情昂首挺胸」

近乎崇拜的艾爾萊亞的愛情。
始於依存的百合的愛情。
形式因人而異，因此如何掛念不在這裡的岬，也是因人而異的。

「謝謝，艾爾萊亞」
「呼呼呼，不用客氣」

稍微恢復了一點自信的百合，露出了淺淺的笑容。

克里普托去了福隆鎮長的家，似乎是去進行今天的住宿處的交涉。
剩下的其他人，在他回來之前都很閒。

「克里普托過去了，我也過去會不會比較好呢—」
「我認為還是不要去比較好哦」
「誒—，為甚麼—？可是我也是四將哦—？」
「因為不適合啊，要是突然生氣起來把鎮長殺死的話，困擾的可是我們」
「拉比你是不是把我當傻瓜了？話先說在前頭，我可是也能做到這種程度的區分的啊！除非說了很失禮的說話否則我是不會殺掉的！」
「請倒是說一定不會殺掉啊……」

芙蘭西思雖然對拉比比較粗暴，但似乎是相當中意他的，在空閒的時候，兩人（大概）在愉快地交談著。
另一方面，百合忽然想起了某件事。
從愛克羅賽避難的時候，岬在顯現烏璐緹歐之前，就把裝有自己行李的袋子放了在當場。
因此，裡面當然放有——奧拉克石。

「難不成，岬把與普拉娜絲小姐的對話一事交托給我們了嗎」
「或許把赫洛斯的那件事也傳達過去會比較好吧」

對於艾爾萊亞的說話，百合「嗯」地點了點頭，然後嘗試與普拉娜絲通話。
「喂喂」這樣對著石頭說後，過了一會兒就傳來回覆了。

『難道，是赤羽同學嗎？』
「是的，我是岬的代理。她為了阻止偉月……啊不是，為了阻止桂，獨自留在愛克羅賽了」
『也就是說，赤羽同學你們去避難了嗎。以及她生死未卜，嗎』

普拉娜絲輕輕地嘆了一口氣。

『莫非，你們是要去帝都嗎？』
「是的，克里普托……那個，是四將的其中之一，那人正為我們帶路」
『我知道這件事，是由他在帶領著對吧。那正好，能讓克里普托先生來通話嗎？』
「誒，這樣可以嗎？但是岬好像想隱瞞普拉娜絲小姐你的存在……」
『因為這是緊急事態，暫且不論能不能防禦，如果能事先傳達大概是要前往帝都的戰力的話，應該也就能夠制訂出作戰計劃的了』
「也就是說……有敵人從王都出發了對吧」
『對，還要是整整3架被奧利哈魯鋼徹底逮住了的阿尼瑪。赤羽同學的話應該也認識的吧，是鞍瀨、嶺崎、吉成這3人』
「啊啊，那3人也被污染了嗎」
『是水木的緣故。不過，現在這樣也只是時間的問題了』

毫無疑問，是一起被召喚了過來的同班同學。
而且，也是日前被教會抓到、被污染了的3名學生。

『然後還有尋求流亡者、國王的計劃等等，我有很多想要傳達的事情哦』
「克里普托先生，現在好像有點忙，可以請你稍微等一下嗎？」
『畢竟沒有預約就希望能夠談談的是這邊，所以無論多久我都會等的。反正我也很閒』

百合，決定等到克里普托與鎮長說完話後從建築物裡出來。
當她在想，在等待的這段期間有沒有其他需要傳達的事情的時候——艾爾萊亞開口了。

「索蕾優小姐的事，不用傳達嗎」
「啊，對啊。如果是用阿尼瑪移動的話，可能差不多也快抵達王都了呢」

岬之所以讓索蕾優憎恨自己，是因為岬預想到她為了復仇會前往王都。
因為要追上去了帝國的仇人，依靠軍隊是最快的辦法。
如果是戰力正在漸漸耗盡的王國軍的話，只要聽到她是阿尼瑪使的話應該就會立馬飛撲過去的吧。
不過，前提是，軍方還正常的話。
目前的情況就是那樣子的，所以如果索蕾優最初接觸到的人是普拉娜絲、或者艾薇的話就好了。
在受到污染之前，必須設法把她拉攏過來。

「普拉娜絲小姐，我想差不多應該會有一個叫索蕾優的阿尼瑪使到達王都的了」
『索蕾優，嗎』
「她是蒙斯城鎮的倖存者，應該對岬抱有強烈的恨意。而且為了追上去了帝國的岬，她應該會想和軍方接觸的」

沒有聽過的名字，莫名其妙的經過。
普拉娜絲的腦內冒出大量的問號。

『那是甚麼，為甚麼要做這種事情？』
「我想，岬大概是在想，這樣做或許能幫到普拉娜絲你們，於是就這樣做了」
『確實，現在是水深火熱到連貓的手也想借的境地……但是可以信任嗎，那位叫索蕾優的人』
「她是個好人哦，雖然有點容易被騙的部分」

否則，應該是不會盡其所能為殺害父母的仇人效力多年的吧。

『原來如此，那真是剛適合的人呢。似乎會是很好處理的』

於是，索蕾優又將再次被人利用了。
話雖如此，如果是普拉娜絲的話，是不會利用她來做壞事的吧。
而且，在現在的王國，比起不慎重地單獨行動，雖說是被利用但好歹也是和普拉娜絲一起行動是會安全得多的。

『那麼，白詰同學送來的禮物，就讓我心懷感激地加以使用吧』
「如果可以的話還請你溫柔地對待她呢，畢竟岬似乎也很在意索蕾優。……噢，克里普托先生回來了，那我換人來說了」
『好的，拜託了』

百合跑近從建築物裡出來的克里普托。
他看著接近過來的百合時露出了詫異的表情，然後在「給」一聲地被遞過陌生的石頭時眉間就出現了皺紋。

「這是，甚麼啊」
「這連接著王國的間諜，普拉娜絲小姐想和克里普托先生談一談」
「普拉娜絲……？難道是，王國魔法師的那個！？」

看見克里普托一下子起了勁的樣子，百合確切地感受到普拉娜絲是何等的大人物了。
因為這樣的人物居然在做間諜，所以才更加教人驚訝。

「我對著這塊石頭說話就可以了嗎？」
「是的，我認為這樣對方已經能聽見的了」
「是嗎……那首先走去沒人的地方吧，避免被第三者聽見」

---

克里普托快步移動到建築物的陰影處，帶著緊張的樣子對著奧拉克石說道。

「我是帝國軍上將，克里普托・扎福尼卡」
『很高興能與您通話，“帝國之劍”的克里普托閣下。我是普拉娜絲・色卡摩』
「沒想到居然會在這樣的地方與被譽為能留名於王國歷史的天才的您交談呢。看來是被捲入麻煩事裡了」
『彼此彼此，我在送出赤羽同學的時候，也是無法想像居然真的能與四將有所聯繫』

在說出適度的和睦的社交辭令後，普拉娜絲立即開口說出了正題。

『今天是有事想請求克里普托閣下』
「請不要使用閣下這種拘謹的用詞啊。如果對方是一名兵卒就算了，但很不巧，我並沒有能在普拉娜絲閣下這樣的大人物面前擺起架子的粗神經和厚臉皮啊」
『那麼就冒昧地稱呼為克里普托先生，吧。您也請隨便地稱我為普拉娜絲就好』

對於她的戲言，克里普托以「哈哈哈」的乾笑聲回應。
到底，那是想開玩笑呢，還是認真的呢。
對於聽著兩人的對話的百合來說，她怎麼都不認為會是後者。

『那麼，立刻進入正題吧。其實在昨晚，有足足6名阿尼瑪使從王都逃走了』
「那可是大事件呢，對於現在的王國來說，他們是很寶貴的戰力吧」
『您說得沒錯，警備的漏洞到底是誰製造出來的呢。不過，先把追究責任的事放在後面，他們的目的地似乎是帝國』
「是希望流亡過來嗎」
『恐怕是的。那6位是，為了我們王國的方便而被呼喚過來的，也即是受害者的少年少女。就這樣死在荒野的話事後會感到各種不是味兒的哦』
「所以想讓我們收容他們，嗎？但很遺憾，我不是沒有回報也會收容他們的老好人」

而這也非普拉娜絲沒有考慮過的情況。
不過，普拉娜絲硬是押在說不定他是個出乎意料的老好人的可能性上，繼續著交涉。

『我認為，阿尼瑪使，僅僅是這一點就已經對帝國有充分的好處了』
「6人中潛伏著間諜的可能性呢？」
『現在的王國沒有那樣的餘裕』
「為甚麼能如此斷言，就算說是王國魔法師，也不可能掌握王國所有的情報的」
『不過，王國已經完蛋的事我是掌握得到的』
「……甚麼？」

用冰冷的語氣斷言道的普拉娜絲。
聽到這句話的克里普托，皺起了眉頭。
被譽為王國最頂尖的頭腦的普拉娜絲・色卡摩，將自國稱為“完蛋了”。
說甚麼完蛋了呢，明明就在日前，克里普托他們才剛和王國放過來的、令他們無計可施的怪物交戰過。
如果那種東西存在複數的話，完蛋的應該是帝國和歐利涅斯王國。

『“污染”這個詞語您有聽聞過嗎？』
「我聽白詰說明過，如果將奧利哈魯鋼放進體內的話，精神就會出現異常。這應該就是污染吧？」
『是的，這個污染早已將王國的高層部分幾乎全都吞噬掉了。國防大臣、大主教、甚至是國王，宛如發瘋了一樣每天都奧利哈魯鋼奧利哈魯鋼的歡愉著』
「也就是說……王國早就已經，被來路不明的物質支配了嗎？」
『應該稱之為支配嗎。奧利哈魯鋼自身，所渴望的唯獨是擴張自己的存在。因為那總其量，只是以增幅魔力為目的來行動著的物質。的確，是產生了會向他人推薦奧利哈魯鋼和會變得粗暴的這些人格上的問題，但其目的並沒有改變』
「所以要帝國和歐利涅斯王國驅逐他們嗎……」
『對，被污染了的他們，即使理解這很危險也會在使用奧利哈魯鋼的事上毫無躊躇的吧』

現在，已經有3架阿尼瑪自王都啟程了。
在那之後，只要準備好了，戰力便會絡繹不絕地被派出去的吧。

『但在這種處境下，希望可以流亡的6人是沒有受到污染的。重點就是，他們只是純粹在尋求避難之處。這樣的他們沒有必要做間諜』
「原來如此，的確，只要保障戰後的安全的話，或許他們就能成為會為帝國而戰的士兵。然而，這是結果，而非給出的代價。既然這是“請求”，那我希望您能相應地付出一些東西」

克里普托並沒有天真到，會被普拉娜絲的話語哄騙。
她在奧拉克石的另一邊，用誰都聽不見的聲量小小地「嘖」了一聲，然後死心並傳達她事先準備好的“代價”。

『光憑語言就能支付的代價，程度是有限的。如果這樣也可以的話』
「倒不如說我想要的就是這個，把情報交過來吧。根據情報的質素，我也可以向皇帝直接提議接受流亡的」
『質素是有保證的，因此在說之前我希望您能明言會接受流亡』
「要信任敵國的人的說話嗎？」
『說實話，我也是被逼到走投無路了哦。其實我甚至想和艾薇一起跟著那6人，流亡到帝國去的』
「艾薇・斐黛拉——騎士團長，“王之城牆”嗎。哈，那當然是不可能接受的吧」
『即使不說我也是知道的，但儘管如此……嘛，與其就這樣留在王都遭受污染，還不如被處刑、像個人樣地死去呢』

普拉娜絲用低沉的聲音說道。
雖然為了表現得誇張一點而或許多少混入了一些演技，不過八成都是真心話。
與其變成人類以外的、自己以外的某者，還不如死掉算了，普拉娜絲是真心如此認為的。
克里普托，也被這句話透露出的“真心”，多少打動到了。

「哈啊……說還不如被處刑嗎。看來說被逼到走投無路是真的呢。沒辦法，既然被逼到這種地步了，那我就明言吧，會接受流亡者的。承諾是不會違反的。這樣就可以了嗎？」
『身為四將卻很大大咧咧呢，是老好人真是幫大忙了』
「至少道個謝吧」
『啊哈哈，因為我的個性就是坦率不了』

普拉娜絲的腦海中浮現出艾薇的身影。
如果能夠變得坦率的話，現在早就已經——她一邊想著這種無用的事情，一邊開始說起情報來。

『那麼，接下來我會傳達我所獲得的情報。今天早上，3架纏著奧利哈魯鋼製的部件的阿尼瑪從王都出發了』
「去的地方是帝都嗎？」
『不是。透過各種手段偷來了機密情報，結果弄清楚3架阿尼瑪會沿著奇妙的路線前進的這件事了』
「不要故弄玄虛了，快點說出來」

事到如今卻讓人著急的普拉娜絲，使克里普托露出多少焦躁的模樣。
就算是普拉娜絲，也是會害怕惹四將生氣的。
她立即將情報的核心，簡短地傳達出來。

『他們會在王國各地轉一圈』
「為了甚麼？」
『奧利哈魯鋼，有增幅魔力的這一個作用。這個作用在人類攝取了它的情況下也是同樣的，體內吸收了它的人類不久後便會成為阿尼瑪使』
「這跟那3架阿尼瑪有甚麼關連」
『看來，他們似乎會在王國的所有城鎮上，到處分發奧利哈魯鋼的粉末。因為能飛在空中所以效率非常之高，似乎僅僅3天左右任務就會結束的了』
「難不成……王國的目的是」

說到這裡，就連克里普托也察覺到了。
是為了甚麼目的，才在王國的所有城鎮上分發奧利哈魯鋼。
分發的結果，又會是甚麼。

『全體王國國民的阿尼瑪使化』

聽後，克里普托僵住了。
普拉娜絲回想起當她第一次從艾薇那裡聽到作戰內容的情報時，她也是做出了完全相同的反應。

「怎可能，這樣的事……！」
『是不存在的，我也希望是這樣。可是，王國是真心想要實現這件事的。按國王的說話來說的話，這就是依靠奧利哈魯鋼達成的“魂的進化”』
「別開玩笑了！帝國的阿尼瑪使，就算把流浪的也加在一起，也不會達到4位數啊！？儘管如此，所有國民的阿尼瑪使化甚麼的——」

大聲喊著的克里普托。
與他相對，普拉娜絲平靜地回答道。

『嗯，在不夠一個月之內，就會有以數萬為單位的阿尼瑪蜂擁去帝國了吧』

克里普托，想認為這一切全都是夢。
本以為會以帝國的大捷作結的戰爭，卻因奧利哈魯鋼的登場而使得基礎受到動搖，並且因數量的暴力而翻盤了。
3架的阿尼瑪的話尚可一戰。
但是，幾萬的阿尼瑪甚麼的——光是考慮與之戰鬥就已經非常愚蠢了。

「太不像話了……這種，荒唐的事……」

面對擺在眼前的現實，他只能如此不斷重複著。


==============


註釋：
[1] 「福隆」，フルーン，荷蘭語Groen，意為綠色的。
[2] 「連貓的手也想借」，原文為諺語「貓の手も借りたい」，意為人手不足、十分忙碌到連貓的手也想借來幫忙。
[3] 普拉娜絲的姓氏「色卡摩」，シカモア，英語Sycamore，源自希臘語Σῡκόμορος，有樹葉像桑樹樹葉的無花果樹之意，也專指學名為Ficus sycomorus的西克莫無花果。這個西克莫無花果也在《聖經》中被提到，其被譯作無花果樹或桑樹。
