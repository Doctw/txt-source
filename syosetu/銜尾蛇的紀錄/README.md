# novel

- title: ウロボロス・レコード　～円環のオーブニル～
- title_zh: 銜尾蛇的紀錄
- author: 山下湊
- illust: しの とうこ
- source: http://ncode.syosetu.com/n9016cm/
- cover: https://images-na.ssl-images-amazon.com/images/I/819N0MKDO9L.jpg
- publisher: syosetu
- date: 2016-08-12T21:20:00+08:00
- status: 連載
- novel_status: 0x0100

## illusts


## publishers

- syosetu

## series

- name: ウロボロス・レコード　～円環のオーブニル～

## preface


```
伯爵家次男圖利烏斯‧修爾南‧歐布尼爾（トゥリウス・シュルーナン・オーブニル），從現代日本轉世到異世界的轉生者。
即是，乃一度歷死之身。他不知自己轉世的理由，下次會否轉生同樣亦未知。
然，死比一切都可怕──基於此，只有變為不死。
作為這個世界特有之力的魔法，其中一種的煉金術。通過窮究此，達至不老不死。
他在那其中找到了救贖，無論如何都想要觸及煉金術的至為秘奧而掙扎求生。
即便是為此，要犧牲一切。
從幼時起戀慕它的奴隸女僕。伯爵家的家人。
冒険者。奴隷。人民。貴族。國家。人類。非人類的存在......僅僅一個男人的妄念，改變著，扭曲著，崩壞著大多數人的人生。
即使如此，圖利烏斯（トゥリウス）的步伐仍未止。一切，都是為了抵達他渴求的永遠。

「これは永遠に恋をする物語」
　伯爵家次男トゥリウス・シュルーナン・オーブニルは、現代日本から異世界に生まれ変わった転生者である。即ち、一度は死を経験した身であるということだ。自分が生まれ変わった理由は分からない、次も生まれ変われるかもまた同様。そして、何より死ぬのが怖い――である以上、死なないようになるしかない。この世界特有の力である魔法、その一種である錬金術。これを極めることで、不老不死に至れるという。彼はそのことに救いを見出し、何としても錬金術の最秘奥に手を伸ばそうと生き足掻く。たとえその為に、何を犠牲にしようとも。
　幼い頃より彼を慕う奴隷のメイド。伯爵家の家族。冒険者。奴隷。民。貴族。国。人間。人間ではない存在……たった一人の男の妄念が、数多の人生を変え、歪め、壊していく。それでもトゥリウスの歩みは止まらない。全ては、彼が焦がれる永遠へと辿り着く為に。
■ヒーロー文庫様より『ウロボロス・レコード』のタイトルで書籍化いたしました。削除、ダイジェスト化の予定はございません。
```

## tags

- node-novel
- R15
- syosetu
- チート
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ファンタジー
- 性的表現あり
- 悪役
- 成り上がり
- 改造
- 残酷な描写あり
- 洗脳
- 異世界転生

# contribute

- cscomic
- 

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n9016cm

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n9016cm&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n9016cm/)
- [https://masiro.moe/forum.php?mod=viewthread\&tid=12859\&extra=page%3D1](https://masiro.moe/forum.php?mod=viewthread&tid=12859&extra=page%3D1)
- 

