「......和《長》？不，這實在是......」
《哥布林》似乎有些反對，但老人卻搖了搖頭
「汝的心情也能夠理解，是不是在想這樣太不合規矩，或著是叛變組織的行為？但若像這樣坐以待斃，我等也只有死路一條啊。最壞的情況下，老朽也許可以一個人逃走，但你和《塞壬》該如何是好呢？勸汝還是三思而後行......」
他這話並不是發自內心吧

若《哥布林》最後還是不願配合，我們沒辦法見到《長》的話，事情會如何發展呢......嘛，就算不說也能大致想像得到

老人就是擔心事情發展到那種地步吧
雖然也不是不能放他們離開
但綜合考慮，果然還是不能那樣做

《哥布林》對老人說到
「有什麼好辦法麼？我也不想死啊，雖然很感謝組織......但還不至於甘願犧牲自己。說到底，我差不多也在打算隱退了」
「果然如此麼？老朽本也打算，完成這次工作後就建議你隱退的」
「老爺子......你注意到了嗎？」
「你是個溫柔的男人，與老朽不同，並不適合這種工作啊。而且幸運的是，掩飾身份的商人工作，汝也做的不錯呢」
「啊......如果能這樣繼續做行商的話，至少能養活自己啊......因為這次的事情，又開始幻想起未來了呢」
「話不能這麼說。倒不如說，就把這份工作當成最後一次，好好努力吧。那樣想來，心情上也會好受一些」
「話雖如此，但......唉，嘛，沒辦法吶，但是......你們是認真的麼？說想與《長》見面」
《哥布林》把話題轉向了我們
總而言之，他算是被老人成功說服了吧


羅蕾露回答了《哥布林》的問題
「當然是認真的。被你們盯上的原因我已經知道了，但老實說，不想再摻和進去了啊。如果像那個老人一樣的傢伙，之後也會被不斷派過來......真是饒了我吧。所以想和上層交涉一番，消除這種隱患，這些話是發自心內的」
聽到她的話，《哥布林》似乎也能夠相信，他露出一副理解的表情
「......啊，這個老頭子，就算在組織裡也是個相當的武鬥派啊......我聽幾個組織的朋友說過些關於他的傳聞，像是曾經把哪裡鬧得天翻地覆之類的......竟然能贏過那個人？不，既然爺爺自己說了，我也不會懷疑。但能勝過他，你們也是了不得的怪物吶」
「這也是多虧了運氣好啊，完全不想再來一次了，是吧？」
羅蕾露對我和奧格利這樣說到
我們則是
「那是當然的吧」
「絕對不想......」
毫不猶豫的回答
但老人用鼻子哼了一聲
「老朽也是一樣。《哥布林》，雖然這些人嘴上這麼說，但實際上比這邊還像怪物吶。老實說，不是你我能夠應付的對手。建議還是把他們引薦給《長》，就算是為了組織，也最好不要再和他們扯上關係。這些事情，就由汝來向《長》傳達」
「......雖然早就有心理準備了，但我去真的合適麼」
「沒錯，一起去不是會有很大問題麼？」
「問題......是麼？可是......」
「老朽所說的問題是指，有可能會被一網打盡，然後處死」
「啊，這樣麼。確實如此......但，我去的話，不是也有可能被抓起來殺掉麼......」
「任務失敗的事情還沒有傳出去，總之，不必多慮。汝就想辦法把這種事情矇混過去，把會面的事情向《長》傳達即可」
「這種事情麼，您還是一樣粗枝大葉的啊......不光是身體」
「異能是能夠影響一個人性格的。總而言之，就拜託汝了」
「好吧，我知道了。事已至此，不上船也不行了吶，就讓我們盡力而為吧」


聽到這番對話，呣——！呣——！，《塞壬》突然又鬧騰了起來
剛剛聽到老人敗北的消息後大受打擊，現在似乎又復活了過來
差點忘記她還在了，大家都向她看去

我說到
「......可以給她解開了吧？話都已經談妥了」
老人點了點頭
「也是啊......可這傢伙絕對會吵起來的，沒關係麼」
提出了忠告
但羅蕾露開口說道
「......是嗎？不過也沒什麼問題，就讓她放鬆一下好了」
說完，念起了什麼咒語
然後，綁住《塞壬》的魔術繩索被解開了
塞在她嘴裡的東西是普通物品，手腳被解放的《塞壬》一下就把它扯出來丟掉了

然後
「......等等啊！你們都擅自決定了什麼！我才不會承認呢！」
這樣喊道
「......看吧！果然吵起來了......」
「這傢伙，剛才有沒有在聽啊......」
老人和《哥布林》不約而同地捂住耳朵，這樣說到

這些傢伙真的是朋友麼，不，正因為是親密的朋友，才會有這種反應吧
塞壬從床上下來
「......我當然有聽，但你們真的認為那種事有可能做到嗎？組織的恐怖，難道都忘沒啦！？」
她這樣對老人和《哥布林》大吼了一通，那二人都露出一副受不了的表情
本以為她只是會歇斯底里大鬧一番，沒想到說出的內容還是很有條理的
只是完全不會讀氣氛
雖然我覺得事情已經談妥了......但她完全沒有要鬆口的意思呢


緊接著，《塞壬》又轉向了我們
「你們也是啊！那種交涉，怎麼可能辦得到......！！」
但話說到一半，羅蕾露突然走到她面前，《塞壬》看到羅蕾露，突然哆嗦了一下
這奇怪的反應令我們歪起了頭。羅蕾露又向《塞壬》伸出了法杖
「......想再來一次的話，我也可以滿足你的願望哦？」
說出這種不明所以的話來
到底說什麼呢......我正百思不得其解，《塞壬》卻有了反應
「......噫！唉，不勞您了！已經夠了！那種東西！我知道了，我知道了啦！」
語無倫次地說著，最後甚至抽抽嗒嗒哭了起來
看上去實在是太可憐了，奧格利走過去把手帕遞給了她

除了羅蕾露和《塞壬》以外的人，都把視線轉向了羅蕾露
——這個人到底做過些什麼？
雖然這樣的疑問浮現在腦海裏，但實在太可怕了，沒有一個人敢把話問出口