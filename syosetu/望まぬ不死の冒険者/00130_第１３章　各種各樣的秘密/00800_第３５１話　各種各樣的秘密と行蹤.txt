出現新迷宮為何會變得繁忙，最直接的原因，就是冒險者會增多

迷宮內部的資源雖然會持續再生，但也並非無限
如果進入了過多冒險者，可能會有很多人一無所獲
嘛，魔物雖然不至於那樣，但寶物就不一定了
本來就沒那麼容易找到，冒險者數量增加的話，發現率也會隨之下降，這是理所當然的事情

所謂冒險者，就是通過賣掉打倒魔物獲得的魔石、素材來換取錢財的人，在這樣的生活中，發現昂貴的魔道具之類，是大家都夢寐以求的事情，只要發現一次，就能一攉千金
雖然能夠獲得玩樂一生錢財的機會非常少見，但作為艱苦生活中的一種夢想，這毫無疑問帶給了冒險者們堅持下去的動力

但若是大量冒險者蜂擁而至，實現這個夢想的機會也會隨之下降
本來機率就已經很低了，但即便如此，還是會有人抱著「說不定」的想法，想要發現寶物，從新冒險者們之中脫穎而出，那麼人數增多這種事情自然會令人不快

為了避免發生爭奪的情況，冒險者公會通常會緩慢進行調整，但隨著這次新迷宮出現，馬路特所能容納的冒險者數量一口氣提升了
雖然還沒有完全探索、評估，但根據情況，可能會增多幾百個冒險者

所以烏路夫才說，會忙起來
嘛，有新迷宮出現，裡面的寶物和魔物都還未經探索，很多人都會想要搶佔先機吧。所以在一段時間內，人們恐怕會蜂擁而至，然後迅速減少，最後漸漸穩定下來。但隨著這樣的進程，公會可能會有半年到一年的時間忙的不可開交


「......除此之外，研究者們也會來吧。雖說是邊境，但新出現的迷宮還是很少見的。我也有很多想要調查的事情......」
說出這話的是羅蕾露
魔物和迷宮相關的研究，是她的專長，所以這也是可以理解的

而且這次的事件與她也不無關係，不如說是與她有很深的聯繫
甚至想現在就把勞拉敲起來，問個幾天幾夜
不過那種事情果然還是不行
聽到羅蕾露的話，烏路夫點了點頭
「會來的吧，王都著名的「學院」和「塔」，那裡的教授、魔術師之類的傢伙。那些傢伙都與國家上層有關，應該會由騎士團之類來擔任護衛。冒險者公會沒有什麼插手的餘地，不過相應的，工作量也不會上升......但還是要好準備」


「學院」是國立的高等教育機構，是國內精英輩出的學校
基本上是為了傳授魔術而設立的組織，有時也被稱為魔術學院，正式名稱是「學院」
貴族和豪商將常會將孩子送進去學習，平民也可以加入
雖然對年齡並沒有特別加以限制，但一般加入的都是十幾歲的年輕人


「塔」則是魔術師們聚集在一起，形成組織的統稱
在不同國家，有各種各樣的組織被稱之為「塔」，但在亞蘭，很少有那種結黨營私的激進派的魔術師，所以說到王都的「塔」，就只有一個

雖然可以細分位魔物研究所、魔術研究所、迷宮研究所等，但都在統一的組織管理之下，那個組織，我們一般稱之為「塔」
順便一提，在其他國家，特別是帝國，說到「塔」，就泛指各種不同的組織......嘛，先不提那些①


「提起「學院」和「塔」，感覺全都是一群怪胎啊。真是不想和他們扯上關係」
我這麼一說，烏路夫也皺起眉頭表示同意
「能那樣就好了啊，嘛，萬一出了什麼事，就請多多關照了」
說得還真嚇人
「學院」也好，「塔」也好，大致上來說，都是權力機關，不太想和他們發生糾纏

嘛，沒辦法的時候也是有的啊
好在亞蘭的貴族一般不太蠻橫，只要好好表達自己的意思，應該不會幹出什麼荒唐事吧，所以不必太過擔心......我是這樣想的
「出了事希望你不要找上我啊」
「那可不行......你也是職員，必須要幹活。那麼，馬路特的現狀差不多就是這些了，還有什麼其它事想問的麼？」
我稍微思索了一番
不過感覺沒什麼要問的
羅蕾露似乎也沒有問題，但莉娜問到
「在「新月迷宮」中被救出的冒險者們，現在......？」


對哦，還有這回事

萊伊茲和蘿拉
和我一起參加銅級測試的那兩人，現在正和莉娜組隊
我沒醒來的話，莉娜無法確認情況，所以不能隨便到街上去

他們二人的情況也就無從得知
不過之前他們被我救下來時候，已經可以自力走動了

那時候拜託了老手冒險者帶他們回城，所以現在應該是在市內的什麼地方吧
關於這些事情，公會長似乎都牢記在心
「啊，那些傢伙啊，小姑娘是叫莉娜吧，是在擔心隊員的萊伊茲與蘿拉麼？」
提出了準確的問題
連新人冒險者組成的小隊都記得，真是了不起

雖然說起來有些殘酷，但對不知道什麼時候會死的人，一般公會長是不會下心思去一一記住的，至少在其它大多數城市，都是如此
烏路夫果然是個值得尊敬的公會長啊
莉娜點了點頭，烏路夫露出正在思考的表情
「從新月的迷宮裡救出來的那些傢伙，都分散在與公會有合作的治療院了......等一下，我查查資料......有了，他們兩個都在柯姆治療院（コーム治療院），地點是這裡」
他展開馬路特地圖，親切的為莉娜指示地點


------------


Alice注：


①作者在這一節語無倫次，所以我大幅修改了
總結一下，「塔」應該並非統一的跨國組織，而是民間對魔術師們組成社團的統稱（非正式名稱）
亞蘭的「塔」，應該是一個大研究機構下轄許多研究院的結構，聯繫上文應該是國立的
「沒有那麼多過激的魔術師」，所以沒有私立魔術結社
而帝國大概就是國立、私立研究機構或其它社團很多的樣子


下面是原文對照翻譯，各位可以參考一下


《塔》は、魔術師たちが集う研究機関だな
「塔」是魔術師們聚集在一起的研究機構


國によって呼び方が違ったり、亂立していたりするが、この國ヤーランの魔術師たちはそこまで過激なのは少ないから、王都の《塔》と言ったら一つしかない
在不同國家，稱呼方式也不盡相同，雜亂建立著（許多機構）。但在這個國家亞蘭的魔術師，很少有那麼過激的人，所以提到王都的「塔」，就只有一個


細かく區切ると魔物の研究所やら、魔術の研究所や、迷宮の研究所やらと分かれてはいるが、組織としては一まとまりで《塔》と呼ぶことが多い。
詳細區分開來的話，有魔物研究所、魔術研究所、迷宮研究所等，但作為組織，都統一稱之為「塔」


ちなみに他の國、特に帝國なんかは一口に《塔》と言っても色々とあるみたいだが……まぁ、それはいいだろう。
順便一提，在其他國家，特別是帝國，提到「塔」，有各種各樣的（組織）......嘛，先不提那些