# CONTENTS

其實，我乃最強  
実は俺、最強でした？ ～転生直後はどん底スタート、でも万能魔法で逆転人生を上昇中！～  

作者： すみもりさい  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/user2/out/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/user2/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/user2/其實，我乃最強/導航目錄.md "導航目錄")




## [第一章：出生後的奮鬥記](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%87%BA%E7%94%9F%E5%BE%8C%E7%9A%84%E5%A5%AE%E9%AC%A5%E8%A8%98)

- [01-01　那個，測定器不行嗎？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%87%BA%E7%94%9F%E5%BE%8C%E7%9A%84%E5%A5%AE%E9%AC%A5%E8%A8%98/01-01%E3%80%80%E9%82%A3%E5%80%8B%EF%BC%8C%E6%B8%AC%E5%AE%9A%E5%99%A8%E4%B8%8D%E8%A1%8C%E5%97%8E%EF%BC%9F.txt)
- [01-02　實現了夢想](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%87%BA%E7%94%9F%E5%BE%8C%E7%9A%84%E5%A5%AE%E9%AC%A5%E8%A8%98/01-02%E3%80%80%E5%AF%A6%E7%8F%BE%E4%BA%86%E5%A4%A2%E6%83%B3.txt)
- [01-03　王宮裡的種種](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%87%BA%E7%94%9F%E5%BE%8C%E7%9A%84%E5%A5%AE%E9%AC%A5%E8%A8%98/01-03%E3%80%80%E7%8E%8B%E5%AE%AE%E8%A3%A1%E7%9A%84%E7%A8%AE%E7%A8%AE.txt)
- [01-04　在森林中遇到了狗](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%87%BA%E7%94%9F%E5%BE%8C%E7%9A%84%E5%A5%AE%E9%AC%A5%E8%A8%98/01-04%E3%80%80%E5%9C%A8%E6%A3%AE%E6%9E%97%E4%B8%AD%E9%81%87%E5%88%B0%E4%BA%86%E7%8B%97.txt)
- [01-05　嬰兒生存所必需的東西](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%87%BA%E7%94%9F%E5%BE%8C%E7%9A%84%E5%A5%AE%E9%AC%A5%E8%A8%98/01-05%E3%80%80%E5%AC%B0%E5%85%92%E7%94%9F%E5%AD%98%E6%89%80%E5%BF%85%E9%9C%80%E7%9A%84%E6%9D%B1%E8%A5%BF.txt)
- [01-06　天然犬娘](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%87%BA%E7%94%9F%E5%BE%8C%E7%9A%84%E5%A5%AE%E9%AC%A5%E8%A8%98/01-06%E3%80%80%E5%A4%A9%E7%84%B6%E7%8A%AC%E5%A8%98.txt)
- [01-07　嬰兒，脫離危機](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%87%BA%E7%94%9F%E5%BE%8C%E7%9A%84%E5%A5%AE%E9%AC%A5%E8%A8%98/01-07%E3%80%80%E5%AC%B0%E5%85%92%EF%BC%8C%E8%84%AB%E9%9B%A2%E5%8D%B1%E6%A9%9F.txt)


## [第二章：兒童時代的故事](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B)

- [02-01　製作分身](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-01%E3%80%80%E8%A3%BD%E4%BD%9C%E5%88%86%E8%BA%AB.txt)
- [02-02　續，王宮裡的種種](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-02%E3%80%80%E7%BA%8C%EF%BC%8C%E7%8E%8B%E5%AE%AE%E8%A3%A1%E7%9A%84%E7%A8%AE%E7%A8%AE.txt)
- [02-03　關係好的兩個人](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-03%E3%80%80%E9%97%9C%E4%BF%82%E5%A5%BD%E7%9A%84%E5%85%A9%E5%80%8B%E4%BA%BA.txt)
- [02-04　天使的姐姐和垃圾的弟弟](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-04%E3%80%80%E5%A4%A9%E4%BD%BF%E7%9A%84%E5%A7%90%E5%A7%90%E5%92%8C%E5%9E%83%E5%9C%BE%E7%9A%84%E5%BC%9F%E5%BC%9F.txt)
- [02-05　公主，戰慄](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-05%E3%80%80%E5%85%AC%E4%B8%BB%EF%BC%8C%E6%88%B0%E6%85%84.txt)
- [02-06　女僕正在看](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-06%E3%80%80%E5%A5%B3%E5%83%95%E6%AD%A3%E5%9C%A8%E7%9C%8B.txt)
- [02-07　是啊，如果是哥哥的話](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-07%E3%80%80%E6%98%AF%E5%95%8A%EF%BC%8C%E5%A6%82%E6%9E%9C%E6%98%AF%E5%93%A5%E5%93%A5%E7%9A%84%E8%A9%B1.txt)
- [02-08　骷髏軍團、來襲](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-08%E3%80%80%E9%AA%B7%E9%AB%8F%E8%BB%8D%E5%9C%98%E3%80%81%E4%BE%86%E8%A5%B2.txt)
- [02-09　召喚士的受難](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-09%E3%80%80%E5%8F%AC%E5%96%9A%E5%A3%AB%E7%9A%84%E5%8F%97%E9%9B%A3.txt)
- [02-10　初次出行](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-10%E3%80%80%E5%88%9D%E6%AC%A1%E5%87%BA%E8%A1%8C.txt)
- [02-11　單方面的母子重逢](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-11%E3%80%80%E5%96%AE%E6%96%B9%E9%9D%A2%E7%9A%84%E6%AF%8D%E5%AD%90%E9%87%8D%E9%80%A2.txt)
- [02-12　警告、項圈](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0%EF%BC%9A%E5%85%92%E7%AB%A5%E6%99%82%E4%BB%A3%E7%9A%84%E6%95%85%E4%BA%8B/02-12%E3%80%80%E8%AD%A6%E5%91%8A%E3%80%81%E9%A0%85%E5%9C%88.txt)


## [第三章：家裡蹲絕對不停止的入學篇](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87)

- [03-01　這世上最討厭的地方](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-01%E3%80%80%E9%80%99%E4%B8%96%E4%B8%8A%E6%9C%80%E8%A8%8E%E5%8E%AD%E7%9A%84%E5%9C%B0%E6%96%B9.txt)
- [03-02　黑暗中蠢動的神秘組織](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-02%E3%80%80%E9%BB%91%E6%9A%97%E4%B8%AD%E8%A0%A2%E5%8B%95%E7%9A%84%E7%A5%9E%E7%A7%98%E7%B5%84%E7%B9%94.txt)
- [03-03　龍老師很安靜](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-03%E3%80%80%E9%BE%8D%E8%80%81%E5%B8%AB%E5%BE%88%E5%AE%89%E9%9D%9C.txt)
- [03-04　圓桌會議](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-04%E3%80%80%E5%9C%93%E6%A1%8C%E6%9C%83%E8%AD%B0.txt)
- [03-05　一個人的入學考試](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-05%E3%80%80%E4%B8%80%E5%80%8B%E4%BA%BA%E7%9A%84%E5%85%A5%E5%AD%B8%E8%80%83%E8%A9%A6.txt)
- [03-06　入學第一天常有的那個](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-06%E3%80%80%E5%85%A5%E5%AD%B8%E7%AC%AC%E4%B8%80%E5%A4%A9%E5%B8%B8%E6%9C%89%E7%9A%84%E9%82%A3%E5%80%8B.txt)
- [03-07　偽物的矜持](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-07%E3%80%80%E5%81%BD%E7%89%A9%E7%9A%84%E7%9F%9C%E6%8C%81.txt)
- [03-08　如果有時候的應對法（申請決鬥的話？）](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-08%E3%80%80%E5%A6%82%E6%9E%9C%E6%9C%89%E6%99%82%E5%80%99%E7%9A%84%E6%87%89%E5%B0%8D%E6%B3%95%EF%BC%88%E7%94%B3%E8%AB%8B%E6%B1%BA%E9%AC%A5%E7%9A%84%E8%A9%B1%EF%BC%9F%EF%BC%89.txt)
- [03-09　貴公子，濕了](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-09%E3%80%80%E8%B2%B4%E5%85%AC%E5%AD%90%EF%BC%8C%E6%BF%95%E4%BA%86.txt)
- [03-10　我的評價直線上升（無法理解）](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-10%E3%80%80%E6%88%91%E7%9A%84%E8%A9%95%E5%83%B9%E7%9B%B4%E7%B7%9A%E4%B8%8A%E5%8D%87%EF%BC%88%E7%84%A1%E6%B3%95%E7%90%86%E8%A7%A3%EF%BC%89.txt)
- [03-11　我，爭奪戰](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-11%E3%80%80%E6%88%91%EF%BC%8C%E7%88%AD%E5%A5%AA%E6%88%B0.txt)
- [03-12　我的名字是](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-12%E3%80%80%E6%88%91%E7%9A%84%E5%90%8D%E5%AD%97%E6%98%AF.txt)
- [03-13　妹妹，襲來。各種決定。](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-13%E3%80%80%E5%A6%B9%E5%A6%B9%EF%BC%8C%E8%A5%B2%E4%BE%86%E3%80%82%E5%90%84%E7%A8%AE%E6%B1%BA%E5%AE%9A%E3%80%82.txt)
- [03-14　科學家的魔法講座](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-14%E3%80%80%E7%A7%91%E5%AD%B8%E5%AE%B6%E7%9A%84%E9%AD%94%E6%B3%95%E8%AC%9B%E5%BA%A7.txt)
- [03-15　因為被召喚了](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-15%E3%80%80%E5%9B%A0%E7%82%BA%E8%A2%AB%E5%8F%AC%E5%96%9A%E4%BA%86.txt)
- [03-16　你要是嫌棄得過頭了就要懲罰你](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-16%E3%80%80%E4%BD%A0%E8%A6%81%E6%98%AF%E5%AB%8C%E6%A3%84%E5%BE%97%E9%81%8E%E9%A0%AD%E4%BA%86%E5%B0%B1%E8%A6%81%E6%87%B2%E7%BD%B0%E4%BD%A0.txt)
- [03-17　還不錯](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-17%E3%80%80%E9%82%84%E4%B8%8D%E9%8C%AF.txt)
- [03-18　精銳部隊](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-18%E3%80%80%E7%B2%BE%E9%8A%B3%E9%83%A8%E9%9A%8A.txt)
- [03-19　沒有懲罰的男人的末路](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%81%9C%E6%AD%A2%E7%9A%84%E5%85%A5%E5%AD%B8%E7%AF%87/03-19%E3%80%80%E6%B2%92%E6%9C%89%E6%87%B2%E7%BD%B0%E7%9A%84%E7%94%B7%E4%BA%BA%E7%9A%84%E6%9C%AB%E8%B7%AF.txt)


## [第四章：王都、騷亂](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82)

- [04-01　叫做妹妹的生物](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-01%E3%80%80%E5%8F%AB%E5%81%9A%E5%A6%B9%E5%A6%B9%E7%9A%84%E7%94%9F%E7%89%A9.txt)
- [04-02　一開始就搞砸了，但我不怪妹妹](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-02%E3%80%80%E4%B8%80%E9%96%8B%E5%A7%8B%E5%B0%B1%E6%90%9E%E7%A0%B8%E4%BA%86%EF%BC%8C%E4%BD%86%E6%88%91%E4%B8%8D%E6%80%AA%E5%A6%B9%E5%A6%B9.txt)
- [04-03　只有你，重複一遍](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-03%E3%80%80%E5%8F%AA%E6%9C%89%E4%BD%A0%EF%BC%8C%E9%87%8D%E8%A4%87%E4%B8%80%E9%81%8D.txt)
- [04-04　天才與天才的遙遠邂逅](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-04%E3%80%80%E5%A4%A9%E6%89%8D%E8%88%87%E5%A4%A9%E6%89%8D%E7%9A%84%E9%81%99%E9%81%A0%E9%82%82%E9%80%85.txt)
- [04-05　新成員（暫定）](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-05%E3%80%80%E6%96%B0%E6%88%90%E5%93%A1%EF%BC%88%E6%9A%AB%E5%AE%9A%EF%BC%89.txt)
- [04-06　充滿笑容的快樂課](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-06%E3%80%80%E5%85%85%E6%BB%BF%E7%AC%91%E5%AE%B9%E7%9A%84%E5%BF%AB%E6%A8%82%E8%AA%B2.txt)
- [04-07　大家對我說「要贏」](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-07%E3%80%80%E5%A4%A7%E5%AE%B6%E5%B0%8D%E6%88%91%E8%AA%AA%E3%80%8C%E8%A6%81%E8%B4%8F%E3%80%8D.txt)
- [04-08　糟糕了](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-08%E3%80%80%E7%B3%9F%E7%B3%95%E4%BA%86.txt)
- [04-09　黑色戰士的忠告](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-09%E3%80%80%E9%BB%91%E8%89%B2%E6%88%B0%E5%A3%AB%E7%9A%84%E5%BF%A0%E5%91%8A.txt)
- [04-10　偷偷靠近犯人的我](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-10%E3%80%80%E5%81%B7%E5%81%B7%E9%9D%A0%E8%BF%91%E7%8A%AF%E4%BA%BA%E7%9A%84%E6%88%91.txt)
- [04-11　現在進行中的黑歷史](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-11%E3%80%80%E7%8F%BE%E5%9C%A8%E9%80%B2%E8%A1%8C%E4%B8%AD%E7%9A%84%E9%BB%91%E6%AD%B7%E5%8F%B2.txt)
- [04-12　暗中活動的狙擊者](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-12%E3%80%80%E6%9A%97%E4%B8%AD%E6%B4%BB%E5%8B%95%E7%9A%84%E7%8B%99%E6%93%8A%E8%80%85.txt)
- [04-13　魔神與魔人](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-13%E3%80%80%E9%AD%94%E7%A5%9E%E8%88%87%E9%AD%94%E4%BA%BA.txt)
- [04-14　逼近陰謀的卡美洛](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-14%E3%80%80%E9%80%BC%E8%BF%91%E9%99%B0%E8%AC%80%E7%9A%84%E5%8D%A1%E7%BE%8E%E6%B4%9B.txt)
- [04-15　圓桌騎士，出擊！](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-15%E3%80%80%E5%9C%93%E6%A1%8C%E9%A8%8E%E5%A3%AB%EF%BC%8C%E5%87%BA%E6%93%8A%EF%BC%81.txt)
- [04-16　革命開始](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-16%E3%80%80%E9%9D%A9%E5%91%BD%E9%96%8B%E5%A7%8B.txt)
- [04-17　在逃出去的前方等待的是](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-17%E3%80%80%E5%9C%A8%E9%80%83%E5%87%BA%E5%8E%BB%E7%9A%84%E5%89%8D%E6%96%B9%E7%AD%89%E5%BE%85%E7%9A%84%E6%98%AF.txt)
- [04-18　單方面的舊識重逢](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-18%E3%80%80%E5%96%AE%E6%96%B9%E9%9D%A2%E7%9A%84%E8%88%8A%E8%AD%98%E9%87%8D%E9%80%A2.txt)
- [04-19　抓到魔人了](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-19%E3%80%80%E6%8A%93%E5%88%B0%E9%AD%94%E4%BA%BA%E4%BA%86.txt)
- [04-20　只是為了平靜的家裡蹲生活](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0%EF%BC%9A%E7%8E%8B%E9%83%BD%E3%80%81%E9%A8%B7%E4%BA%82/04-20%E3%80%80%E5%8F%AA%E6%98%AF%E7%82%BA%E4%BA%86%E5%B9%B3%E9%9D%9C%E7%9A%84%E5%AE%B6%E8%A3%A1%E8%B9%B2%E7%94%9F%E6%B4%BB.txt)


## [第五章：學校天堂計畫](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB)

- [05-01　把業務委託給合適的人才](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-01%E3%80%80%E6%8A%8A%E6%A5%AD%E5%8B%99%E5%A7%94%E8%A8%97%E7%B5%A6%E5%90%88%E9%81%A9%E7%9A%84%E4%BA%BA%E6%89%8D.txt)
- [05-02　如果不能提前退學的話](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-02%E3%80%80%E5%A6%82%E6%9E%9C%E4%B8%8D%E8%83%BD%E6%8F%90%E5%89%8D%E9%80%80%E5%AD%B8%E7%9A%84%E8%A9%B1.txt)
- [05-03　為了偷懶要全力以赴](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-03%E3%80%80%E7%82%BA%E4%BA%86%E5%81%B7%E6%87%B6%E8%A6%81%E5%85%A8%E5%8A%9B%E4%BB%A5%E8%B5%B4.txt)
- [05-04　誰來回答我啊](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-04%E3%80%80%E8%AA%B0%E4%BE%86%E5%9B%9E%E7%AD%94%E6%88%91%E5%95%8A.txt)
- [05-05　每次都這樣就好了](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-05%E3%80%80%E6%AF%8F%E6%AC%A1%E9%83%BD%E9%80%99%E6%A8%A3%E5%B0%B1%E5%A5%BD%E4%BA%86.txt)
- [05-06　好像不是【開】，而是【解】](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-06%E3%80%80%E5%A5%BD%E5%83%8F%E4%B8%8D%E6%98%AF%E3%80%90%E9%96%8B%E3%80%91%EF%BC%8C%E8%80%8C%E6%98%AF%E3%80%90%E8%A7%A3%E3%80%91.txt)
- [05-07　免去上課的道路](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-07%E3%80%80%E5%85%8D%E5%8E%BB%E4%B8%8A%E8%AA%B2%E7%9A%84%E9%81%93%E8%B7%AF.txt)
- [05-08　學院長是什麼樣的人？](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-08%E3%80%80%E5%AD%B8%E9%99%A2%E9%95%B7%E6%98%AF%E4%BB%80%E9%BA%BC%E6%A8%A3%E7%9A%84%E4%BA%BA%EF%BC%9F.txt)
- [05-09　簡直是三方會談](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-09%E3%80%80%E7%B0%A1%E7%9B%B4%E6%98%AF%E4%B8%89%E6%96%B9%E6%9C%83%E8%AB%87.txt)
- [05-10　合法隱居的條件](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-10%E3%80%80%E5%90%88%E6%B3%95%E9%9A%B1%E5%B1%85%E7%9A%84%E6%A2%9D%E4%BB%B6.txt)
- [05-11　決定探索遺跡的隊伍](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-11%E3%80%80%E6%B1%BA%E5%AE%9A%E6%8E%A2%E7%B4%A2%E9%81%BA%E8%B7%A1%E7%9A%84%E9%9A%8A%E4%BC%8D.txt)
- [05-12　一開始就做了嗎？](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-12%E3%80%80%E4%B8%80%E9%96%8B%E5%A7%8B%E5%B0%B1%E5%81%9A%E4%BA%86%E5%97%8E%EF%BC%9F.txt)
- [05-13　現在要重新開始了](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-13%E3%80%80%E7%8F%BE%E5%9C%A8%E8%A6%81%E9%87%8D%E6%96%B0%E9%96%8B%E5%A7%8B%E4%BA%86.txt)
- [05-14　支配遺跡的人](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-14%E3%80%80%E6%94%AF%E9%85%8D%E9%81%BA%E8%B7%A1%E7%9A%84%E4%BA%BA.txt)
- [05-15　保護了走失的孩子](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-15%E3%80%80%E4%BF%9D%E8%AD%B7%E4%BA%86%E8%B5%B0%E5%A4%B1%E7%9A%84%E5%AD%A9%E5%AD%90.txt)
- [05-16　墨爾庫美尼斯的計畫](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-16%E3%80%80%E5%A2%A8%E7%88%BE%E5%BA%AB%E7%BE%8E%E5%B0%BC%E6%96%AF%E7%9A%84%E8%A8%88%E7%95%AB.txt)
- [05-17　沒有麵包的理論](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-17%E3%80%80%E6%B2%92%E6%9C%89%E9%BA%B5%E5%8C%85%E7%9A%84%E7%90%86%E8%AB%96.txt)
- [05-18　這是聖武具（偽）](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-18%E3%80%80%E9%80%99%E6%98%AF%E8%81%96%E6%AD%A6%E5%85%B7%EF%BC%88%E5%81%BD%EF%BC%89.txt)
- [05-19　浪漫武器做好了](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-19%E3%80%80%E6%B5%AA%E6%BC%AB%E6%AD%A6%E5%99%A8%E5%81%9A%E5%A5%BD%E4%BA%86.txt)
- [05-20　新的魔人](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-20%E3%80%80%E6%96%B0%E7%9A%84%E9%AD%94%E4%BA%BA.txt)
- [05-21　看起來還不錯](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-21%E3%80%80%E7%9C%8B%E8%B5%B7%E4%BE%86%E9%82%84%E4%B8%8D%E9%8C%AF.txt)
- [05-22　我會玩弄學院長的](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-22%E3%80%80%E6%88%91%E6%9C%83%E7%8E%A9%E5%BC%84%E5%AD%B8%E9%99%A2%E9%95%B7%E7%9A%84.txt)
- [05-23　攻略了學院長哦](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-23%E3%80%80%E6%94%BB%E7%95%A5%E4%BA%86%E5%AD%B8%E9%99%A2%E9%95%B7%E5%93%A6.txt)
- [05-24　特蕾西亞的猜測](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-24%E3%80%80%E7%89%B9%E8%95%BE%E8%A5%BF%E4%BA%9E%E7%9A%84%E7%8C%9C%E6%B8%AC.txt)
- [05-25　在學校閉門不出生活的開始](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0%EF%BC%9A%E5%AD%B8%E6%A0%A1%E5%A4%A9%E5%A0%82%E8%A8%88%E7%95%AB/05-25%E3%80%80%E5%9C%A8%E5%AD%B8%E6%A0%A1%E9%96%89%E9%96%80%E4%B8%8D%E5%87%BA%E7%94%9F%E6%B4%BB%E7%9A%84%E9%96%8B%E5%A7%8B.txt)


## [第六章：學院隱居生活](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB)

- [06-01　妹妹是同學嗎？](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-01%E3%80%80%E5%A6%B9%E5%A6%B9%E6%98%AF%E5%90%8C%E5%AD%B8%E5%97%8E%EF%BC%9F.txt)
- [06-02　心理萬全準備](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-02%E3%80%80%E5%BF%83%E7%90%86%E8%90%AC%E5%85%A8%E6%BA%96%E5%82%99.txt)
- [06-03　妹妹是傳道者](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-03%E3%80%80%E5%A6%B9%E5%A6%B9%E6%98%AF%E5%82%B3%E9%81%93%E8%80%85.txt)
- [06-04　發生了什麼事？](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-04%E3%80%80%E7%99%BC%E7%94%9F%E4%BA%86%E4%BB%80%E9%BA%BC%E4%BA%8B%EF%BC%9F.txt)
- [06-05　連續不斷的訪客](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-05%E3%80%80%E9%80%A3%E7%BA%8C%E4%B8%8D%E6%96%B7%E7%9A%84%E8%A8%AA%E5%AE%A2.txt)
- [06-06　朋友、夥伴方便的用語](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-06%E3%80%80%E6%9C%8B%E5%8F%8B%E3%80%81%E5%A4%A5%E4%BC%B4%E6%96%B9%E4%BE%BF%E7%9A%84%E7%94%A8%E8%AA%9E.txt)
- [06-07　增加的圓桌成員](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-07%E3%80%80%E5%A2%9E%E5%8A%A0%E7%9A%84%E5%9C%93%E6%A1%8C%E6%88%90%E5%93%A1.txt)
- [06-08　夏爾的密會，對方是？](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-08%E3%80%80%E5%A4%8F%E7%88%BE%E7%9A%84%E5%AF%86%E6%9C%83%EF%BC%8C%E5%B0%8D%E6%96%B9%E6%98%AF%EF%BC%9F.txt)
- [06-09　搞笑集團的目的](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-09%E3%80%80%E6%90%9E%E7%AC%91%E9%9B%86%E5%9C%98%E7%9A%84%E7%9B%AE%E7%9A%84.txt)
- [06-10　有事要問王妃](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-10%E3%80%80%E6%9C%89%E4%BA%8B%E8%A6%81%E5%95%8F%E7%8E%8B%E5%A6%83.txt)
- [06-11　奇怪的王妃](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-11%E3%80%80%E5%A5%87%E6%80%AA%E7%9A%84%E7%8E%8B%E5%A6%83.txt)
- [06-12　王妃的忠告](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-12%E3%80%80%E7%8E%8B%E5%A6%83%E7%9A%84%E5%BF%A0%E5%91%8A.txt)
- [06-13　其實很簡單](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0%EF%BC%9A%E5%AD%B8%E9%99%A2%E9%9A%B1%E5%B1%85%E7%94%9F%E6%B4%BB/06-13%E3%80%80%E5%85%B6%E5%AF%A6%E5%BE%88%E7%B0%A1%E5%96%AE.txt)

