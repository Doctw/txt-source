那傢夥竟然在【家】裏。之前打敗龍的時候，完全沒有注意到。似乎施加了高度的【隱蔽】。如果不檢索戒指的話，也許一生都不會注意到。

嗯。居然在家裡。這，好歹與考慮的不同。也許對方會說話。

我用雷達找到了，看起來很堅固的門上，有一隻看起來很酷的獅子敲門門環。在日本的話，好像是在附近的房子大門上見過的設計。

大概製作者是日本人吧。稀人之類，自報姓名沒錯吧。我猶豫了，敲了敲門。

「請進～」
聽見像是高音質的內線電話，清爽的聲音的同時，還發出了像是高科技的搖滾脫落的電子機械式的開鎖音。

收到答覆。抬起頭。稍微猶豫了一下，下定決心進去看看。
「打擾了～」

走進裡面，有個漂亮的美女微笑著。黑髮黑眼珠圓圓地日本人的臉。

「客人什麼的，隔了多少年了啊。好高興啊。這裡看得很清楚啊。我在監視器上看到，馬上就知道你沒對我抱有敵意。而且……。」

她沉默了一下。好像在期待著什麼。嗯。黑髮黑眼，扁平的臉。

「呀，你好。哦。如果錯了就對不起了，你是阿爾伯特洛斯第一代國王製作的哥雷姆……不，是機器人？他是日本人嗎？」
我試著用日語問了一下。

「哎呀，你是……果然是日本人？」
她也用日語還了。我告訴了他的名字。

「我的主人叫船橋武。」
真的是日本人的名字。

「對不起。日本的飲料和食物，什麼都沒剩了。」
「誒，要的話就分妳吧？因為要多少都有。」

「不，我不需要。」
「妳在這裡，一直看守著戒指嗎？」
一邊環視房間一邊聽了。簡陋的房間。整理得很好，一點灰塵也沒有。

「是的。因為沒有別的事要做。這是主人最後的命令。」

「這樣啊。在我和船橋所在的國家，這樣的故事有很多。看到這樣，我真不知道該說些什麼。」
我不禁閉上眼睛，想起了那段漫長的時光。

「是嗎？」
「和我……去冒險之旅吧？」

「誒？」
「在我住的地方，有很尊重像你這樣的忠義者的習慣。反正我和你這樣的傢夥在一起。」

想什麼時候，製作朋友。信賴的傢夥。我在這個世界也是異端的。如果是這個孩子的話。

「這樣啊。也有主人住的地方？」
「在哪裡？」

「我不太明白。」
「那我就當薩摩吧。」

日本也是屈指可數的戰鬥民族。她很適合吧。

「喂！」
我被安東尼奧叫醒了。

「啊，想起了重要的事情。船橋武25代以後的子孫有點辛苦。」

「啊。怎麼回事？
說明詳細情況。

「知道了。我幫您拿戒指。」

「可以嗎？」
「還有一個被主人拜託的事情。如果有一天，我的子孫陷入困境的話，請幫幫忙。如果真的需要那枚戒指的話，希望你遞給我的子孫。」
靜靜地閉上眼睛，就好像想起了主人一樣。

「這樣啊。沒事可做了。」
「帶妳去旅行吧？」

「啊，這樣啊！」
好！得到冒險的夥伴了！這個世界的冒險者酒館在迷宮的底部嗎？

「你從這裡出去，魔力沒問題嗎？你在這裡用魔力行動著吧？」
「你注意到了嗎？」

「啊，因為所擁有的知識是一樣的。構思也差不多。」
嗯。他的愛讀書，看的節目和電影好像浮現在眼前。

「能使用我的魔力嗎？積蓄了魔力，因為有魔石所以也可以使用。如果我死了，再來這裡的話就不好辦了。還有……去給武掃墓吧。」

「是的……是吧。」
戒指的看門人，靦腆地微笑著。

以轉移魔法回到王宮前。告訴了城裡的衛兵。
「叫我羅巴侍從長。他要的戒指拿來了。還有一位重要的客人。」

爺爺慌忙地一下子跑了過來。
「真是的，阿爾馮斯！」

「啊，給我看看。」
然後，她只穿著衣服前面……啪的一聲打開了胸部。那裡有戒指。

「請收下。」
「哦，你究竟是？」
因驚愕而扭曲臉的爺爺。

「她是第一代國王船橋武，將戒指託付給了這個國家的未來。好棒啊，爺爺。」
我調皮地笑著對他說。

「誰知道船橋這個姓，是第一代國王嗎？」
絕句的老爺爺。

「長時間以來，真的辛苦您了。這枚戒指，謹存。」
老爺爺深深地低下了頭。

「啊，我還沒聽過妳的名字。」
「真理啊。是主人的妹妹的名字。我的原型是她。」

「比主人小10歲。父母早逝。主人很努力地照顧她。」
「哇……。」

在那種情況下，你來到這裡嗎？南無。我沒有任何困擾。就是那樣，但有些東西會來。有點難過。
（譯註：なむさん，南無，佛教用語。）

「他們什麼時候出生的？」
「妹妹出生於1997年。2015年，主人來到了這個世界。」

哇！幾乎是同一個年齡啊。妹妹現在也許一直在等哥哥回來。